const _ = require('lodash')
const path = require('path')
const fs = require('fs-extra')
const recursiveReaddir = require('recursive-readdir')

const filterFilesByType = files => {
    const allowedTypes = ['.png', '.jpg', '.jpeg', '.svg']
    return _.filter(files, file => _.includes(allowedTypes, path.extname(file).toLowerCase()))
}

const relativisePaths = files => _.map(files, file => file.replace(/^.*\/src\/assets/, '/assets'))

const processFilenames = files => {
    const filtered = filterFilesByType(files)
    return relativisePaths(filtered)
}

const fetchFilesFromDir = async (directory = '', basedir = '../../client/src/assets/') => {
    const dest = path.resolve(__dirname, basedir, directory)
    const files = await recursiveReaddir(dest)
    return processFilenames(files)
}

module.exports = {
    fetchFilesFromDir,
    processFilenames
}
