const imageService = require('./imageService')
const assert = require('assert')
const mock = require('mock-fs')
const _ = require('lodash')
const path = require('path')

describe('imageService', () => {
    describe('#fetchFilesFromDir', () => {
        beforeEach(async () => {
            // Create mock filesytem:
            const rootUrl = path.resolve(__dirname, '../../')
            mock({
                [`${rootUrl}/client/src/assets`]: {
                    'a.md': 'hi',
                    'b.png': 'asdfsdf',
                    'c.jpg': 'asdfasdf',
                    d: {
                        'd-01.png': 'asdfasdf',
                        'd-02.png': 'asdfasdf',
                        deep: {
                            deeper: {
                                'deepest.png': 'asdf'
                            }
                        }
                    }
                }
            })
        })

        afterEach(async () => {
            // Clean-up:
            mock.restore()
        })

        it('should read files from src directory', async () => {
            const actual = await imageService.fetchFilesFromDir('')
            const expected = [
                '/assets/b.png',
                '/assets/c.jpg',
                '/assets/d/d-01.png',
                '/assets/d/d-02.png',
                '/assets/d/deep/deeper/deepest.png'
            ]
            assert.deepEqual(actual, expected)
        })

        it('should read files from nested directory', async () => {
            const actual = await imageService.fetchFilesFromDir('d')
            const expected = [
                '/assets/d/d-01.png',
                '/assets/d/d-02.png',
                '/assets/d/deep/deeper/deepest.png'
            ]
            assert.deepEqual(actual, expected)
        })

        it('should read files heavily nested directory', async () => {
            const actual = await imageService.fetchFilesFromDir('d/deep/deeper')
            const expected = ['/assets/d/deep/deeper/deepest.png']
            assert.deepEqual(actual, expected)
        })
    })

    describe('#processFilenames', () => {
        it('should remove all non-image filenames from array', () => {
            const actual = imageService.processFilenames([
                'a.png',
                'b.jpg',
                'c.jpeg',
                'd.svg',
                'e.txt'
            ])
            const expected = ['a.png', 'b.jpg', 'c.jpeg', 'd.svg']
            assert.deepEqual(actual, expected)
        })

        it('should be case insensitive', () => {
            const actual = imageService.processFilenames([
                'a.png',
                'b.JPG',
                'c.jpeg',
                'd.SVG',
                'e.txt'
            ])
            const expected = ['a.png', 'b.JPG', 'c.jpeg', 'd.SVG']
            assert.deepEqual(actual, expected)
        })

        it('strips filepath before /assets', () => {
            const actual = imageService.processFilenames([
                '/User/billybob/tipster-web/client/src/assets/a.png',
                '/User/billybob/tipster-web/client/src/assets/b.jpg'
            ])
            const expected = ['/assets/a.png', '/assets/b.jpg']
            assert.deepEqual(actual, expected)
        })
    })
})
