const jwt = require('jsonwebtoken')
const _ = require('lodash')
const config = require('config')
const Route = require('route-parser')

const ignoredRoutes = {}

const isIgnoredRoute = req => {
    return _.some(_.keys(ignoredRoutes), routePattern => {
        const route = new Route(routePattern)
        return (
            route.match(req.url) &&
            (ignoredRoutes[routePattern]['*'] ||
                ignoredRoutes[routePattern][_.lowerCase(req.method)])
        )
    })
}

function verifyUser(req, res, next) {
    if (isIgnoredRoute(req)) {
        return next()
    }

    const token = req.body.token || req.query.token || req.headers['x-access-token']

    if (!token) {
        // if there is no token
        return res.status(403).send({
            error: true,
            message: 'No token provided.'
        })
    }

    // verifies secret and checks exp
    jwt.verify(token, config.get('jwtSecret'), (err, decoded) => {
        if (err) {
            return res.status(401).json({
                error: true,
                message: 'Failed to authenticate token.'
            })
        }

        req.tipsterAuth = {
            role: decoded.role,
            organisation: decoded.organisation,
            id: decoded.id
        }

        return next()
    })
}

verifyUser.ignore = (routePattern, method) => {
    _.set(ignoredRoutes, [routePattern, _.lowerCase(method) || '*'], true)
    return verifyUser
}

module.exports = verifyUser
