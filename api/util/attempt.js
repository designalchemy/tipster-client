module.exports = function attempt(fn, args, defaults) {
    try {
        const result = fn(...args)
        return result
    } catch (e) {
        return defaults
    }
}
