const config = require('config')
const thinky = require('thinky')

// By requiring this file we can ensure thinky only initialised once.

module.exports = thinky(config.get('rethink'))

