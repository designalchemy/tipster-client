const Config = require('../../model/Config')
const moment = require('moment')
const router = require('express').Router()
const thinky = require('../../util/thinky')
const Auth = require('../../model/Auth')
const _ = require('lodash')

const rethink = thinky.r

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/:id', (req, res, next) => {
        const urlId = req.params.id

        const data = {
            brandId: '',
            id: req.headers['rpt-auth-key'],
            browser: req.headers.browser,
            location: '', // hard code this but change it later, this dosnt have a location so give it face one for now
            os: req.headers.os,
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            lastLogin: moment().format(),
            type: 'RPT',
            configId: [
                {
                    id: urlId,
                    auth: false
                }
            ]
        }

        const saveNew = () => {
            const saveAuth = new Auth(data)
            data.requestDate = moment().format()
            delete data.id

            saveAuth
                .save()
                .then(response => {
                    return res.status(201).send({
                        error: true,
                        message: 'This auth key isnt authenticated',
                        data: response
                    })
                })
                .catch(next)
        }

        if (_.isNil(data.id) || data.id.length === 0) {
            saveNew()
        } else {
            rethink
                .table('Auth')
                .get(data.id)
                .run()
                .then(data => {
                    if (_.isNil(data)) {
                        saveNew()
                        return false
                    }

                    if (_.find(data.configId, configId => configId.id === urlId).auth === false) {
                        return res.send({
                            error: true,
                            message: 'This auth key isnt authenticated',
                            data: data
                        })
                    }

                    rethink
                        .table('Config')
                        .get(urlId)
                        .run()
                        .then(data => {
                            res.send({ error: false, data: data })
                        })
                        .catch(next)
                })
                .catch(next)
        }
    })
