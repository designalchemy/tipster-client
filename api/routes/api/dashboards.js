const Dashboard = require('../../model/Dashboard')
const moment = require('moment')
const router = require('express').Router()
const thinky = require('../../util/thinky')
const _ = require('lodash')
const verifyUser = require('../../middleware/auth')
const attempt = require('../../util/attempt')
const ss = require('string-similarity')

const rethink = thinky.r

const createConfigData = req => ({
    bookmaker_id: req.body.bookmaker_id,
    soft_name: req.body.soft_name,
    sport: req.body.sport,
    dashboard_mascot: req.body.dashboard_mascot,
    global_timing: req.body.global_timing,
    text_timing: req.body.text_timing,
    bet_timing: req.body.bet_timing,
    predictor_timing: req.body.predictor_timing,
    price_format: req.body.price_format,
    price_source: req.body.price_source,
    show_race_title: req.body.show_race_title,
    show_prices: req.body.show_prices,
    max_promps: req.body.max_promps,
    logo: req.body.logo,
    main_color: req.body.main_color,
    bottom_bar_text: req.body.bottom_bar_text,
    bet_prompt_offer_logo: req.body.bet_prompt_offer_logo,
    bet_prompt_offer_text: req.body.bet_prompt_offer_text,
    frozen_race_card_cells: req.body.frozen_race_card_cells,
    clock_is_24_hour: req.body.clock_is_24_hour === 'true',
    race_time_is_24_hour: req.body.race_time_is_24_hour === 'true',
    next_race_time_is_24_hour: req.body.next_race_time_is_24_hour === 'true',
    welome_prompt_text: req.body.welome_prompt_text,
    odds_color: req.body.odds_color
})

router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', verifyUser, (req, res, next) => {
        const id = req.headers.id
        const tipsterAuth = req.tipsterAuth
        const orgId = tipsterAuth.organisation

        const filters = attempt(JSON.parse, [req.headers.filters], [])
        const pagination = attempt(JSON.parse, [req.headers.pagination], [])
        const search = attempt(JSON.parse, [req.headers.search], {
            query: '',
            type: ''
        })
        const sorting = attempt(JSON.parse, [req.headers.sorting], {
            order: '',
            type: ''
        })

        let order = ''

        if (sorting.order !== '' && sorting.order === 'ascend') {
            order = 'asc'
        } else if (sorting.order !== '' && sorting.order === 'descend') {
            order = 'desc'
        }

        if (id) {
            Promise.all([
                rethink
                    .table('Dashboard')
                    .filter({ _dashboard_id: id })
                    .filter(
                        dashboard =>
                            tipsterAuth.role === 'admin'
                                ? dashboard
                                : dashboard('dashboard_brand_id').eq(orgId)
                    )
                    .eqJoin('dashboard_brand_id', rethink.table('Brand'))
                    .zip()
                    .run(),
                rethink
                    .table('Brand')
                    .filter(brand => (tipsterAuth.role === 'admin' ? brand : brand('id').eq(orgId)))
                    .run(),
                rethink.table('Act').run()
            ])
                .then(([dashboards, brands, acts]) => {
                    const dashboard = _.head(dashboards)
                    res.send({ dashboard, brands, acts })
                })
                .catch(next)
        } else {
            rethink
                .table('Dashboard')
                .filter(
                    dashboard =>
                        tipsterAuth.role === 'admin'
                            ? dashboard
                            : dashboard('dashboard_brand_id').eq(orgId)
                )
                .eqJoin('dashboard_brand_id', rethink.table('Brand')) // brand for brand info
                .without({ right: 'id' })
                .zip()
                .run()
                .then(response => {
                    let bookmakers = _.map(
                        _.uniqBy(response, 'config.bookmaker_id'),
                        item => (item.config.bookmaker_id !== '' ? item.config.bookmaker_id : null)
                    )
                    bookmakers = _.compact(bookmakers)

                    let orgs = _.map(
                        _.uniqBy(response, 'brand_name'),
                        item => (item.brand_name !== '' ? item.brand_name : null)
                    )
                    orgs = _.compact(orgs)

                    let filterData = _.chain(response)
                        .filter(
                            item =>
                                _.size(filters['config.bookmaker_id']) > 0
                                    ? _.includes(
                                          filters['config.bookmaker_id'],
                                          item.config.bookmaker_id
                                      )
                                    : item
                        )
                        .filter(
                            item =>
                                _.size(filters.brand_name) > 0
                                    ? _.includes(filters.brand_name, item.brand_name)
                                    : item
                        )
                        .filter(item => {
                            if (search.query !== '') {
                                return (
                                    ss.compareTwoStrings(
                                        search.query.toUpperCase(),
                                        _.get(item, search.type).toUpperCase()
                                    ) >= 0.4
                                )
                            }
                            return item
                        })
                        .value()
                    const total = filterData.length

                    filterData =
                        sorting.order !== ''
                            ? _.orderBy(filterData, sorting.field, order)
                            : filterData

                    filterData =
                        _.size(pagination) > 0
                            ? filterData.slice(
                                  pagination.pageSize * (pagination.current - 1),
                                  pagination.pageSize * pagination.current
                              )
                            : filterData

                    const data = {
                        bookmakers,
                        total,
                        orgs,
                        data: filterData
                    }
                    res.send(data)
                })
                .catch(next)
        }
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth

        const data = {
            dashboard_brand_id: req.body.dashboard_brand_id,
            config: createConfigData(req)
        }

        if (tipsterAuth.role !== 'admin') {
            data.dashboard_brand_id = tipsterAuth.organisation
        }

        const saveDashboard = new Dashboard(data)
            .save()
            .then(response => {
                return res.status(201).send({
                    error: false,
                    message: 'New Dashboard created'
                })
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth
        const id = req.body.id

        const data = {
            dashboard_brand_id: req.body.dashboard_brand_id,
            config: createConfigData(req)
        }

        rethink
            .table('Dashboard')
            .filter(dashboard => {
                if (tipsterAuth.role === 'admin') {
                    return dashboard('_dashboard_id').eq(req.body.id)
                }
                return dashboard('_dashboard_id')
                    .eq(req.body.id)
                    .and(dashboard('dashboard_brand_id').eq(tipsterAuth.organisation))
                // filter ID and ensure they have brand access
            })
            .update(data)
            .run()
            .then(response => {
                if (response.errors) {
                    next(response.error)
                } else {
                    res.status(200).send({ error: false, message: response })
                }
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth

        rethink
            .table('Dashboard')
            .filter(dashboard => {
                if (tipsterAuth.role === 'admin') {
                    return dashboard('_dashboard_id').eq(req.body.id)
                }
                return dashboard('_dashboard_id')
                    .eq(req.body.id)
                    .and(dashboard('dashboard_brand_id').eq(tipsterAuth.organisation))
                // filter ID and ensure they have brand access
            })
            .delete()
            .run()
            .then(response => {
                if (response.errors) {
                    next(response.error)
                } else {
                    res.status(200).send({ error: false, message: response })
                }
            })
            .catch(next)
    })

module.exports = router
