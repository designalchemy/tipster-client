const router = require('express').Router()
const _ = require('lodash')
const promiseMiddleware = require('../../middleware/promiseMiddleware')
const Languages = require('../../model/Languages')
const thinky = require('../../util/thinky')
const rethink = thinky.r

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get(
        '/',
        promiseMiddleware(async (req, res) => {
            const languages = await rethink.table('Languages')
            res.send({ error: false, data: languages })
        })
    )
