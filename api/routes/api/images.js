const router = require('express').Router()

const path = require('path')

const promiseMiddleware = require('../../middleware/promiseMiddleware')
const imageService = require('../../services/imageService')

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get(
        '/',
        promiseMiddleware(async (req, res) => {
            const directory = ''
            const files = await imageService.fetchFilesFromDir(directory)
            res.send(files)
        })
    )
    .get(
        '/:directory',
        promiseMiddleware(async (req, res) => {
            const directory = req.params.directory
            const files = await imageService.fetchFilesFromDir(directory)
            res.send(files)
        })
    )
    .get(
        '/:directory/:subdir',
        promiseMiddleware(async (req, res) => {
            const directory = path.join(req.params.directory, req.params.subdir)
            const files = await imageService.fetchFilesFromDir(directory)
            res.send(files)
        })
    )
    .get(
        '/:directory/:subdir/:subsubdir',
        promiseMiddleware(async (req, res) => {
            const directory = path.join(
                req.params.directory,
                req.params.subdir,
                req.params.subsubdir
            )
            const files = await imageService.fetchFilesFromDir(directory)
            res.send(files)
        })
    )
