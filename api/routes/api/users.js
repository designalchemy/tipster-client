const bcrypt = require('bcrypt-nodejs')
const moment = require('moment')
const router = require('express').Router()
const thinky = require('../../util/thinky')
const attempt = require('../../util/attempt')
const User = require('../../model/Users')
const verifyUser = require('../../middleware/auth')
const ss = require('string-similarity')
const _ = require('lodash')

const rethink = thinky.r
const saltRounds = 10

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth

        const filters = attempt(JSON.parse, [req.headers.filters], [])
        const pagination = attempt(JSON.parse, [req.headers.pagination], [])
        const search = attempt(JSON.parse, [req.headers.search], {
            query: '',
            type: ''
        })
        const sorting = attempt(JSON.parse, [req.headers.sorting], {
            order: '',
            type: ''
        })

        let order = ''

        if (sorting.order !== '' && sorting.order === 'ascend') {
            order = 'asc'
        } else if (sorting.order !== '' && sorting.order === 'descend') {
            order = 'desc'
        }

        rethink
            .table('Users')
            .filter(
                user =>
                    tipsterAuth.role === 'admin'
                        ? user
                        : user('organisation').eq(tipsterAuth.organisation)
            )
            .eqJoin('organisation', rethink.table('Brand'))
            .without({ right: 'id' })
            .zip()
            .run()
            .then(response => {
                let orgs = _.map(
                    _.uniqBy(response, 'brand_name'),
                    item => (item.brand_name !== '' ? item.brand_name : null)
                )

                orgs = _.compact(orgs)

                let filterData = _.chain(response)
                    .filter(
                        item =>
                            _.size(filters.brand_name) > 0
                                ? _.includes(filters.brand_name, item.brand_name)
                                : item
                    )
                    .filter(item => {
                        if (search.query !== '') {
                            if (search.type === 'firstName') {
                                const wholeName = `${item.firstName} ${item.secondName}`
                                return (
                                    ss.compareTwoStrings(
                                        search.query.toUpperCase(),
                                        wholeName.toUpperCase()
                                    ) >= 0.4
                                )
                            }
                            return (
                                ss.compareTwoStrings(
                                    search.query.toUpperCase(),
                                    item[search.type].toUpperCase()
                                ) >= 0.4
                            )
                        }
                        return item
                    })
                    .value()
                const total = filterData.length

                filterData =
                    sorting.order !== '' ? _.orderBy(filterData, sorting.field, order) : filterData

                filterData =
                    _.size(pagination) > 0
                        ? filterData.slice(
                              pagination.pageSize * (pagination.current - 1),
                              pagination.pageSize * pagination.current
                          )
                        : filterData

                const data = {
                    total,
                    orgs,
                    data: filterData
                }
                res.send(data)
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', verifyUser, (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            const createUser = () => {
                const formData = {
                    firstName: req.body.firstName,
                    secondName: req.body.secondName,
                    email: req.body.email,
                    role: req.body.role,
                    organisation: req.body.organisation,
                    skype: req.body.skype,
                    ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                }

                formData.password = bcrypt.hashSync(
                    req.body.password,
                    bcrypt.genSaltSync(saltRounds)
                )

                const saveUser = new User(formData)

                saveUser
                    .save()
                    .then(response => {
                        res.status(201).send({
                            error: false,
                            message: 'Sign up successful'
                        })
                    })
                    .catch(next)
            }
            rethink
                .table('Users')
                .filter({ email: req.body.email })
                .count()
                .run()
                .then(users => {
                    if (users > 0) {
                        res.send({
                            error: true,
                            message: 'User with this email already exists'
                        })
                    } else {
                        createUser()
                    }
                })
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', verifyUser, (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            const updateUser = () => {
                const formData = {
                    id: req.body.id,
                    firstName: req.body.firstName,
                    secondName: req.body.secondName,
                    email: req.body.email,
                    role: req.body.role,
                    organisation: req.body.organisation,
                    skype: req.body.skype
                }

                rethink
                    .table('Users')
                    .get(formData.id)
                    .update(formData)
                    .run()
                    .then(response => {
                        if (response.errors) {
                            res.status(400).send({ error: true, message: response.error })
                        } else {
                            res.status(200).send({ error: false, message: response })
                        }
                    })
                    .catch(next)
            }
            if (req.body.editEmail) {
                rethink
                    .table('Users')
                    .filter({ email: req.body.email })
                    .count()
                    .run()
                    .then(users => {
                        if (users > 0) {
                            res.send({
                                error: true,
                                message: 'User with this email already exists'
                            })
                        } else {
                            updateUser()
                        }
                    })
            } else {
                updateUser()
            }
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', verifyUser, (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            rethink
                .table('Users')
                .get(req.body.id)
                .delete()
                .run()
                .then(response => {
                    if (response.errors) {
                        res.status(400).send({ error: true, message: response.error })
                    } else {
                        res.status(200).send({ error: false, message: response })
                    }
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
