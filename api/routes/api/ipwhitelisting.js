const moment = require('moment')
const router = require('express').Router()
const thinky = require('../../util/thinky')
const _ = require('lodash')
const attempt = require('../../util/attempt')
const verifyUser = require('../../middleware/auth')
const CIDR = require('cidr-js')
const ss = require('string-similarity')
const IP = require('../../model/IP')

const rethink = thinky.r
const assert = require('assert')

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', (req, res, next) => {
        const filters = attempt(JSON.parse, [req.headers.filters], [])
        const pagination = attempt(JSON.parse, [req.headers.pagination], [])
        const search = attempt(JSON.parse, [req.headers.search], {
            query: '',
            type: ''
        })
        const sorting = attempt(JSON.parse, [req.headers.sorting], {
            order: '',
            type: ''
        })

        let order = ''
        let locations
        let orgs
        let total

        if (sorting.order !== '' && sorting.order === 'ascend') {
            order = 'asc'
        } else if (sorting.order !== '' && sorting.order === 'descend') {
            order = 'desc'
        }

        rethink
            .table('IP_Whitelist')
            .outerJoin(rethink.table('Locations'), (ips, locations) =>
                ips('locationId').eq(locations('id'))
            )
            .without({ right: 'id' })
            .zip()
            .run()
            .then(response => {
                orgs = _.reduce(
                    _.uniqBy(response, 'brand_softName'),
                    (acc, item) => {
                        item.brand_softName && acc.push(item.brand_softName)
                        return acc
                    },
                    []
                )
                locations = _.reduce(
                    _.uniqBy(response, 'location_softName'),
                    (acc, item) => {
                        item.location_softName && acc.push(item.location_softName)
                        return acc
                    },
                    []
                )

                let filterData = _.chain(response)
                    .filter(
                        item =>
                            _.size(filters.brand_softName) > 0
                                ? _.includes(filters.brand_softName, item.brand_softName)
                                : item
                    )
                    .filter(
                        item =>
                            _.size(filters.location_softName) > 0
                                ? _.includes(
                                      filters.location_softName.toString(),
                                      item.location_softName.toString()
                                  )
                                : item
                    )
                    .filter(
                        item =>
                            search.query !== ''
                                ? ss.compareTwoStrings(
                                      search.query.toUpperCase(),
                                      item[search.type].toUpperCase()
                                  ) >= 0.4
                                : item
                    )
                    .value()

                total = filterData.length
                filterData =
                    _.size(pagination) > 0
                        ? filterData.slice(
                              pagination.pageSize * (pagination.current - 1),
                              pagination.pageSize * pagination.current
                          )
                        : filterData

                filterData =
                    sorting.order !== '' ? _.orderBy(filterData, sorting.field, order) : filterData

                const data = {
                    total,
                    locations,
                    orgs,
                    data: filterData
                }

                res.send(data)
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', verifyUser, async (req, res, next) => {
        const createWhiteList = () => {
            if (req.tipsterAuth.role === 'admin') {
                const data = {
                    ip_range: req.body.ip_range,
                    locationId: req.body.locationId,
                    location_softName: req.body.location_softName,
                    type: 'RPD',
                    brandId: req.body.brandId,
                    auto_auth: req.body.auto_auth,
                    brand_softName: req.body.brand_softName,
                    ip_type: req.body.ip_type,
                    softName: req.body.softName,
                    configId: req.body.configId
                }

                const saveWhitelist = new IP(data)
                saveWhitelist
                    .save()
                    .then(response => {
                        res.status(201).send({
                            error: false,
                            message: 'Added new IP'
                        })
                    })
                    .catch(next)
            } else {
                res.send({ error: true, message: 'Not admin' })
            }
        }
        rethink
            .table('IP_Whitelist')
            .run()
            .then(all => {
                const cidrIncome = new CIDR()
                const rangeIncome = req.body.ip_range
                const rangeOfIPIncome = cidrIncome.list(rangeIncome)
                const exists = _.reduce(
                    all,
                    (acc, whitelist) => {
                        const cidrLoop = new CIDR()
                        const rangeLoop = whitelist.ip_range
                        const rangeOfIPLoop = cidrIncome.list(rangeLoop)

                        if (_.intersection(rangeOfIPIncome, rangeOfIPLoop).length > 0) {
                            acc = true
                        }

                        return acc
                    },
                    false
                )
                if (exists) {
                    res.status(409).send({
                        error: true,
                        message: 'IP Range clashes with another range'
                    })
                } else {
                    createWhiteList()
                }
            })
    })
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            const data = {
                ip_range: req.body.ip_range,
                locationId: req.body.locationId,
                location_softName: req.body.location_softName,
                type: 'RPD',
                brandId: req.body.brandId,
                auto_auth: req.body.auto_auth,
                brand_softName: req.body.brand_softName,
                ip_type: req.body.ip_type,
                softName: req.body.softName,
                configId: req.body.configId
            }

            const getAuth = () => {
                rethink
                    .table('Auth')
                    .filter({
                        ip_range: req.body.old_ip_range
                    })
                    .delete()
                    .run()
                    .then(response => {
                        res.status(200).send({
                            error: false,
                            message: 'IP Updated and associated Auths deleted.'
                        })
                    })
            }

            rethink
                .table('IP_Whitelist')
                .get(req.body.id)
                .update(data)
                .run()
                .then(response => {
                    if (response.errors) {
                        res.status(400).send({ error: true, message: response.error })
                    } else {
                        getAuth()
                    }
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', (req, res, next) => {
        const deleteAuth = () => {
            rethink
                .table('Auth')
                .filter({
                    ip_range: req.body.ip_range
                })
                .delete()
                .run()
                .then(response => {
                    res.status(201).send({
                        error: false,
                        message: 'IP and Auths deleted'
                    })
                })
        }
        if (req.tipsterAuth.role === 'admin') {
            rethink
                .table('IP_Whitelist')
                .get(req.body.id)
                .delete()
                .run()
                .then(response => {
                    deleteAuth()
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
