const Config = require('../../model/Config')
const moment = require('moment')
const router = require('express').Router()
const thinky = require('../../util/thinky')
const _ = require('lodash')
const verifyUser = require('../../middleware/auth')
const attempt = require('../../util/attempt')
const ss = require('string-similarity')

const rethink = thinky.r

router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', verifyUser, (req, res, next) => {
        const sendId = req.headers.id
        const tipsterAuth = req.tipsterAuth
        const orgId = tipsterAuth.organisation

        const filters = attempt(JSON.parse, [req.headers.filters], [])
        const pagination = attempt(JSON.parse, [req.headers.pagination], [])
        const search = attempt(JSON.parse, [req.headers.search], {
            query: '',
            type: ''
        })
        const sorting = attempt(JSON.parse, [req.headers.sorting], {
            order: '',
            type: ''
        })

        let order = ''

        if (sorting.order !== '' && sorting.order === 'ascend') {
            order = 'asc'
        } else if (sorting.order !== '' && sorting.order === 'descend') {
            order = 'desc'
        }

        if (sendId) {
            Promise.all([
                rethink
                    .table('Config')
                    .filter({ id: sendId })
                    .filter(
                        config =>
                            tipsterAuth.role === 'admin'
                                ? config
                                : config('config_brand_id').eq(orgId)
                    )
                    .eqJoin('config_brand_id', rethink.table('Brand'))
                    .without({ right: 'id' })
                    .zip()
                    .run(),
                rethink
                    .table('Brand')
                    .filter(brand => (tipsterAuth.role === 'admin' ? brand : brand('id').eq(orgId)))
                    .run()
            ])
                .then(([configs, brands]) => {
                    const config = _.head(configs)
                    res.send({ config, brands })
                })
                .catch(next)
        } else {
            rethink
                .table('Config')
                .filter(
                    dashboard =>
                        tipsterAuth.role === 'admin'
                            ? dashboard
                            : dashboard('config_brand_id').eq(orgId)
                )
                .eqJoin('config_brand_id', rethink.table('Brand')) // brand for brand info
                .without({ right: 'id' })
                .zip()
                .run()
                .then(response => {
                    let orgs = _.map(
                        _.uniqBy(response, 'brand_name'),
                        item => (item.brand_name !== '' ? item.brand_name : null)
                    )

                    orgs = _.compact(orgs)

                    let filterData = _.chain(response)
                        .filter(item => {
                            if (search.query !== '') {
                                return (
                                    ss.compareTwoStrings(
                                        search.query.toUpperCase(),
                                        item[search.type].toUpperCase()
                                    ) >= 0.4
                                )
                            } else {
                                return item
                            }
                        })
                        .filter(
                            item =>
                                _.size(filters.brand_name) > 0
                                    ? _.includes(filters.brand_name, item.brand_name)
                                    : item
                        )
                        .value()
                    const total = filterData.length

                    filterData =
                        sorting.order !== ''
                            ? _.orderBy(filterData, sorting.field, order)
                            : filterData

                    filterData =
                        _.size(pagination) > 0
                            ? filterData.slice(
                                  pagination.pageSize * (pagination.current - 1),
                                  pagination.pageSize * pagination.current
                              )
                            : filterData

                    const data = {
                        orgs,
                        total,
                        data: filterData
                    }

                    res.send(data)
                })
                .catch(next)
        }
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth

        const data = {
            config_brand_id: req.body.config_brand_id,
            soft_name: req.body.soft_name,
            config: JSON.stringify(req.body.config)
        }

        if (tipsterAuth.role !== 'admin') {
            data.config_brand_id = tipsterAuth.organisation
        }

        const saveConfig = new Config(data)

        saveConfig
            .save()
            .then(response => {
                res.status(201).send({
                    error: false,
                    message: 'New Config created'
                })
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .put('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth
        const id = req.body.id
        const data = {
            config_brand_id: req.body.config_brand_id,
            soft_name: req.body.soft_name,
            config: JSON.stringify(req.body.config)
        }

        rethink
            .table('Config')
            .filter(config => {
                if (tipsterAuth.role === 'admin') {
                    return config('id').eq(req.body.id)
                } else {
                    return config('id')
                        .eq(req.body.id)
                        .and(config('config_brand_id').eq(tipsterAuth.organisation))
                    // filter ID and ensure they have brand access
                }
            })
            .update(data)
            .run()
            .then(response => {
                if (response.errors) {
                    next(response.error)
                } else {
                    res.status(200).send({ error: false, message: response })
                }
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth

        rethink
            .table('Config')
            .filter(config => {
                if (tipsterAuth.role === 'admin') {
                    return config('id').eq(req.body.id)
                } else {
                    return config('id')
                        .eq(req.body.id)
                        .and(config('config_brand_id').eq(tipsterAuth.organisation))
                    // filter ID and ensure they have brand access
                }
            })
            .delete()
            .run()
            .then(response => {
                if (response.errors) {
                    next(response.error)
                } else {
                    res.status(200).send({ error: false, message: response })
                }
            })
            .catch(next)
    })

module.exports = router
