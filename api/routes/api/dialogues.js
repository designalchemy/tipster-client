const router = require('express').Router()
const _ = require('lodash')
const promiseMiddleware = require('../../middleware/promiseMiddleware')
const Dialogues = require('../../model/Dialogues')
const Tokens = require('../../model/Tokens')
const thinky = require('../../util/thinky')
const rethink = thinky.r

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get(
        '/:sport/:language',
        promiseMiddleware(async (req, res) => {
            const response = await rethink
                .table('Dialogues')
                .filter({ language: req.params.language })
                .run()
            res.send({ error: false, data: response })
        })
    )
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////

    .post(
        '/:sport/:language',
        promiseMiddleware(async (req, res) => {
            const sport = req.params.sport
            const language = req.params.language
            const { key, items } = req.body

            const newBody = {
                key,
                items,
                sport,
                language
            }

            const token = new Dialogues(newBody)

            const response = await token.save()

            res.send({
                error: false,
                data: response
            })
        })
    )
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////

    .put(
        '/',
        promiseMiddleware(async (req, res) => {
            const newBody = {
                items: req.body.items
            }

            const response = await rethink
                .table('Dialogues')
                .get(req.body.id)
                .update(newBody)
                .run()

            res.send({
                error: false,
                data: response
            })
        })
    )
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete(
        '/',
        promiseMiddleware(async (req, res) => {
            const response = await rethink
                .table('Dialogues')
                .get(req.body.id)
                .delete()
                .run()

            res.send({
                error: false,
                data: response
            })
        })
    )
