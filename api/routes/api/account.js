const router = require('express').Router()
const thinky = require('../../util/thinky')
const _ = require('lodash')
const bcrypt = require('bcrypt-nodejs')

const rethink = thinky.r
const saltRounds = 10

module.exports = router.put('/', (req, res, next) => {
    const formData = {}

    if (req.body.password)
        formData.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(saltRounds))
    if (req.body.email) formData.email = req.body.email
    if (req.body.skype) formData.skype = req.body.skype
    if (req.body.firstName) formData.firstName = req.body.firstName
    if (req.body.secondName) formData.secondName = req.body.secondName

    rethink
        .table('Users')
        .get(req.tipsterAuth.id)
        .update(formData)
        .run()
        .then(response => {
            if (response.errors) {
                res.status(400).send({ error: true, message: response.error })
            } else {
                res.status(200).send({ error: false, message: response })
            }
        })
        .catch(next)
})
