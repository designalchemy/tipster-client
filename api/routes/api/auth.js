const Auth = require('../../model/Auth')
const moment = require('moment')
const router = require('express').Router()
const attempt = require('../../util/attempt')
const rangeCheck = require('../../util/cidr-matcher')
const thinky = require('../../util/thinky')
const _ = require('lodash')
const ss = require('string-similarity')
const verifyUser = require('../../middleware/auth')

const rethink = thinky.r

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', (req, res, next) => {
        let total
        const tipsterAuth = req.tipsterAuth

        const filters = attempt(JSON.parse, [req.headers.filters], [])
        const pagination = attempt(JSON.parse, [req.headers.pagination], [])
        const search = attempt(JSON.parse, [req.headers.search], {
            query: '',
            type: ''
        })
        const sorting = attempt(JSON.parse, [req.headers.sorting], {
            order: '',
            type: ''
        })

        let order = ''

        if (sorting.order !== '' && sorting.order === 'ascend') {
            order = 'asc'
        } else if (sorting.order !== '' && sorting.order === 'descend') {
            order = 'desc'
        }

        let location

        rethink
            .table('Auth')
            .outerJoin(rethink.table('Locations'), (auth, locations) =>
                auth('location').eq(locations('id'))
            )
            .without({ right: 'id' })
            .zip()
            .run()
            .then(response => {
                location = _.reduce(
                    _.uniqBy(response, 'post_code'),
                    (acc, item) => {
                        item.post_code && acc.push(item.post_code)
                        return acc
                    },
                    []
                )

                const mergeConfigData = _.sortBy(
                    _.flatMap(response, item =>
                        _.map(
                            item.configId,
                            cfg =>
                                cfg.id &&
                                _.assign({}, item, { configRef: cfg.id }, { configAuth: cfg.auth })
                        )
                    )
                )

                // use fitlers from table header

                let filterData = _.chain(mergeConfigData)
                    .filter(
                        item =>
                            _.size(filters.post_code) > 0
                                ? _.includes(filters.post_code, item.post_code)
                                : item
                    )
                    .filter(
                        item =>
                            _.size(filters.configAuth) > 0
                                ? _.includes(
                                      filters.configAuth.toString(),
                                      item.configAuth.toString()
                                  )
                                : item
                    )
                    .filter(
                        item =>
                            search.query !== ''
                                ? ss.compareTwoStrings(
                                      search.query.toUpperCase(),
                                      item[search.type].toUpperCase()
                                  ) >= 0.4
                                : item
                    )
                    .value()

                total = filterData.length
                filterData = _.map(filterData, item =>
                    rethink
                        .table('Dashboard')
                        .get(item.configRef)
                        .run()
                        .then(result =>
                            _.assign({}, item, {
                                configOwnerId: _.get(result, 'dashboard_brand_id'),
                                configSoftName: _.get(result, 'config.soft_name')
                            })
                        )
                )

                return Promise.all(filterData)
            })
            .then(filterData => {
                const configRefs = _.uniq(_.map(filterData, item => item.id))

                return rethink
                    .table('hz_client_connections')
                    .getAll(...configRefs, { index: 'uuid' })
                    .filter(rethink.row('online').gt(rethink.row('offline').default(0)))
                    .map({
                        uuid: rethink.row('uuid'),
                        page: rethink.row('page')
                    })
                    .run()
                    .then(result => {
                        const filterResults = _.uniqWith(result, _.isEqual)
                        const groupResults = _.groupBy(filterResults, 'uuid')
                        const onlyKeys = _.keys(groupResults)

                        // loop data and add active or not depending upon match of ID and config ID
                        return _.map(filterData, item =>
                            _.assign({}, item, {
                                active:
                                    _.includes(onlyKeys, item.id) &&
                                    Boolean(
                                        _.find(groupResults[item.id], {
                                            page: item.configRef
                                        })
                                    ) // force true / false
                            })
                        )
                    })
            })
            .then(filterData => {
                // filter admin or user data

                filterData =
                    sorting.order !== '' ? _.orderBy(filterData, sorting.field, order) : filterData

                filterData =
                    _.size(pagination) > 0
                        ? filterData.slice(
                              pagination.pageSize * (pagination.current - 1),
                              pagination.pageSize * pagination.current
                          )
                        : filterData

                filterData = _.filter(
                    // move this before rest of queries
                    filterData,
                    item =>
                        tipsterAuth.role === 'admin'
                            ? item
                            : item.configOwnerId === tipsterAuth.organisation
                )

                const data = {
                    total,
                    locations: location,
                    data: filterData
                }

                res.send(data)
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', async (req, res, next) => {
        // ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        let range
        const data = {
            id: req.body.id,
            configId: [{ id: req.body.configId, auth: false }],
            browser: req.body.browser,
            os: req.body.os,
            lastLogin: moment().format(),
            location: '',
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            type: 'RPD',
            brandId: '',
            softName: '',
            ip_whitelist: false,
            auto_auth: false
        }

        const isRealConfigCheck = await rethink
            .table('Dashboard')
            .get(req.body.configId)
            .run()

        if (!isRealConfigCheck) {
            return res.send({
                error: true,
                message: 'Invalid Config ID',
                data: { ip: data.ip, id: 'error' }
            })
        }

        const updateTime = id => {
            const date = moment().format()
            // update the time auth was attempted

            rethink
                .table('Auth')
                .get(id)
                .update({ lastLogin: date })
                .run()
                .then(response => logger.info(response))
                .catch(next)
        }

        const saveNew = (ipData, whitelist) => {
            //Add new auth db based on if it is ip whitelist data or default auth data
            const newData = ipData || data
            const saveAuth = new Auth(newData)
            // add a new auth attempt to DB

            delete newData.id
            newData.requestDate = moment().format()

            saveAuth
                .save()
                .then(response => {
                    // Save and check to see if automatic auth
                    checkConfigIds(response, whitelist)
                })
                .catch(next)
        }

        const addNewId = (configId, resData) => {
            const newConfig = configId.concat({
                id: req.body.configId,
                auth: false
            })

            rethink
                .table('Auth')
                .insert(
                    {
                        id: data.id,
                        configId: newConfig
                    },
                    { conflict: 'update' }
                )
                .run()
                .then(response => {
                    res.status(201).send({
                        error: true,
                        message: 'Not Authed Yet',
                        data: resData
                    })
                })
                .catch(next)
        }

        const cookieAuth = () => {
            if (!_.isUndefined(data.id)) {
                rethink
                    .table('Auth')
                    .get(data.id)
                    .run()
                    .then(getAuth => {
                        // if nothing comes back form DB, try again, EG, has tryed to auth before but been deleted
                        if (_.isNil(getAuth)) {
                            saveNew()
                            return false
                        }

                        const idToCompare = _.map(getAuth.configId, item => item.id)

                        if (!_.includes(idToCompare, req.body.configId)) {
                            return addNewId(getAuth.configId, getAuth)
                        }

                        const whichAuth = _.filter(
                            getAuth.configId,
                            item => item.id === req.body.configId
                        )

                        if (_.head(whichAuth).auth || getAuth.auto_auth) {
                            // success
                            res.send({
                                error: false,
                                message: 'Auth successful',
                                data: getAuth
                            })
                        } else {
                            // not yet authed
                            res.send({
                                error: true,
                                message: 'Not Authed Yet',
                                data: getAuth
                            })
                        }
                        updateTime(getAuth.id) // update login date
                    })
                    .catch(next)
            } else {
                saveNew()
                // brand new attempt
            }
        }

        const checkConfigIds = (auth, whitelist) => {
            //Checks if auto auth, adds config to auth and updates
            const isAuthed = _.get(_.find(auth.configId, { id: req.body.configId }), 'auth')

            const whitelistContains =
                whitelist && _.get(_.find(whitelist.configId, { id: req.body.configId }), 'auth')

            if (auth.auto_auth) {
                auth.lastLogin = moment().format()
                if (typeof auth.configId === 'undefined') {
                    auth.configId = []
                }
                if (!isAuthed) {
                    auth.configId.push({ id: req.body.configId, auth: true })
                }
                rethink
                    .table('Auth')
                    .get(auth.id)
                    .update(auth)
                    .run()
                    .then(response => {
                        res.send({
                            error: false,
                            message: 'Auth Successful',
                            data: auth
                        })
                    })
            } else if (!isAuthed && whitelistContains && auth.ip_whitelist) {
                if (typeof auth.configId === 'undefined') {
                    auth.configId = []
                }
                auth.configId.push({ id: req.body.configId, auth: true })

                rethink
                    .table('Auth')
                    .get(auth.id)
                    .update(auth)
                    .run()
                    .then(response => {
                        res.send({
                            error: false,
                            message: 'Auth Successful',
                            data: auth
                        })
                    })
            } else if (isAuthed) {
                updateTime(auth.id)
                res.send({
                    error: false,
                    message: 'Auth successful',
                    data: auth
                })
            } else {
                cookieAuth()
            }
        }

        const createNewAuth = whitelist => {
            // Finds the config based on the matched range from the IP whitelist table.
            const newAuth = {
                browser: 'IP Whitelist',
                os: 'IP Whitelist',
                type: 'RPD',
                ip: data.ip,
                brandId: whitelist.brandId,
                lastLogin: moment().format(),
                location: whitelist.locationId,
                softName: whitelist.softName,
                ip_whitelist: true,
                auto_auth: whitelist.auto_auth,
                ip_range: whitelist.ip_range
            }
            // Saves it in db.
            saveNew(newAuth, whitelist)
        }

        // Start by grabbing all IP Whitelist ranges from DB. Returns array of all ranges.
        rethink
            .table('IP_Whitelist')
            .map(ip => ip('ip_range'))
            .run()
            .then(response => {
                // If IP Address is within range, see if it already exists in Auth table.
                range = rangeCheck.inRange(data.ip, response)
                if (range.match) {
                    rethink
                        .table('Auth')
                        .filter({ ip: data.ip })
                        .filter({ ip_whitelist: true })
                        .run()
                        .then(authExists => {
                            rethink
                                .table('IP_Whitelist')
                                .filter({ ip_range: range.range })
                                .run()
                                .then(resp => {
                                    if (authExists.length === 1) {
                                        checkConfigIds(_.head(authExists), _.head(resp))
                                    } else {
                                        // Else, create a new one
                                        createNewAuth(_.head(resp))
                                    }
                                })
                        })
                } else {
                    cookieAuth()
                }
            })
    })
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth
        let getAuth
        const updateData = {
            location: req.body.location,
            brandId: req.body.brandId,
            softName: req.body.softName,
            auto_auth: req.body.auto_auth
        }

        const updateSpecficId = config => {
            const configId = _.map(config, item => {
                item.auth = item.id === req.body.ref ? !item.auth : item.auth
                return item
            })

            rethink
                .table('Auth')
                .insert(
                    {
                        id: req.body.id,
                        configId
                    },
                    { conflict: 'update' }
                )
                .run()
                .then(response => {
                    if (response.errors) {
                        return res.status(400).send({ error: true, message: response.error })
                    }
                    return res.status(200).send({ error: false, message: response })
                })
                .catch(next)
        }

        const updateAuth = config => {
            if (updateData.auto_auth) {
                _.forEach(getAuth.configId, item => {
                    item.auth = true
                })
            }

            getAuth.location = updateData.location
            getAuth.brandId = updateData.brandId
            getAuth.softName = updateData.softName
            getAuth.auto_auth = updateData.auto_auth

            rethink
                .table('Auth')
                .get(req.body.id)
                .update(getAuth)
                .run()
                .then(response => {
                    if (response.error) {
                        res.send({ error: true, message: response })
                    } else {
                        res.send({ error: false, message: 'Auth updated!' })
                    }
                })
        }

        rethink
            .table('Auth')
            .filter(auth => {
                if (tipsterAuth.role === 'admin') {
                    return auth('id').eq(req.body.id)
                    // if admin
                }
                return auth('id')
                    .eq(req.body.id)
                    .and(
                        rethink
                            .table('Dashboard')
                            .get(auth('configId'))
                            .run()
                            .then(
                                dashboard =>
                                    dashboard.dashboard_brand_id === tipsterAuth.organisation
                            )
                    )
                // filter ID and ensure they have brand access
            })
            .run()
            .then(response => {
                getAuth = _.head(response)

                if (response.errors) {
                    return res.status(400).send({ error: true, message: response.error })
                }
                if (updateData.location || updateData.brandId) {
                    updateAuth()
                } else {
                    updateSpecficId(_.head(response).configId)
                }
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth

        const deleteSpecficId = config => {
            const configId = _.reject(config, item => item.id === req.body.ref)

            rethink
                .table('Auth')
                .insert(
                    {
                        id: req.body.id,
                        configId
                    },
                    { conflict: 'update' }
                )
                .run()
                .then(response => {
                    if (response.errors) {
                        res.status(400).send({ error: true, message: response.error })
                    } else {
                        res.status(200).send({ error: false, message: response })
                    }
                })
                .catch(next)
        }

        const deleteTotalRecord = () => {
            rethink
                .table('Auth')
                .get(req.body.id)
                .delete()
                .run()
                .then(response => {
                    if (response.errors) {
                        res.status(400).send({ error: true, message: response.error })
                    } else {
                        res.status(200).send({ error: false, message: response })
                    }
                })
                .catch(next)
        }

        rethink
            .table('Auth')
            .filter(auth => {
                if (tipsterAuth.role === 'admin') {
                    return auth('id').eq(req.body.id)
                }
                return auth('id')
                    .eq(req.body.id)
                    .and(
                        rethink
                            .table('Dashboard')
                            .get(auth('configId'))
                            .run()
                            .then(
                                dashboard =>
                                    dashboard.dashboard_brand_id === tipsterAuth.organisation
                            )
                    )
                // filter ID and ensure they have brand access
            })
            .run()
            .then(response => {
                if (response.errors) {
                    res.status(400).send({ error: true, message: response.error })
                } else if (_.size(_.head(response).configId) === 1) {
                    deleteTotalRecord()
                } else {
                    deleteSpecficId(_.head(response).configId)
                }
            })
            .catch(next)
    })
