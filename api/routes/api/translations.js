const router = require('express').Router()
const promiseMiddleware = require('../../middleware/promiseMiddleware')
const Translations = require('../../model/Translations')
const Tokens = require('../../model/Tokens')
const thinky = require('../../util/thinky')
const rethink = thinky.r

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get(
        '/',
        promiseMiddleware(async (req, res) => {
            const tokens = await rethink.table('Tokens')
            res.send({ error: false, data: tokens })
        })
    )
    .get(
        '/:token',
        promiseMiddleware(async (req, res) => {
            // query rethink by token
            // w/ joins
            const response = await rethink
                .table('Tokens')
                .get(req.params.token)
                .merge(token => ({
                    translations: rethink
                        .table('Translations')
                        .getAll(token('id'), { index: 'translation_token_id' })
                        .coerceTo('array')
                }))

            res.send({ error: false, data: response })
        })
    )
    .get(
        '/:token/:translation',
        promiseMiddleware(async (req, res) => {
            // get the translation
            res.send({ error: false, data: tokens })
        })
    )
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////

    //Post a token
    .post(
        '/',
        promiseMiddleware(async (req, res) => {
            // Send to Translations
            const token = new Tokens({
                name: req.body.name
            })

            const response = await token.save()

            res.send({
                error: false,
                data: response
            })
        })
    )
    // Post a translation
    .post(
        '/:token',
        promiseMiddleware(async (req, res) => {
            const { body } = req
            const tokenId = req.params.token
            const getName = await rethink.table('Tokens').get(tokenId)('name')
            body.translation_token_id = tokenId
            body.token_name = getName

            const translation = new Translations(body)
            const response = await translation.save()

            res.send({
                error: false,
                data: response
            })
        })
    )
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put(
        '/:token',
        promiseMiddleware(async (req, res) => {
            const translationId = req.params.token

            const putData = req.body

            const response = await rethink
                .table('Tokens')
                .get(translationId)
                .update(putData)
                .run()

            if (response.errors) {
                return res.status(400).send({ error: true, message: response.error })
            }

            return res.status(200).send({ error: false, message: response })
        })
    )
    .put(
        '/:token/:translation',
        promiseMiddleware(async (req, res) => {
            const translationId = req.params.translation

            const putData = req.body

            const response = await rethink
                .table('Translations')
                .get(translationId)
                .update(putData)
                .run()

            if (response.errors) {
                return res.status(400).send({ error: true, message: response.error })
            }

            return res.status(200).send({ error: false, message: response })
        })
    )
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete(
        '/:token',
        promiseMiddleware(async (req, res) => {
            const translationId = req.params.token

            const response = await rethink
                .table('Tokens')
                .get(translationId)
                .delete()
                .run()

            if (response.errors) {
                return res.status(400).send({ error: true, message: response.error })
            }

            return res.status(200).send({ error: false, message: response })
        })
    )
    .delete(
        '/:token/:translation',
        promiseMiddleware(async (req, res) => {
            const translationKey = req.params.translation

            const response = await rethink
                .table('Translations')
                .get(translationKey)
                .delete()
                .run()

            if (response.errors) {
                return res.status(400).send({ error: true, message: response.error })
            }

            return res.status(200).send({ error: false, message: response })
        })
    )
