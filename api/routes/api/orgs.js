const Brand = require('../../model/Brand')
const router = require('express').Router()
const thinky = require('../../util/thinky')
const _ = require('lodash')
const attempt = require('../../util/attempt')
const ss = require('string-similarity')
const verifyUser = require('../../middleware/auth')

const rethink = thinky.r

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', (req, res, next) => {
        const id = req.headers.id
        const tipsterAuth = req.tipsterAuth
        const orgId = tipsterAuth.organisation

        const filters = attempt(JSON.parse, [req.headers.filters], [])
        const pagination = attempt(JSON.parse, [req.headers.pagination], [])
        const search = attempt(JSON.parse, [req.headers.search], {
            query: '',
            type: ''
        })
        const sorting = attempt(JSON.parse, [req.headers.sorting], {
            order: '',
            type: ''
        })

        let order = ''

        if (sorting.order !== '' && sorting.order === 'ascend') {
            order = 'asc'
        } else if (sorting.order !== '' && sorting.order === 'descend') {
            order = 'desc'
        }

        if (id) {
            Promise.all([
                rethink
                    .table('Brand')
                    .filter({ id })
                    .merge(orgRecord => ({
                        rpd_config: rethink
                            .table('Dashboard')
                            .getAll(orgRecord('id'), {
                                index: 'dashboard_brand_id'
                            })
                            .coerceTo('array')
                    }))
                    .merge(orgRecord => ({
                        rpt_config: rethink
                            .table('Config')
                            .getAll(orgRecord('id'), {
                                index: 'config_brand_id'
                            })
                            .coerceTo('array')
                    }))
                    .merge(orgRecord => ({
                        users: rethink
                            .table('Users')
                            .getAll(orgRecord('id'), { index: 'organisation' })
                            .coerceTo('array')
                    }))
                    .merge(orgRecord => ({
                        locations: rethink
                            .table('Locations')
                            .getAll(orgRecord('id'), { index: 'owner' })
                            .coerceTo('array')
                    }))
                    .merge(orgRecord => ({
                        rpd_auth: rethink
                            .table('Auth')
                            .filter(rec =>
                                rec('type')
                                    .eq('RPD')
                                    .and(rec('brandId').eq(orgRecord('id')))
                            )
                            .coerceTo('array')
                    }))
                    .merge(orgRecord => ({
                        rpt_auth: rethink
                            .table('Auth')
                            .filter(rec =>
                                rec('type')
                                    .eq('RPT')
                                    .and(rec('brandId').eq(orgRecord('id')))
                            )
                            .coerceTo('array')
                    }))
                    .run()
            ])
                .then(data => {
                    res.send(_.flatten(data))
                })
                .catch(next)
        } else {
            rethink
                .table('Brand')
                .filter(
                    brand =>
                        tipsterAuth.role === 'admin'
                            ? brand
                            : brand('id').eq(tipsterAuth.organisation)
                )
                .merge(orgRecord => ({
                    rpd_config: rethink
                        .table('Dashboard')
                        .getAll(orgRecord('id'), {
                            index: 'dashboard_brand_id'
                        })
                        .coerceTo('array')
                }))
                .merge(orgRecord => ({
                    rpt_config: rethink
                        .table('Config')
                        .getAll(orgRecord('id'), { index: 'config_brand_id' })
                        .coerceTo('array')
                }))
                .merge(orgRecord => ({
                    users: rethink
                        .table('Users')
                        .getAll(orgRecord('id'), { index: 'organisation' })
                        .coerceTo('array')
                }))
                .merge(orgRecord => ({
                    rpd_auth: rethink
                        .table('Auth')
                        .filter(rec =>
                            rec('type')
                                .eq('RPD')
                                .and(rec('brandId').eq(orgRecord('id')))
                        )
                        .coerceTo('array')
                }))
                .merge(orgRecord => ({
                    rpt_auth: rethink
                        .table('Auth')
                        .filter(rec =>
                            rec('type')
                                .eq('RPT')
                                .and(rec('brandId').eq(orgRecord('id')))
                        )
                        .coerceTo('array')
                }))
                .run()
                .then(response => {
                    let filterData = _.chain(response)
                        .filter(
                            item =>
                                search.query !== ''
                                    ? ss.compareTwoStrings(
                                          search.query.toUpperCase(),
                                          item[search.type].toUpperCase()
                                      ) >= 0.4
                                    : item
                        )
                        .value()

                    const total = filterData.length
                    filterData =
                        sorting.order !== ''
                            ? _.orderBy(filterData, sorting.field, order)
                            : filterData
                    // calc total results for pagintaiton

                    filterData =
                        _.size(pagination) > 0
                            ? filterData.slice(
                                  pagination.pageSize * (pagination.current - 1),
                                  pagination.pageSize * pagination.current
                              )
                            : filterData

                    res.send({
                        data: filterData,
                        total
                    })
                })
                .catch(next)
        }
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            const formData = {
                id: req.body.id,
                brand_name: req.body.brand_name,
                brand_logo: req.body.brand_logo
            }
            const saveBrand = new Brand(formData)

            saveBrand
                .save()
                .then(response => {
                    res.status(201).send({
                        error: false,
                        message: 'New organisation added'
                    })
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            const formData = {
                id: req.body.id,
                brand_name: req.body.brand_name,
                brand_logo: req.body.brand_logo
            }

            rethink
                .table('Brand')
                .get(formData.id)
                .update(formData)
                .run()
                .then(response => {
                    if (response.errors) {
                        res.status(400).send({ error: true, message: response.error })
                    } else {
                        res.status(200).send({ error: false, message: response })
                    }
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            rethink
                .table('Brand')
                .get(req.body.id)
                .delete()
                .run()
                .then(response => {
                    if (response.errors) {
                        res.status(400).send({ error: true, message: response.error })
                    } else {
                        res.status(200).send({ error: false, message: response })
                    }
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
