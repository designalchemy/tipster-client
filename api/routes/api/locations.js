const router = require('express').Router()
const thinky = require('../../util/thinky')
const attempt = require('../../util/attempt')
const Locations = require('../../model/locations')
const verifyUser = require('../../middleware/auth')
const ss = require('string-similarity')
const _ = require('lodash')

const rethink = thinky.r
const saltRounds = 10

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', verifyUser, (req, res, next) => {
        const tipsterAuth = req.tipsterAuth
        const id = req.headers.id
        const filters = attempt(JSON.parse, [req.headers.filters], [])
        const pagination = attempt(JSON.parse, [req.headers.pagination], [])
        const search = attempt(JSON.parse, [req.headers.search], {
            query: '',
            type: ''
        })
        const sorting = attempt(JSON.parse, [req.headers.sorting], {
            order: '',
            type: ''
        })

        let order = ''

        if (sorting.order !== '' && sorting.order === 'ascend') {
            order = 'asc'
        } else if (sorting.order !== '' && sorting.order === 'descend') {
            order = 'desc'
        }

        if (id) {
            rethink
                .table('Locations')
                .getAll(id, { index: 'id' })
                .filter(
                    user =>
                        tipsterAuth.role === 'admin'
                            ? user
                            : user('id').eq(tipsterAuth.organisation)
                )
                .eqJoin('owner', rethink.table('Brand'))
                .without({ right: 'id' })
                .zip()
                .merge(locationRecord => ({
                    rpd_auth: rethink
                        .table('Auth')
                        .filter(rec =>
                            rec('type')
                                .eq('RPD')
                                .and(rec('location').eq(locationRecord('id')))
                        )
                        .coerceTo('array')
                }))
                .merge(locationRecord => ({
                    devices: rethink
                        .table('Auth')
                        .getAll(id, { index: 'location' })
                        .coerceTo('array')
                }))
                .merge(locationRecord => ({
                    rpt_auth: rethink
                        .table('Auth')
                        .filter(rec =>
                            rec('type')
                                .eq('RPT')
                                .and(rec('location').eq(locationRecord('id')))
                        )
                        .coerceTo('array')
                }))
                .run()
                .then(response => {
                    res.send(response)
                })
        } else {
            rethink
                .table('Locations')
                .filter(
                    location =>
                        tipsterAuth.role === 'admin'
                            ? location
                            : location('owner').eq(tipsterAuth.organisation)
                )
                .eqJoin('owner', rethink.table('Brand'))
                .without({ right: 'id' })
                .zip()
                .merge(locationRecord => ({
                    rpd_auth: rethink
                        .table('Auth')
                        .filter(rec =>
                            rec('type')
                                .eq('RPD')
                                .and(rec('location').eq(locationRecord('id')))
                        )
                        .coerceTo('array')
                }))
                .merge(locationRecord => ({
                    rpt_auth: rethink
                        .table('Auth')
                        .filter(rec =>
                            rec('type')
                                .eq('RPT')
                                .and(rec('location').eq(locationRecord('id')))
                        )
                        .coerceTo('array')
                }))
                .run()
                .then(response => {
                    let cities = _.map(
                        _.uniqBy(response, 'city'),
                        item => (item.city !== '' ? item.city : null)
                    )
                    cities = _.compact(cities)

                    let orgs = _.map(
                        _.uniqBy(response, 'brand_name'),
                        item => (item.brand_name !== '' ? item.brand_name : null)
                    )

                    orgs = _.compact(orgs)

                    let filterData = _.chain(response)
                        .filter(
                            item =>
                                _.size(filters.city) > 0
                                    ? _.includes(filters.city, item.city)
                                    : item
                        )
                        .filter(
                            item =>
                                _.size(filters.brand_name) > 0
                                    ? _.includes(filters.brand_name, item.brand_name)
                                    : item
                        )
                        .filter(
                            item =>
                                search.query !== ''
                                    ? ss.compareTwoStrings(
                                          search.query.toUpperCase(),
                                          item[search.type].toUpperCase()
                                      ) >= 0.4
                                    : item
                        )
                        .value()

                    const total = filterData.length

                    filterData =
                        sorting.order !== ''
                            ? _.orderBy(filterData, sorting.field, order)
                            : filterData

                    filterData =
                        _.size(pagination) > 0
                            ? filterData.slice(
                                  pagination.pageSize * (pagination.current - 1),
                                  pagination.pageSize * pagination.current
                              )
                            : filterData

                    const data = {
                        total,
                        cities,
                        orgs,
                        data: filterData
                    }
                    res.send(data)
                })
                .catch(next)
        }
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', verifyUser, (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            const formData = {
                name: req.body.name,
                owner: req.body.owner,
                shop_id: req.body.shop_id,
                address_1: req.body.address_1,
                address_2: req.body.address_2,
                city: req.body.city,
                post_code: req.body.post_code
            }

            const saveLocations = new Locations(formData)

            saveLocations
                .save()
                .then(response => {
                    res.status(201).send({
                        error: false,
                        message: 'Added new location'
                    })
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', verifyUser, (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            const formData = {
                name: req.body.name,
                owner: req.body.owner,
                shop_id: req.body.shop_id,
                address_1: req.body.address_1,
                address_2: req.body.address_2,
                city: req.body.city,
                post_code: req.body.post_code
            }

            rethink
                .table('Locations')
                .get(req.body.id)
                .update(formData)
                .run()
                .then(response => {
                    if (response.errors) {
                        res.status(400).send({ error: true, message: response.error })
                    } else {
                        res.status(200).send({ error: false, message: response })
                    }
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', verifyUser, (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            rethink
                .table('Locations')
                .get(req.body.id)
                .delete()
                .run()
                .then(response => {
                    if (response.errors) {
                        res.status(400).send({ error: true, message: response.error })
                    } else {
                        res.status(200).send({ error: false, message: response })
                    }
                })
                .catch(next)
        } else {
            res.send({ error: true, message: 'Not admin' })
        }
    })
