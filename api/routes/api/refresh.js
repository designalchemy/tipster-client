const moment = require('moment')
const router = require('express').Router()
const thinky = require('../../util/thinky')
const rethink = thinky.r

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            rethink
                .table('Auth')
                .update({ refresh: moment().format() })
                .run()
                .then(data => {
                    res.send({
                        error: false,
                        message: 'All screens have been refreshed'
                    })
                })
                .catch(next)
        } else {
            res.send({
                error: true,
                message: 'You are not admin'
            })
        }
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', (req, res, next) => {})
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', (req, res, next) => {
        if (req.tipsterAuth.role === 'admin') {
            rethink
                .table('Auth')
                .get(req.body.id)
                .update({ refresh: moment().format() })
                .run()
                .then(data => {
                    res.send({
                        error: false,
                        message: 'Refresh Happened'
                    })
                })
                .catch(next)
        } else {
            res.send({
                error: true,
                message: 'You are not admin'
            })
        }
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', (req, res, next) => {})
