const moment = require('moment')
const router = require('express').Router()
const thinky = require('../../util/thinky')
const _ = require('lodash')
const rethink = thinky.r
const assert = require('assert')

const unixDate = time => {
    const date = time != null ? new Date(time) : new Date()
    const epoch = Number(date) / 1000
    assert(!_.isNaN(epoch), `Invalid date ${time}.`)
    return epoch
}

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/organisation/', async (req, res, next) => {
        try {
            const id = req.headers.id
            const interval = parseInt(req.headers.interval)
            const from = req.headers.from ? unixDate(req.headers.from) : 0
            const to = unixDate(req.headers.to)

            let timeFormat

            // Send back timeformat for chart.
            if (interval === 60) {
                timeFormat = 'HH:mm'
            } else if (interval === 3600) {
                timeFormat = 'DD/MM - HH:mm'
            } else {
                timeFormat = 'MM/YYYY'
            }

            const allIds = await rethink
                .table('Dashboard')
                .getAll(id, { index: 'dashboard_brand_id' })
                .map(dash => {
                    return dash('_dashboard_id')
                })
                .run()

            const match = await rethink
                .table('cloudwatch_logs')
                .getAll(...allIds, { index: 'configId' })
                .filter(
                    rethink.row('timestamp').during(rethink.epochTime(from), rethink.epochTime(to))
                )
                .group(
                    rethink
                        .row('timestamp')
                        .toEpochTime()
                        .sub(
                            rethink
                                .row('timestamp')
                                .toEpochTime()
                                .mod(interval)
                        )
                )
                .reduce((a, b) => ({
                    total: a('total').add(b('total')),
                    count: a('count').add(b('count')),
                    timestamp: a('timestamp')
                }))
                .ungroup()
                .map({
                    total: rethink.row('reduction')('total'),
                    count: rethink.row('reduction')('count'),
                    group: rethink.epochTime(rethink.row('group')),
                    time_format: timeFormat
                })
                .run()

            res.send(_.orderBy(match, 'timestamp', 'asc'))
        } catch (e) {
            next(e)
        }
    })
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .get('/location/', async (req, res, next) => {
        try {
            const id = req.headers.id
            const interval = parseInt(req.headers.interval)
            const from = req.headers.from ? unixDate(req.headers.from) : 0
            const to = unixDate(req.headers.to)

            let timeFormat

            // Send back timeformat for chart.
            if (interval === 60) {
                timeFormat = 'HH:mm'
            } else if (interval === 3600) {
                timeFormat = 'DD/MM - HH:mm'
            } else {
                timeFormat = 'MM/YYYY'
            }

            const allIds = await rethink
                .table('Auth')
                .getAll(id, { index: 'location' })
                .map(auth => {
                    return auth('id')
                })
                .run()

            const match = await rethink
                .table('cloudwatch_logs')
                .getAll(...allIds, { index: 'deviceId' })
                .filter(
                    rethink.row('timestamp').during(rethink.epochTime(from), rethink.epochTime(to))
                )
                .group(
                    rethink
                        .row('timestamp')
                        .toEpochTime()
                        .sub(
                            rethink
                                .row('timestamp')
                                .toEpochTime()
                                .mod(interval)
                        )
                )
                .reduce((a, b) => ({
                    total: a('total').add(b('total')),
                    count: a('count').add(b('count')),
                    timestamp: a('timestamp')
                }))
                .ungroup()
                .map({
                    total: rethink.row('reduction')('total'),
                    count: rethink.row('reduction')('count'),
                    group: rethink.epochTime(rethink.row('group')),
                    time_format: timeFormat
                })
                .run()

            res.send(_.orderBy(match, 'timestamp', 'asc'))
        } catch (e) {
            next(e)
        }
    })
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', (req, res, next) => {})
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', (req, res, next) => {})
