const nodemailer = require('nodemailer')
const moment = require('moment')
const router = require('express').Router()
const _ = require('lodash')
const thinky = require('../../util/thinky')
const rethink = thinky.r
const uuidv4 = require('uuid/v4')
const bcrypt = require('bcrypt-nodejs')
const saltRounds = 10

let selfSignedConfig = {
    host: 'email-smtp.eu-west-1.amazonaws.com',
    port: 465,
    secure: true, // use TLS
    auth: {
        user: 'AKIAIZUAELQNAXTJRSRA',
        pass: 'AriDrRpt8K2gGiliC+NntxsI56vFPrXTjxuKpXmrVgpT'
    },
    tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false
    }
}

const transporter = nodemailer.createTransport(selfSignedConfig)

module.exports = router
    ////////////////////////////////////////////////////////////////
    // get
    ////////////////////////////////////////////////////////////////
    .get('/', (req, res, next) => {})
    ////////////////////////////////////////////////////////////////
    // post
    ////////////////////////////////////////////////////////////////
    .post('/', (req, res, next) => {
        const data = {
            email: req.body.email
        }

        rethink
            .table('Users')
            .filter({ email: data.email })
            .update({ resetKey: uuidv4() }, { returnChanges: true })
            .run()
            .then(response => {
                const responseObv = _.get(response, 'changes[0].new_val')

                const message =
                    `Hello ${responseObv.firstName}, \n\r\n\r` +
                    `Please follow the below URL to reset your password \n\r` +
                    `https://tipster.racingpost.com/management/reset-password?id=${responseObv.resetKey} \n\r` +
                    `If you have not requested this, please forward this message to support@racingpost.co.uk \n\r\n\r Thanks`

                const mailOptions = {
                    from: 'application@korelogic.co.uk',
                    to: data.email,
                    subject: 'Reset your password',
                    text: message
                }

                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        res.status(201).send({
                            error: true,
                            message: 'Error',
                            data: error
                        })
                    } else {
                        res.status(201).send({
                            error: false,
                            message: 'success',
                            data: info
                        })
                    }
                })
            })
    })
    ////////////////////////////////////////////////////////////////
    // put
    ////////////////////////////////////////////////////////////////
    .put('/', (req, res, next) => {
        const password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(saltRounds))

        rethink
            .table('Users')
            .filter({ resetKey: req.body.resetKey })
            .update({ password: password, resetKey: uuidv4() })
            .run()
            .then(response => {
                if (response.errors) {
                    res.status(400).send({ error: true, message: response.error })
                } else {
                    res.status(200).send({ error: false, message: response })
                }
            })
            .catch(next)
    })
    ////////////////////////////////////////////////////////////////
    // delete
    ////////////////////////////////////////////////////////////////
    .delete('/', (req, res, next) => {})
