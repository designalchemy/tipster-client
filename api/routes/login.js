const _ = require('lodash')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const router = require('express').Router()
const thinky = require('../util/thinky')
const config = require('config')

const rethink = thinky.r

module.exports = router.post('/', (req, res, next) => {
    const password = req.body.password
    const email = req.body.email

    const updateTime = id => {
        const date = moment().format()

        rethink
            .table('Users')
            .get(id)
            .update({ login: date })
            .run()
            .then(response => logger.info(response))
            .catch(err => logger.error(err))
    }

    rethink
        .table('Users')
        .filter({
            email
        })
        .limit(1)
        .run()
        .then(response => {
            const data = _.head(response)

            if (!data) {
                res.status(418).send({
                    error: true,
                    message: 'email is wrong' // change these to generic
                })
            } else if (!bcrypt.compareSync(password, data.password)) {
                res.status(418).send({
                    error: true,
                    message: 'password is wrong' // change these to generic
                })
            } else {
                const tokenData = {
                    email: data.email,
                    firstName: data.firstName,
                    id: data.id,
                    ip: data.ip,
                    login: data.login,
                    organisation: data.organisation,
                    role: data.role,
                    secondName: data.secondName,
                    skype: data.skype
                }

                const token = jwt.sign(tokenData, config.get('jwtSecret'), {
                    expiresIn: '1d'
                })

                res.status(200).send({
                    error: false,
                    message: `welcome ${email}`,
                    token
                })

                updateTime(_.head(response).id)
            }
        })
        .catch(next)
})
