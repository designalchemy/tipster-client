const express = require('express')
const horizon = require('@horizon/server')
const config = require('config')

// Routes
const authRoutes = require('./routes/api/auth')
const activityRoutes = require('./routes/api/activity')
const dashboardsRoutes = require('./routes/api/dashboards')
const healthcheckRoutes = require('./routes/healthcheck')
const loginRoutes = require('./routes/login')
const orgsRoutes = require('./routes/api/orgs')
const userRoutes = require('./routes/api/users')
const locationsRoutes = require('./routes/api/locations')
const configRoutes = require('./routes/api/config')
const rptRoutes = require('./routes/api/racingPostDisplay')
const accountRoutes = require('./routes/api/account')
const emailRoutes = require('./routes/api/email')
const refreshRoutes = require('./routes/api/refresh')
const ipRoutes = require('./routes/api/ipwhitelisting')
const imagesRoutes = require('./routes/api/images')
const translationsRoutes = require('./routes/api/translations')
const languagesRoutes = require('./routes/api/languages')
const dialoguesRoutes = require('./routes/api/dialogues')

// Middleware
const bodyParser = require('body-parser')
const cors = require('cors')
const verifyUser = require('./middleware/auth')

global.logger = require('../common/logger')('api')

//////////////////////////////////////////////////////////////////////////////////
// Express Server
//////////////////////////////////////////////////////////////////////////////////

const app = express()
const port = process.env.PORT || config.get('port')

const httpServer = app.listen(port, () => {
    logger.info(`Listening on port ${port}.`)
})

//////////////////////////////////////////////////////////////////////////////////
// Logger
//////////////////////////////////////////////////////////////////////////////////

// The internal logger for horizon is Winston (https://github.com/winstonjs/winston)
horizon.logger.level = config.get('loggingLevel')

//////////////////////////////////////////////////////////////////////////////////
// Horizon Server
//////////////////////////////////////////////////////////////////////////////////

// Create horizon server instance.
const horizonServer = horizon(httpServer, config.get('horizon'))

// It is possible to add HTTP endpoints to horizon base endpoint.
// For example, this will match `/horizon/connection_count`.
// I am unsure if req and res objects align with Express, but they don't seem to
horizonServer.add_http_handler('connection_count', (req, res) => {
    const stringifyData = JSON.stringify({
        connections: req.socket.server._connections
    })
    res.writeHead(200, {
        'Content-Type': 'application/json'
    })
    res.end(stringifyData)
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

//////////////////////////////////////////////////////////////////////////////////
// Authentication
//////////////////////////////////////////////////////////////////////////////////

// Add authentication middleware. All routes require authentication by default.
app.use(verifyUser)

// Define routes that don't require authentication.
verifyUser
    .ignore('/horizon')
    .ignore('/healthcheck')
    .ignore('/api/login')
    .ignore('/api/auth', 'POST')
    .ignore('/api/rpt/:id')
    .ignore('/api/rpt')
    .ignore('/api/email')
    .ignore('/api/images')
    .ignore('/api/images/:dir')
    .ignore('/api/images/:dir/:subdir')
    .ignore('/api/images/:dir/:subdir/:subsubdir')
    .ignore('/api/translations')
    .ignore('/api/translations/:token')
    .ignore('/api/languages')
    .ignore('/api/dialogues')
    .ignore('/api/dialogues/:sport/:language')

//////////////////////////////////////////////////////////////////////////////////
// Routes
//////////////////////////////////////////////////////////////////////////////////

app.use('/healthcheck', healthcheckRoutes)
app.use('/api/login', loginRoutes)
app.use('/api/auth', authRoutes)
app.use('/api/dashboard', dashboardsRoutes)
app.use('/api/orgs', orgsRoutes)
app.use('/api/users', userRoutes)
app.use('/api/locations', locationsRoutes)
app.use('/api/config', configRoutes)
app.use('/api/rpt/', rptRoutes)
app.use('/api/account/', accountRoutes)
app.use('/api/email/', emailRoutes)
app.use('/api/refresh/', refreshRoutes)
app.use('/api/activity/', activityRoutes)
app.use('/api/ip/', ipRoutes)
app.use('/api/images/', imagesRoutes)
app.use('/api/translations/', translationsRoutes)
app.use('/api/languages/', languagesRoutes)
app.use('/api/dialogues/', dialoguesRoutes)

//////////////////////////////////////////////////////////////////////////////////
// Generic error handling.
//////////////////////////////////////////////////////////////////////////////////

// If `next` callback is passed an error it will short-circuit to here:
app.use((error, req, res, next) => {
    logger.error(error.stack)
    res.status(500).send({ error: true, message: error.message })
})
