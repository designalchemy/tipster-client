const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Languages = thinky.createModel(
    'Languages',
    {
        name: type.string()
    },
    {
        conflict: 'update',
        pk: 'id'
    }
)

module.exports = Languages
