const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Locations = thinky.createModel(
    'Locations',
    {
        id: type.string().default(uuid.v4),
        owner: type.string(),
        name: type.string(),
        shop_id: type.string(),
        address_1: type.string(),
        address_2: type.string(),
        city: type.string(),
        post_code: type.string()
    },
    {
        conflict: 'update',
        pk: 'id'
    }
)

module.exports = Locations
