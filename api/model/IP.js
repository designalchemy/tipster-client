const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const IP = thinky.createModel(
    'IP_Whitelist',
    {
        id: type.string().default(uuid.v4),
        ip_range: type.string(),
        locationId: type.string(),
        location_softName: type.string(),
        type: type.string(),
        ip_type: type.number(),
        brandId: type.string(),
        auto_auth: type.boolean(),
        brand_softName: type.string(),
        configId: type.array().optional(),
        softName: type.string()
    },
    {
        conflict: 'replace',
        pk: 'id'
    }
)

module.exports = IP
