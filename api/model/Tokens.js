const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Tokens = thinky.createModel(
    'Tokens',
    {
        id: type.string().default(uuid.v4),
        name: type.string()
    },
    {
        conflict: 'update',
        pk: 'id'
    }
)

module.exports = Tokens
