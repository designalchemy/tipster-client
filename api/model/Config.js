const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Config = thinky.createModel(
    'Config',
    {
        id: type.string().default(uuid.v4),
        config_brand_id: type.string(),
        soft_name: type.string(),
        config: type.string()
    },
    {
        conflict: 'replace',
        pk: 'id'
    }
)

module.exports = Config
