const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Dialogues = thinky.createModel(
    'Dialogues',
    {
        id: type.string().default(uuid.v4),
        sport: type.number(),
        language: type.string(),
        key: type.string(),
        items: type.array()
    },
    {
        conflict: 'update',
        pk: 'id'
    }
)

Dialogues.ensureIndex('translation_token_id')

module.exports = Dialogues
