const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Translations = thinky.createModel(
    'Translations',
    {
        id: type.string().default(uuid.v4),
        translation_token_id: type.string(),
        text: type.string(),
        token_name: type.string(),
        language: type.string() // TODO: Index
    },
    {
        conflict: 'update',
        pk: 'id'
    }
)

Translations.ensureIndex('translation_token_id')

module.exports = Translations
