const config = require('config')

module.exports = {
    setupFiles: ['./jest.setup.js'],
    verbose: config.loggingLevel === 'debug',
    modulePathIgnorePatterns: [
        // Don't test test environment config
        'config/test.js'
    ],
    transform: {
        '^.+\\.jsx$': 'babel-jest',
        '^.+\\.js$': 'babel-jest'
    },
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            '<rootDir>/__mocks__/fileMock.js',
        '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js'
    },
    moduleDirectories: ['node_modules', 'client-management']
}
