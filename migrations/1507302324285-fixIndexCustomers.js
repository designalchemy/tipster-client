'use strict'
const r = require('../api/util/thinky').r
exports.up = async function(next) {
    try {
        const configIndexes = await r
            .table('Config')
            .indexList()
            .run()

        const dashboardIndexes = await r
            .table('Dashboard')
            .indexList()
            .run()

        const usersIndexes = await r
            .table('Users')
            .indexList()
            .run()

        if (!dashboardIndexes.includes('dashboard_brand_id')) {
            await r
                .table('Dashboard')
                .indexCreate('dashboard_brand_id')
                .run()
        }

        if (!configIndexes.includes('config_brand_id')) {
            await r
                .table('Config')
                .indexCreate('config_brand_id')
                .run()
        }

        if (!usersIndexes.includes('organisation')) {
            await r
                .table('Users')
                .indexCreate('organisation')
                .run()
        }

        next()
    } catch (e) {
        next(e)
    }
}

exports.down = function(next) {
    next()
}
