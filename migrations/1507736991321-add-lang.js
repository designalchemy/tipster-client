const r = require('../api/util/thinky').r

exports.up = async function(next) {
    try {
        await r
            .table('Dashboard')
            .update({
                config: {
                    lang: 'en'
                }
            })
            .run()

        next()
    } catch (e) {
        next(e)
    }
}

exports.down = function(next) {
    next()
}
