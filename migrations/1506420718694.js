const r = require('../api/util/thinky').r

const name = 'IP_Whitelist'

exports.up = function(next) {
    r
        .tableList()
        .run()
        .then(tables => {
            if (!tables.includes('IP_Whitelist')) {
                return r.tableCreate('IP_Whitelist', { primaryKey: 'id' }).run()
            }
        })
        .then(() => next())
        .catch(next)
}

exports.down = function(next) {
    next()
}
