const r = require('../api/util/thinky').r

exports.up = function(next) {
    r
        .table('Dashboard')
        .update({
            config: {
                clock_is_24_hour: true,
                race_time_is_24_hour: false,
                next_race_time_is_24_hour: false
            }
        })
        .run()
        .then(() => {
            next()
        })
}

exports.down = function(next) {
    next()
}
