'use strict'

const r = require('../api/util/thinky').r

const tablename = 'hz_client_connections'

exports.up = async function(next) {
    try {
        const tableList = await r.tableList().run()

        if (tableList.includes('Acts')) {
            await r
                .table('Acts')
                .delete()
                .run()
        }

        next()
    } catch (e) {
        next(e)
    }
}

exports.down = function(next) {
    next()
}
