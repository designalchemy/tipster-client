'use strict'

const r = require('../api/util/thinky').r

exports.up = function(next) {
    r
        .table('hz_client_connections')
        .indexList()
        .run()
        .then(indexs => {
            if (!indexs.includes('uuid')) {
                return r.table('hz_client_connections').indexCreate('uuid').run()
            }
        })
        .then(() => next())
        .catch(next)
}

exports.down = function(next) {
    next()
}
