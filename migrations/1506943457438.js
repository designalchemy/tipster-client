const r = require('../api/util/thinky').r

exports.up = function(next) {
    r
        .table('Auth')
        .update({ ip_whitelist: false })
        .run()
        .then(() => {
            next()
        })
}

exports.down = function(next) {
    next()
}
