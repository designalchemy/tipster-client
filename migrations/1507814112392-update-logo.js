const r = require('../api/util/thinky').r

exports.up = async function(next) {
    try {
        const dashboard = await r
            .table('Dashboard')
            .limit(1)
            .run()

        if (dashboard[0].config.logo.includes('.png')) {
            return next()
        }

        await r
            .table('Dashboard')
            .update({
                config: {
                    logo: r.row('config')('logo').add('.png'),
                    bet_prompt_offer_logo: r.row('config')('bet_prompt_offer_logo').add('.png'),
                    dashboard_mascot: r.row('config')('dashboard_mascot').add('.svg')
                }
            })
            .run()

        next()
    } catch (e) {
        next(e)
    }
}

exports.down = function(next) {
    next()
}
