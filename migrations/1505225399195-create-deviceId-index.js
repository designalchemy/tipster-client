'use strict'
const r = require('../api/util/thinky').r

exports.up = async function(next) {
    try {
        const indexes = await r
            .table('cloudwatch_logs')
            .indexList()
            .run()

        if (!indexes.includes('deviceId')) {
            await r
                .table('cloudwatch_logs')
                .indexCreate('deviceId')
                .run()
        }

        next()
    } catch (e) {
        next(e)
    }
}

exports.down = function(next) {
    next()
}
