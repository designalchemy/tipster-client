'use strict'

const r = require('../api/util/thinky').r

const tablename = 'hz_client_connections'

exports.up = async function(next) {
    try {
        await r
            .table('Dashboard')
            .update({
                config: {
                    text_timing: '3000'
                }
            })
            .run()

        next()
    } catch (e) {
        next(e)
    }
}

exports.down = function(next) {
    next()
}
