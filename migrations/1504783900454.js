'use strict'
const r = require('../api/util/thinky').r
exports.up = function(next) {
    r
        .table('Locations')
        .indexList()
        .run()
        .then(indexs => {
            if (!indexs.includes('owner')) {
                return r
                    .table('Locations')
                    .indexCreate('owner')
                    .run()
            }
        })
        .then(() => next())
        .catch(next)
}

exports.down = function(next) {
    next()
}
