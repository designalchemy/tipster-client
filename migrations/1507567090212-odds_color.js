const r = require('../api/util/thinky').r

exports.up = function(next) {
    r
        .table('Dashboard')
        .update({ config: { odds_color: '#e12a20' } })
        .run()
        .then(() => {
            next()
        })
}

exports.down = function(next) {
    next()
}
