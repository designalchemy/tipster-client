const r = require('../api/util/thinky').r

exports.up = function(next) {
    r
        .table('Dashboard')
        .update({ config: { price_source: 'guide_diffusion' } })
        .run()
        .then(() => {
            next()
        })
}

exports.down = function(next) {
    next()
}
