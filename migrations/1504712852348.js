'use strict'

const r = require('../api/util/thinky').r

exports.up = function(next) {
    r
        .table('Dashboard')
        .update({ config: { show_prices: true } })
        .run()
        .then(() => next())
        .catch(next)
}

exports.down = function(next) {
    next()
}
