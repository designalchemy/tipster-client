'use strict'

const r = require('../api/util/thinky').r

const tablename = 'hz_client_connections'

exports.up = async function(next) {
    try {
        await r
            .table('Dashboard')
            .filter({ config: { sport: '1' } })
            .update({
                config: {
                    welome_prompt_text:
                        'Welcome to <strong>{{course_style_name}}</strong> for the <strong>{{race_instance_title}}</strong>. There are <strong>{{no_of_runners}}</strong> runners{{till_race_time}}'
                }
            })
            .run()

        await r
            .table('Dashboard')
            .filter({ config: { sport: '2' } })
            .update({
                config: {
                    welome_prompt_text:
                        'Hi welcome to the <strong>{{race_datetime}}</strong> at <strong>{{track_name}}</strong>. Heres the racecard and latest betting'
                }
            })
            .run()

        next()
    } catch (e) {
        next(e)
    }
}

exports.down = function(next) {
    next()
}
