const webpack = require('webpack')
const DashboardPlugin = require('webpack-dashboard/plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')
const config = require('config')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const BUILD_DIR = path.resolve(__dirname, 'public/')

const webpackConfig = {
    production: {
        entry: './app/main.js',
        output: {
            path: BUILD_DIR,
            filename: '[hash].js',
            chunkFilename: '[chunkhash].js',
            publicPath: '/management'
        },
        resolve: {
            extensions: ['.js', '.jsx'],
            alias: {
                components: path.resolve(__dirname, './app/components/'),
                tools: path.resolve(__dirname, './app/tools')
            }
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    loader: ['babel-loader']
                },
                {
                    test: /\.(scss|css)$/,
                    loaders: ['style-loader', 'css-loader', 'sass-loader']
                },
                {
                    test: /\.(less)$/,
                    loaders: ['style-loader', 'css-loader', 'less-loader']
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: ['url-loader?limit=10000', 'img-loader']
                }
            ]
        },
        plugins: [
            new CopyWebpackPlugin([{ from: './app/assets', to: 'assets' }]),
            new webpack.SourceMapDevToolPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production')
                }
            }),
            new webpack.optimize.ModuleConcatenationPlugin(),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: './index.hbs'
            }),
            new webpack.optimize.UglifyJsPlugin()
        ]
    },

    development: {
        entry: './client-management/app/main.js',
        output: {
            path: BUILD_DIR,
            filename: '[hash].js',
            chunkFilename: '[chunkhash].js',
            publicPath: '/'
        },
        resolve: {
            extensions: ['.js', '.json', '.jsx'],
            alias: {
                components: path.resolve(__dirname, './app/components/'),
                tools: path.resolve(__dirname, './app/tools')
            }
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    loader: ['babel-loader']
                },
                {
                    test: /\.(scss|css)$/,
                    loaders: ['style-loader', 'css-loader', 'sass-loader']
                },
                {
                    test: /\.(less)$/,
                    loaders: ['style-loader', 'css-loader', 'less-loader']
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: ['url-loader?limit=10000', 'img-loader']
                }
            ]
        },
        plugins: [
            new CopyWebpackPlugin([
                {
                    from: './client-management/app/assets',
                    to: 'management/assets'
                }
            ]),
            new webpack.SourceMapDevToolPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('development')
                }
            }),
            new webpack.optimize.ModuleConcatenationPlugin(),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: 'client-management/index.hbs'
            })
        ]
    }
}

module.exports = webpackConfig[config.get('webpack')]
