# Folder structure:



```
app/
	assets/ 			*images etc* access via < src="/assets/xx.png" />
	components/ 		react components, will later be split into clever / dumb*
	components/____ 	naming convention of folder + .jsx + .less file is the same
	styles/				custom.less = theme over rides, var.less our vars, colors.less default colors, but these are pulled int our vars

```


```
main.js 		= dom render
routes.js 		= routing
App.jsx 		= data layer after the router + main presenter
Index 		 	= current index (dashboard) copy this as a example component and either use the router to navigate to new components (copy example) or use components as normal
```


#Starting the App: 

1. Install Database

	- install rethinkDB
	- run rethinkDB


2. Install server

	- install server
	- yarn start server
	- run 'yarn seed:dev'


3. Install backoffice

	- yarn start
	- nav to localhost:3000




#Build the App:

```
to do
```

