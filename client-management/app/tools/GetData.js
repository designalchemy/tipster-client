import createBrowserHistory from 'history/createBrowserHistory'

const browserHistory = createBrowserHistory()

class GetData {
    getHeaders(type, headers) {
        return {
            method: type || 'GET',
            headers: {
                'x-access-token': localStorage.getItem('backoffice-jwt'),
                'Content-type': 'application/json; charset=utf-8',
                ...headers
            }
        }
    }

    getBody(body) {
        return {
            body: JSON.stringify(body)
        }
    }

    getOptions(type, body) {
        if (type === 'GET' && body) {
            return {
                ...this.getHeaders(type, body)
            }
        }
        return {
            ...this.getHeaders(type),
            ...this.getBody(body)
        }
    }

    callApi(url, options) {
        return new Promise((resolve, reject) => {
            fetch(url, options)
                .then(res => {
                    if (res.status === 401 || res.status === 403) {
                        browserHistory.push('/management/login#error')
                        browserHistory.go('/management/login#error')
                    } else {
                        return res.json()
                    }
                })
                .then(json => {
                    resolve(json)
                })
                .catch(err => reject(err))
        })
    }

    // /////////////////////////////////////////////////////////////
    // Auth
    // /////////////////////////////////////////////////////////////

    auth(type, body) {
        const url = '/api/auth'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // Users
    // /////////////////////////////////////////////////////////////

    users(type, body) {
        const url = '/api/users'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // orgs
    // /////////////////////////////////////////////////////////////

    orgs(type, body) {
        const url = '/api/orgs'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // IP
    // /////////////////////////////////////////////////////////////

    IP(type, body) {
        const url = '/api/ip'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // Location
    // /////////////////////////////////////////////////////////////

    locations(type, body) {
        const url = '/api/locations'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // dashboard
    // /////////////////////////////////////////////////////////////

    dashboard(type, body) {
        const url = '/api/dashboard'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // config
    // /////////////////////////////////////////////////////////////

    config(type, body) {
        const url = '/api/config'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // user
    // /////////////////////////////////////////////////////////////

    account(type, body) {
        const url = '/api/account'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // organisation
    // /////////////////////////////////////////////////////////////

    activityOrg(type, body) {
        const url = '/api/activity/organisation'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }
    // /////////////////////////////////////////////////////////////
    // locations
    // /////////////////////////////////////////////////////////////

    activityLocation(type, body) {
        const url = '/api/activity/location'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // email
    // /////////////////////////////////////////////////////////////

    email(type, body) {
        const url = '/api/email'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    // /////////////////////////////////////////////////////////////
    // refresh
    // /////////////////////////////////////////////////////////////

    refesh(type, body) {
        const url = '/api/refresh'
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    ///////////////////////////////////////////////////////////////
    // images
    ///////////////////////////////////////////////////////////////

    images(type, body, dir) {
        const url = `/api/images${dir}`
        const options = this.getOptions(type, body)
        return this.callApi(url, options)
    }

    ///////////////////////////////////////////////////////////////
    // translations
    ///////////////////////////////////////////////////////////////

    getTokens() {
        const url = '/api/translations'
        const options = this.getOptions('GET')
        return this.callApi(url, options)
    }

    postToken(body) {
        const url = '/api/translations'
        const options = this.getOptions('POST', body)
        return this.callApi(url, options)
    }

    putToken(tokenId, body) {
        const url = `/api/translations/${tokenId}`
        const options = this.getOptions('PUT', body)
        return this.callApi(url, options)
    }

    deleteToken(tokenId) {
        const url = `/api/translations/${tokenId}`
        const options = this.getOptions('DELETE')
        return this.callApi(url, options)
    }

    getTranslations(tokenId) {
        const url = `/api/translations/${tokenId}`
        const options = this.getOptions('GET')
        return this.callApi(url, options)
    }

    postTranslation(tokenId, body) {
        const url = `/api/translations/${tokenId}`
        const options = this.getOptions('POST', body)
        return this.callApi(url, options)
    }

    putTranslation(tokenId, translationId, body) {
        const url = `/api/translations/${tokenId}/${translationId}`
        const options = this.getOptions('PUT', body)
        return this.callApi(url, options)
    }

    deleteTranslation(tokenId, translationId) {
        const url = `/api/translations/${tokenId}/${translationId}`
        const options = this.getOptions('DELETE')
        return this.callApi(url, options)
    }

    getDialogues(sport, language) {
        const url = `/api/dialogue/${sport}/${language}`
        const options = this.getOptions('GET')
        return this.callApi(url, options)
    }

    postDialogue(sport, language, body) {
        const url = `/api/dialogue/${sport}/${language}`
        const options = this.getOptions('POST', body)
        return this.callApi(url, options)
    }

    putDialogue(sport, language, body) {
        const url = `/api/dialogue/${sport}/${language}`
        const options = this.getOptions('PUT', body)
        return this.callApi(url, options)
    }

    deleteDialogue(sport, language) {
        const url = `/api/dialogue/${sport}/${language}`
        const options = this.getOptions('DELETE')
        return this.callApi(url, options)
    }

    ///////////////////////////////////////////////////////////////
    // languages
    ///////////////////////////////////////////////////////////////
    getLanguages(type, body) {
        const url = '/api/languages'
        const options = this.getOptions('GET')
        return this.callApi(url, options)
    }
}

const exportData = new GetData()
export default exportData
