// sorts strings alphabetically

const sortAlpha = (a, b) => {
    const x = a ? String(a).toLowerCase() : ''
    const y = b ? String(b).toLowerCase() : ''
    if (x < y) return -1
    if (x > y) return 1
    return 0
}

const containsNumber = string => /\d/.test(string) && string.length > 8

const strongPass = (rule, value, callback) => {
    if (!containsNumber(value)) {
        callback('Must contain 8 characters and at least 1 number.')
    } else {
        callback()
    }
}

const jsonValidator = (rule, value, callback) => {
    try {
        JSON.parse(value)
    } catch (error) {
        callback('Not valid JSON - https://jsonlint.com/ check here')
    }

    callback()
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
    }
}

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 14,
            offset: 4
        }
    }
}

const ipRanges = {
    '1': '/32',
    '2': '/31',
    '4': '/30',
    '8': '/29',
    '16': '/28',
    '32': '/27',
    '64': '/26',
    '128': '/25',
    '256': '/24',
    '512': '/23',
    '1024': '/22',
    '2048': '/21'
}

const proccessFilePath = item => {
    return item.split(/(\\|\/)/g).pop()
}

export {
    sortAlpha,
    containsNumber,
    strongPass,
    formItemLayout,
    tailFormItemLayout,
    jsonValidator,
    ipRanges,
    proccessFilePath
}
