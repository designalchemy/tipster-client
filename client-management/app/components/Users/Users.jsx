import React, { Component } from 'react'
import { Table, Icon, Button, Row, Col, Popconfirm, message, Input } from 'antd'
import ModalSwitcher from '../Modals/ModalSwitcher'
import { observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import GetData from '../../tools/GetData'
import moment from 'moment'
import queryString from 'query-string'
import { sortAlpha } from '../../tools/utils'

import css from './Users.less'

@inject('globalStore')
@observer
class Users extends Component {
    @observable
    pagination = {
        pageSize: 50,
        total: 0,
        current: 1
    }
    @observable filters = {}
    @observable
    sorting = {
        order: 'descend',
        field: 'firstName'
    }

    @observable
    search = {
        query: '',
        type: ''
    }

    @observable
    emailFilter = {
        visible: false,
        search: ''
    }

    @observable
    nameFilter = {
        visible: false,
        search: ''
    }

    @observable addUserModal = false
    @observable editUserModal = false
    @observable editUserData = ''
    @observable loading = true

    componentDidMount() {
        this.getData()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location.search !== this.props.location.search) {
            this.getData()
        }
    }

    getData = () => {
        this.addUserModal = false
        this.editUserData = ''
        this.editUserModal = false

        const parsed = queryString.parse(this.props.location.search)

        GetData.users('GET', {
            pagination: JSON.stringify(this.pagination),
            filters: JSON.stringify(this.filters),
            search: JSON.stringify(this.search),
            sorting: JSON.stringify(this.sorting)
        }).then(json => {
            let jsonCopy = json
            if (parsed.company)
                jsonCopy = _.filter(jsonCopy, item => item.organisation === parsed.company)
            this.handleResponse(jsonCopy, true)
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.pagination.total = json.total
                this.userData = json.data
                this.orgs = json.orgs
                this.loading = false
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    deleteUser = id => {
        this.loading = false

        GetData.users('DELETE', { id }).then(json => {
            this.handleResponse(json)
        })
    }

    onChange = (pagination, filters, sorter) => {
        this.loading = true
        this.sorting.order = sorter.order
        this.sorting.field = sorter.field
        this.pagination = pagination
        this.filters = filters
        this.getData()
    }

    onInputChange = (e, type) => {
        this[type].search = e.target.value
    }

    onSearch = (type, key) => {
        this.loading = true
        this[key].visible = false
        this.search.query = this[key].search
        this.search.type = type
        this.getData()
    }

    launchModal = data => {
        this.editUserData = data
        this.editUserModal = true
    }

    render() {
        const { role } = this.props.globalStore.user
        let orgs = this.orgs || {}

        const columns = [
            {
                title: 'Name',
                dataIndex: 'firstName',
                sorter: true,
                render: (text, record) => (
                    <span>
                        {text} {record.secondName}
                    </span>
                ),
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.nameFilterInput = ele)}
                            placeholder="Search name"
                            value={this.nameFilter.search}
                            onChange={event => this.onInputChange(event, 'nameFilter')}
                            onPressEnter={() => this.onSearch('firstName', 'nameFilter')}
                        />
                        <Button
                            type="primary"
                            onClick={() => this.onSearch('firstName', 'nameFilter')}
                        >
                            Search
                        </Button>
                    </div>
                ),
                filterIcon: <Icon type="zoom-1" />,
                filterDropdownVisible: this.nameFilter.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.nameFilter.visible = visible
                }
            },
            {
                title: 'Email',
                dataIndex: 'email',
                sorter: true,
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.emailFilterInput = ele)}
                            placeholder="Search name"
                            value={this.emailFilter.search}
                            onChange={event => this.onInputChange(event, 'emailFilter')}
                            onPressEnter={() => this.onSearch('email', 'emailFilter')}
                        />
                        <Button
                            type="primary"
                            onClick={() => this.onSearch('email', 'emailFilter')}
                        >
                            Search
                        </Button>
                    </div>
                ),
                filterIcon: <Icon type="zoom-1" />,
                filterDropdownVisible: this.emailFilter.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.emailFilter.visible = visible
                }
            },
            {
                title: 'Organisation',
                dataIndex: 'brand_name',
                sorter: true,
                filters: _.map(orgs, item => ({ text: item, value: item })),
                onFilter: (value, record) => record.brand_name === value
            },
            {
                title: 'Role',
                dataIndex: 'role',
                sorter: true
            },
            {
                title: 'IP',
                dataIndex: 'ip'
            },
            {
                title: 'Last Login',
                dataIndex: 'login',
                sorter: true,
                render: text => <span>{moment(text).format('HH:mm DD-MM-YYYY')}</span>
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        {role === 'admin' && (
                            <span>
                                <a
                                    href="#"
                                    onClick={e => {
                                        e.preventDefault()
                                        this.launchModal(record)
                                    }}
                                >
                                    Edit <Icon type="pencil-1" />
                                </a>

                                <span className="ant-divider" />

                                <Popconfirm
                                    title="Are you sure delete this User?"
                                    onConfirm={() => this.deleteUser(record.id)}
                                    placement="left"
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <a href="#">
                                        Delete <Icon type="trash-1" />
                                    </a>
                                </Popconfirm>

                                <span className="ant-divider" />
                            </span>
                        )}

                        <a href={`mailto:${record.email}`}>
                            Email <Icon type="email-1" />
                        </a>
                    </span>
                )
            }
        ]

        return (
            <div>
                <ModalSwitcher
                    title="Add New User"
                    modal="EditUser"
                    showModal={this.addUserModal}
                    closeModal={() => this.getData()}
                />
                <ModalSwitcher
                    title="Edit User"
                    modal="EditUser"
                    data={this.editUserData}
                    showModal={this.editUserModal}
                    closeModal={() => this.getData()}
                />
                <Row type="flex" justify="space-between">
                    <h1>Users</h1>

                    {role === 'admin' && (
                        <Button type="primary" onClick={() => (this.addUserModal = true)}>
                            Add New User
                        </Button>
                    )}
                </Row>
                <div className="table__body-container">
                    <Table
                        columns={columns}
                        dataSource={this.userData}
                        onChange={this.onChange}
                        loading={this.loading}
                        pagination={this.pagination}
                        rowKey={record => record.id}
                        onRowDoubleClick={row => this.launchModal(row)}
                    />
                </div>
            </div>
        )
    }
}

export default Users
