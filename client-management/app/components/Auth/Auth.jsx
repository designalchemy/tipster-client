import React, { Component } from 'react'
import {
    Table,
    Icon,
    Button,
    Row,
    Popconfirm,
    Switch,
    message,
    Alert,
    Input,
    Popover,
    Tooltip
} from 'antd'
import { Link } from 'react-router-dom'
import ModalSwitcher from '../Modals/ModalSwitcher'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import { observable, toJS } from 'mobx'
import GetData from '../../tools/GetData'
import queryString from 'query-string'
import { sortAlpha } from '../../tools/utils'
import css from './Auth.less'

@inject('globalStore')
@observer
class Auth extends React.Component {
    @observable
    pagination = {
        pageSize: 50,
        total: 0,
        current: 1
    }

    @observable
    search = {
        query: '',
        type: ''
    }

    @observable
    editAuth = {
        modal: false,
        data: {}
    }

    @observable
    IPFilter = {
        visible: false,
        search: ''
    }

    @observable filters = {}
    @observable
    sorting = {
        order: 'ascend',
        field: 'configAuth'
    }

    @observable loading = true

    @observable filteredInfo = null
    @observable sortedInfo = null
    @observable locations = []

    @observable tableData = []
    @observable dashboard = []
    @observable orgs = []

    componentDidMount() {
        this.getData()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location.search !== this.props.location.search) {
            this.getData()
        }
    }

    getData = () => {
        this.editAuth.modal = false
        this.editAuth.data = {}
        const parsed = queryString.parse(this.props.location.search)

        GetData.dashboard()
            .then(json => {
                this.dashboard = json
                return GetData.auth('GET', {
                    pagination: JSON.stringify(this.pagination),
                    filters: JSON.stringify(this.filters),
                    search: JSON.stringify(this.search),
                    sorting: JSON.stringify(this.sorting)
                })
            })
            .then(json => {
                const jsonCopy = json
                if (parsed.location)
                    jsonCopy.data = _.filter(
                        jsonCopy.data,
                        item => item.location === parsed.location
                    )
                if (parsed.type)
                    jsonCopy.data = _.filter(jsonCopy.data, item => item.type === parsed.type)
                if (parsed.org)
                    jsonCopy.data = _.filter(jsonCopy.data, item => item.brandId === parsed.org)
                // URL filter, parse query strings then check if they are present and filter data based on string

                this.handleResponse(jsonCopy, true)
            })

        GetData.orgs('GET').then(json => {
            this.orgs = json.data
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error('Error getting data, make sure you are logged in', 5)
        } else {
            if (load) {
                this.pagination.total = json.total
                this.tableData = json.data
                this.locations = json.locations
                this.loading = false
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    authUser = record => {
        const body = {
            id: record.id,
            ref: record.configRef,
            auth: !record.configAuth
        }
        if (record.softName === '' || record.location === '' || record.brandId === '') {
            message.error('Please a soft name, location and customer before authorising')
            this.launchModal(record, true)
        } else {
            this.loading = true

            GetData.auth('PUT', body).then(json => {
                this.handleResponse(json)
            })
        }
    }

    deleteAuth = (id, ref) => {
        this.loading = true

        GetData.auth('DELETE', { id, ref }).then(json => {
            this.handleResponse(json)
        })
    }

    clearFilters = () => {
        this.filteredInfo = null
    }

    clearAll = () => {
        this.filteredInfo = null
        this.sortedInfo = null
    }

    closeModal = () => {
        this.editAuth.modal = false
        this.editAuth.data = {}
    }

    onChange = (pagination, filters, sorter) => {
        if (JSON.stringify(pagination) === JSON.stringify(this.pagination)) {
            this.sorting.order = sorter.order
            this.sorting.field = sorter.field
        }
        this.loading = true
        this.pagination = pagination
        this.filters = filters
        this.getData()
    }

    onInputChange = (e, type) => {
        this[type].search = e.target.value
    }

    onSearch = (type, key) => {
        this.loading = true
        this[key].visible = false
        this.search.query = this[key].search
        this.search.type = type
        this.getData()
    }

    launchModal = (data, authAfterSubmit) => {
        data.authAfter = authAfterSubmit
        this.editAuth.data = data
        this.editAuth.modal = true
    }

    confirmRefresh = () => {
        GetData.refesh('GET').then(json => {
            message.success(json.message)
        })
    }

    render() {
        let { sortedInfo, locations } = this
        const { role } = this.props.globalStore.user

        locations = locations || {}

        const columns = [
            {
                title: 'Online',
                dataIndex: 'active',
                sorter: true,
                render: (text, record) => (
                    <span className={`table-icon table-icon__${String(text)}`} />
                )
            },
            {
                title: 'Soft Name',
                dataIndex: 'softName',
                sorter: true
            },
            {
                title: 'Auth Code',
                dataIndex: 'id',
                render: (text, record) => (
                    <Tooltip title={text}>
                        <span className="tool-tip">
                            {_.size(text) < 1 ? '' : String(text).substring(0, 8)}{' '}
                            {_.size(text) >= 8 ? '...' : ''}
                        </span>
                    </Tooltip>
                )
            },
            {
                title: 'Location Name',
                dataIndex: 'name',
                sorter: true
            },
            {
                title: 'Post Code',
                dataIndex: 'post_code',
                filters:
                    _.size(locations) > 0
                        ? _.map(locations, item => ({
                              text: item,
                              value: item
                          }))
                        : false,
                render: (text, record) => (
                    <Tooltip
                        title={`${record.address_1}, ${record.address_2}, ${record.city}, ${record.post_code}`}
                    >
                        <span className="tool-tip">{text}</span>
                    </Tooltip>
                )
            },
            {
                title: 'Config ID',
                dataIndex: 'configRef',
                sorter: true,
                render: (text, record) => (
                    <Tooltip title={text}>
                        <span className="tool-tip">{record.configSoftName}</span>
                    </Tooltip>
                )
            },
            {
                title: 'Config Org',
                dataIndex: 'configOwnerId',
                sorter: true,
                render: text => (
                    <span>{_.get(_.find(this.orgs, ['id', text]), ['brand_name'])}</span>
                )
            },
            {
                title: 'Type',
                dataIndex: 'type',
                sorter: true
            },
            {
                title: 'IP',
                dataIndex: 'ip',
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => {
                                this.searchIPInput = ele
                            }}
                            placeholder="Search name"
                            value={this.IPFilter.search}
                            onChange={event => this.onInputChange(event, 'IPFilter')}
                            onPressEnter={() => this.onSearch('ip', 'IPFilter')}
                        />
                        <Button type="primary" onClick={() => this.onSearch('ip', 'IPFilter')}>
                            Search
                        </Button>
                    </div>
                ),
                filterIcon: <Icon type="zoom-1" />,
                filterDropdownVisible: this.IPFilter.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.IPFilter.visible = visible
                },
                render: (text, record) => {
                    let content = record.browser
                    let title = record.os
                    if (record.ip_whitelist) {
                        title = 'IP Whitelist'
                        content = `Range: ${record.ip_range}`
                    }
                    return (
                        <Tooltip title={`${title} - ${content}`}>
                            <span className="tool-tip">{text}</span>
                        </Tooltip>
                    )
                }
            },
            {
                title: 'Last Login',
                dataIndex: 'lastLogin',
                sorter: true,
                render: text => <span>{moment(text).format('HH:mm DD-MM-YYYY')}</span>
            },
            {
                title: 'Authenticated',
                dataIndex: 'configAuth',
                sorter: true,
                render: (text, record) => {
                    if (record.ip_whitelist) {
                        return (
                            <Link to={`/management/ipwhitelist#${record.ip_range}`}>
                                IP Whitelisted
                            </Link>
                        )
                    } else if (record.auto_auth) {
                        return <span style={{ color: '#0F86DD' }}>Auto Auth</span>
                    }
                    return (
                        <span>
                            <Switch
                                checked={text || record.auto_auth}
                                onChange={() => this.authUser(record)}
                            />
                        </span>
                    )
                },
                filters: [{ text: 'True', value: true }, { text: 'False', value: false }]
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a
                            href="#"
                            onClick={e => {
                                e.preventDefault()
                                this.launchModal(record)
                            }}
                        >
                            Edit <Icon type="pencil-1" />
                        </a>

                        {role === 'admin' && (
                            <span>
                                <span className="ant-divider" />

                                <Popconfirm
                                    title="Are you sure you want to delete this record?"
                                    onConfirm={() => this.deleteAuth(record.id, record.configRef)}
                                    placement="left"
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <a href="#">
                                        Delete <Icon type="trash-1" />
                                    </a>
                                </Popconfirm>
                            </span>
                        )}
                    </span>
                )
            }
        ]

        return (
            <div className="auth__container">
                <ModalSwitcher
                    title="Device Settings"
                    data={this.editAuth.data}
                    modal="EditAuth"
                    showModal={this.editAuth.modal}
                    closeModal={this.closeModal}
                    callback={() => this.getData()}
                />

                <Row type="flex" justify="space-between">
                    <h1>Authorise Hardware</h1>
                    {role === 'admin' && (
                        <Popconfirm
                            title="This will refresh every screen, are you sure you wish to proceed?"
                            onConfirm={() => this.confirmRefresh()}
                            okText="Yes, reset all screens"
                            cancelText="Cancel"
                            placement="bottomRight"
                        >
                            <Button type="danger">Refresh all Screens</Button>
                        </Popconfirm>
                    )}
                </Row>

                <div className="table__body-container">
                    <Table
                        columns={columns}
                        dataSource={toJS(this.tableData)}
                        onChange={this.onChange}
                        loading={this.loading}
                        pagination={this.pagination}
                        rowKey={(record, i) => `${record.id + i}`}
                        onRowDoubleClick={row => this.launchModal(row)}
                    />
                </div>
            </div>
        )
    }
}

export default Auth
