import React, { Component } from 'react'
import { Table, Icon, Button, Row, Popconfirm, message, Col, Popover, Select } from 'antd'
import { Link } from 'react-router-dom'
import GetData from 'tools/GetData'
import ModalSwitcher from '../Modals/ModalSwitcher'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import { observable, toJS } from 'mobx'
import { sortAlpha } from '../../tools/utils'
import css from './Organisation.less'
import createG2 from 'g2-react'
import { Stat } from 'g2'

const Option = Select.Option

const Line = createG2(chart => {
    function formatBytes(bytes) {
        if (bytes == 0) return '0 Bytes'
        let kilobytes = 1024,
            decimalPoint = 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            index = Math.floor(Math.log(bytes) / Math.log(kilobytes))
        return `${parseFloat((bytes / Math.pow(kilobytes, index)).toFixed(decimalPoint))} ${sizes[
            index
        ]}`
    }

    chart
        .line()
        .position('group*total')
        .color('#36B3C3')
        .size(3)

    chart
        .line()
        .position('group*count')
        .color('#0F86DD')
        .size(3)

    chart.cols({
        group: {
            alias: ' ',
            type: 'timeCat',
            formatter: value => moment(value).format(chart._attrs.data.data[0].time_format)
        },
        total: {
            alias: 'Bandwidth',
            formatter: value => formatBytes(value)
        },
        count: {
            alias: 'Requests'
        }
    })

    chart.legend({
        position: 'bottom',
        dy: 5
    })

    chart.render()
})

@inject('globalStore')
@observer
class Organisation extends Component {
    @observable org = []
    @observable locations = []
    @observable configs = []
    @observable
    plotCfg: {
        margin: [100, 100, 180, 180]
    }
    @observable timeRange = 'day'
    @observable editOrgModal = false
    @observable editLocationData = false
    @observable editLocationModal = false
    @observable loading = true
    @observable showMoreConfigs = true
    @observable showMoreLocations = true

    @observable
    to = moment()
        .utc()
        .set({ hour: 23, minute: 59, second: 59 })

    @observable
    from = moment()
        .utc()
        .set({ hour: 0, minute: 0, second: 0 })

    @observable interval = 60

    @observable data = []

    componentDidMount() {
        this.getData(true)
    }

    getData = initialLoad => {
        if (initialLoad) this.loading = true
        this.editOrgModal = false
        this.editLocationData = ''
        this.editOrgModal = false
        GetData.orgs('GET', { id: this.props.match.params.id }).then(json => {
            this.handleResponse(json, true)
        })
        GetData.activityOrg('GET', {
            id: this.props.match.params.id,
            from: this.from,
            to: this.to,
            interval: this.interval
        }).then(json => {
            this.data = json
            this.loading = false
        })
    }

    // Removed till we figure out the ramifications of deleting an Organisation...
    // deleteOrg = () => {
    //     GetData.orgs('DELETE', { id: this.props.match.params.id }).then(() => {
    //         this.props.history.push('/management/organisations#deleted')
    //     })
    // }

    getSport = sport => {
        switch (sport) {
            case '1':
                return 'Horse Racing'
                break
            case '2':
                return 'Greyhounds'
                break
            default:
                return ''
        }
    }

    getType = filter => {
        switch (filter) {
            case 'hours':
                this.to = moment()
                    .utc()
                    .set({ hour: 23, minute: 59, second: 59 })

                this.from = moment()
                    .utc()
                    .set({ hour: 0, minute: 0, second: 0 })
                this.interval = 60
                break
            case 'month':
                this.from = moment()
                    .utc()
                    .set({ hour: 23, minute: 59, second: 59 })
                    .subtract(30, 'days')
                this.to = moment()
                    .utc()
                    .set({ hour: 23, minute: 59, second: 59 })
                this.interval = 3600
                break
            case 'year':
                this.from = moment()
                    .utc()
                    .set({
                        year: moment().get('year'),
                        month: 0,
                        date: 1,
                        hour: 0,
                        minute: 0,
                        second: 0
                    })
                this.to = moment()
                    .utc()
                    .set({
                        year: moment().get('year'),
                        month: 11,
                        date: 31,
                        hour: 23,
                        minute: 59,
                        second: 59
                    })
                this.interval = 604800
                break
        }
    }

    changeFilter = filter => {
        this.timeRange = _.split(filter, ':')[1]
        this.getType(_.split(filter, ':')[0])
        this.getData()
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.org = _.head(json).data
                this.showMoreLocations = _.head(json).locations.length > 5
                this.showMoreConfigs = _.head(json).rpd_config.length > 5
                this.locations = _.head(json).locations.slice(0, 5)
                this.configs = _.head(json).rpd_config.slice(0, 5)
            }
            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    setEditLocation = loc => {
        this.editLocationModal = true
        this.editLocationData = loc
    }

    render() {
        const { brand_name } = this.org

        const locationsCol = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'firstName',
                render: (text, record) => (
                    <span>
                        {text} {record.secondName}
                    </span>
                )
            },
            {
                title: 'City',
                dataIndex: 'city',
                key: 'city',
                render: (text, record) => <span>{text}</span>
            },
            {
                title: 'Post code',
                dataIndex: 'post_code',
                key: 'post_code',
                render: (text, record) => <span>{text}</span>
            }
        ]

        const displayCol = [
            {
                title: 'Soft Name',
                dataIndex: 'config.soft_name',
                key: 'config.soft_name',
                render: (text, record) => (
                    <Popover content={record._dashboard_id} title="ID">
                        <span className="tool-tip">{text}</span>
                    </Popover>
                )
            },
            {
                title: 'Sport',
                dataIndex: 'config.sport',
                key: 'config.sport',
                render: (text, record) => <span>{this.getSport(text)}</span>
            },
            {
                title: 'Bookmaker',
                dataIndex: 'config.bookmaker_id',
                key: 'config.bookmaker_id',
                render: (text, record) => <span>{text}</span>
            }
        ]
        return (
            <div className="">
                <ModalSwitcher
                    title="Edit Customer"
                    modal="EditOrganisations"
                    data={this.org}
                    showModal={this.editOrgModal}
                    closeModal={() => {
                        this.editOrgModal = false
                    }}
                    callback={() => this.getData()}
                />
                <ModalSwitcher
                    title="Edit Location"
                    modal="EditLocation"
                    data={this.editLocationData}
                    showModal={this.editLocationModal}
                    closeModal={() => {
                        this.editLocationModal = false
                    }}
                    callback={() => this.getData()}
                />

                <Row type="flex" justify="space-between" align="middle">
                    <h1>{brand_name} </h1>
                    <Button
                        type="primary"
                        ghost
                        onClick={() => (this.editOrgModal = true)}
                        style={{ marginRight: '5px' }}
                    >
                        Edit {brand_name}
                    </Button>
                </Row>

                <div className="main__body-container" style={{ minHeight: '454px' }}>
                    <span>
                        <Row
                            type="flex"
                            justify="space-between"
                            align="middle"
                            className="title-bar"
                        >
                            <Col>
                                {this.data.length > 1 &&
                                !this.loading && <h2>Data usage for the past {this.timeRange}.</h2>}
                                {this.data.length <= 1 &&
                                !this.loading && <h2>No data for this time range!</h2>}
                            </Col>
                            <Col span={3}>
                                Filter by
                                <Select
                                    className="filter__select-box"
                                    defaultValue="hours:day"
                                    onChange={this.changeFilter}
                                >
                                    <Option value="hours:day">Today</Option>
                                    <Option value="month:30 days">Last 30 days</Option>
                                    <Option value="year:year">Months</Option>
                                </Select>
                            </Col>
                        </Row>
                    </span>
                    {this.data.length > 1 &&
                    !this.loading && (
                        <Line
                            data={toJS(this.data)}
                            forceFit
                            width={0}
                            plotCfg={this.plotCfg}
                            height={400}
                            ref="myChart"
                            timeformat={'HH:mm'}
                        />
                    )}
                    {this.loading && (
                        <div className="line-chart__loading-container">
                            <Icon type="loading-2" spin />
                        </div>
                    )}
                </div>

                <Row gutter={16} style={{ marginTop: '20px' }}>
                    <Col span={12}>
                        <h2>Locations:</h2>
                        <div className="table__body-container table__body-small">
                            <Table
                                columns={locationsCol}
                                dataSource={toJS(this.locations)}
                                pagination={false}
                                _dashboard_id
                                rowKey={record => record.id}
                                onRowDoubleClick={row =>
                                    this.props.history.push(`/management/location/${row.id}`)}
                            />
                            <div className="show-more">
                                {this.showMoreLocations && (
                                    <span>
                                        <Link to="/management/screens">More...</Link>
                                    </span>
                                )}
                            </div>
                        </div>
                    </Col>

                    <Col span={12}>
                        <h2>Configurations:</h2>
                        <div className="table__body-container table__body-small">
                            <Table
                                columns={displayCol}
                                dataSource={toJS(this.configs)}
                                rowKey={record => record._dashboard_id}
                                pagination={false}
                                onRowDoubleClick={row =>
                                    this.props.history.push(
                                        `/management/screen/${row._dashboard_id}`
                                    )}
                            />
                            <div className="show-more">
                                {this.showMoreConfigs && (
                                    <span>
                                        <Link to="/management/screens">More...</Link>
                                    </span>
                                )}
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Organisation
