import React, { Component } from 'react'
import { Table, Popconfirm, message, Button, Select, Icon } from 'antd'
import EditableCell from './EditableCell'
import { observable, toJS } from 'mobx'
import { observer } from 'mobx-react'
import GetData from '../../tools/GetData'
import FlagIcon from 'react-flag-kit/lib/CDNFlagIcon.js'

const Option = Select.Option

@observer
class EditableTable extends Component {
    @observable dataSource = []

    getFlag(code) {
        if (code) {
            return code === 'en' ? 'GB' : code.toUpperCase()
        } else {
            return 'WW' // Neutral fallback flag if code doesn't match any
        }
    }

    getLanguage(code) {
        switch (code) {
            case 'en':
                return 'English (UK)'
            case 'hk':
                return 'Cantonese'
            case 'es':
                return 'Spanish'
            case 'gr':
                return 'Greek'
            default:
                console.warn('Unknown country code!')
                break
        }
    }

    handleChangeCountry = id => value => {
        const dataSource = [...this.dataSource]
        const target = dataSource.find(item => item.id === id)
        target.language = value
        this.dataSource = dataSource
        const body = target
        const { data } = this.props
        const tokenId = data.id
        const translationId = id

        GetData.putTranslation(tokenId, translationId, body).then(json => {
            this.handleSubmitResponse(json)
        })
    }

    constructor(props) {
        super(props)
        const langsOptions = this.props.languages.map(({ name }) => (
            <Option key={name} value={name}>
                <FlagIcon code={this.getFlag(name)} size={20} />
                <span>{this.getLanguage(name)}</span>
            </Option>
        ))

        this.columns = [
            {
                title: 'Language',
                dataIndex: 'language',
                render: (text, record) => (
                    <div>
                        <Select
                            defaultValue={record.language}
                            style={{ width: 150 }}
                            onChange={this.handleChangeCountry(record.id)}
                        >
                            {langsOptions}
                        </Select>
                    </div>
                )
            },
            {
                title: 'Text',
                dataIndex: 'text',
                width: '70%',
                render: (text, record) => (
                    <EditableCell value={text} onChange={this.onCellChange(record.id, 'text')} />
                )
            },
            {
                title: 'Action',
                dataIndex: 'action',
                render: (text, record) =>
                    this.dataSource.length > 1 ? (
                        <Popconfirm
                            title="Sure to delete?"
                            onConfirm={() => this.onDelete(record.id)}
                        >
                            <a href="#">
                                <Icon type="x-2" />
                            </a>
                        </Popconfirm>
                    ) : null
            }
        ]

        const translations = this.props.data.translations
        this.dataSource = translations
    }

    onCellChange = (id, dataIndex) => value => {
        const dataSource = [...this.dataSource]
        const target = dataSource.find(item => item.id === id)

        // Only update if the value has changed.
        if (target[dataIndex] !== value) {
            target[dataIndex] = value
            this.dataSource = dataSource
            const body = target
            const { data } = this.props
            const tokenId = data.id
            const translationId = id

            GetData.putTranslation(tokenId, translationId, body).then(json => {
                this.handleSubmitResponse(json)
            })
        }
    }

    handleSubmitResponse = json => {
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            message.success('Success - change has been made')
        }
    }

    onDelete = id => {
        const dataSource = [...this.dataSource]
        this.dataSource = dataSource.filter(item => item.id !== id)

        const { data } = this.props
        const tokenId = data.id
        const translationId = id

        GetData.deleteTranslation(tokenId, translationId).then(json => {
            this.handleSubmitResponse(json)
        })
    }

    handleAdd = () => {
        const newData = {
            language: 'en',
            text: 'New Translation'
        }

        const tokenId = this.props.data.id

        GetData.postTranslation(tokenId, newData).then(({ data }) => {
            this.dataSource.push(data)
            this.handleSubmitResponse(data)
        })
    }

    render() {
        const { dataSource } = this
        const columns = this.columns
        return (
            <div>
                <Table
                    pagination={false}
                    bordered
                    dataSource={toJS(dataSource)}
                    columns={columns}
                />
                <Button className="editable-add-btn" onClick={this.handleAdd}>
                    Add Translation
                </Button>
            </div>
        )
    }
}

export default EditableTable
