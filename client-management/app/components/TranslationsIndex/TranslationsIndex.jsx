import React, { Component } from 'react'
import { Tabs, Table, message, Button, Popconfirm, Row, Icon } from 'antd'
import { observable, toJS } from 'mobx'
import { observer } from 'mobx-react'
import GetData from '../../tools/GetData'
import EditableTable from './EditableTable'
import EditableCell from './EditableCell'

const TabPane = Tabs.TabPane

@observer
class TranslationsIndex extends Component {
    @observable tokenData = []
    @observable languages = []

    componentDidMount() {
        this.getTokens()
        GetData.getLanguages().then(languages => {
            this.languages = languages
            this.handleResponse(languages)
        })
    }

    getTokens = () => {
        GetData.getTokens().then(json => {
            this.handleResponse(json, true)
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error('An error occured', 5)
        } else {
            if (load) {
                GetData.getTokens().then(json => {
                    this.tokenData = json.data
                    this.handleResponse(json)
                })
            }

            if (!load) {
                message.success('Success - change has been made')
            }
        }
    }

    expandedRowRender = row => {
        return (
            <ExpandedTranslation
                languages={toJS(this.languages.data)}
                handleResponse={() => this.handleResponse}
                token={row}
            />
        )
    }

    handleAdd = () => {
        const { tokenData } = this
        const newObj = {
            name: 'New Token'
        }
        this.tokenData = [...tokenData, newObj]

        GetData.postToken(newObj).then(json => {
            this.handleResponse(json, true)
        })
    }

    onDelete = id => {
        const tokenData = [...this.tokenData]
        this.tokenData = tokenData.filter(item => item.id !== id)
        GetData.deleteToken(id).then(json => {
            this.handleResponse(json)
        })
    }

    onCellChange = (id, dataIndex) => value => {
        const dataSource = [...this.tokenData]
        const target = dataSource.find(item => item.id === id)

        // Only update if the value has changed.
        if (target[dataIndex] !== value) {
            target[dataIndex] = value
            this.dataSource = dataSource
            const tokenId = id
            const body = target

            GetData.putToken(tokenId, body).then(json => {
                this.handleResponse(json, true)
            })
        }
    }

    renderTokensSection() {
        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                render: (text, record) => (
                    <EditableCell value={text} onChange={this.onCellChange(record.id, 'name')} />
                )
            },
            {
                title: 'Action',
                dataIndex: 'action',
                render: (text, record) =>
                    this.tokenData.length > 1 ? (
                        <Popconfirm
                            title="Sure to delete?"
                            onConfirm={() => this.onDelete(record.id)}
                        >
                            <a href="#">
                                <Icon type="x-2" />
                            </a>
                        </Popconfirm>
                    ) : null
            }
        ]

        const data = toJS(this.tokenData)

        return (
            <div className="tokens__container">
                <Table
                    columns={columns}
                    dataSource={data}
                    onChange={this.onChange}
                    loading={this.loading}
                    rowKey={record => record.id}
                    expandedRowRender={this.expandedRowRender}
                />
            </div>
        )
    }

    render() {
        return (
            <div className="container">
                <Row type="flex" justify="space-between">
                    <h1>Translations - Tokens</h1>
                    <Button className="editable-add-btn" onClick={this.handleAdd} type="primary">
                        Add Token
                    </Button>
                </Row>
                <div className="table__body-container">{this.renderTokensSection()}</div>
            </div>
        )
    }
}

@observer
class ExpandedTranslation extends Component {
    @observable translationData = null

    constructor(props) {
        super(props)
        this.getTranslations(props.token.id)
    }

    getTranslations = id => {
        return GetData.getTranslations(id).then(json => {
            this.translationData = json.data
            this.props.handleResponse(json, false)
        })
    }

    render() {
        return !this.translationData ? (
            <h1>{/* TODO: insert loading spinner */}</h1>
        ) : (
            <EditableTable languages={this.props.languages} data={this.translationData} />
        )
    }
}

export default TranslationsIndex
