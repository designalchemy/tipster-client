import React, { Component } from 'react'
import { Input, Icon } from 'antd'
import { observable } from 'mobx'
import { observer } from 'mobx-react'

@observer
class EditableCell extends Component {
    @observable value = this.props.value
    @observable editable = false

    handleChange = e => {
        const value = e.target.value
        this.value = value
    }
    check = () => {
        if (!this.value) {
            this.value = 'New Translation'
        }

        this.editable = false
        if (this.props.onChange) {
            this.props.onChange(this.value)
        }
    }
    edit = () => {
        this.editable = true
    }
    render() {
        const { value, editable } = this
        return (
            <div className="editable-cell">
                {editable ? (
                    <div className="editable-cell-input-wrapper">
                        <Icon
                            type="pencil-1"
                            className="editable-cell-icon-check"
                            onClick={this.check}
                        />
                        <Input
                            value={value}
                            onChange={this.handleChange}
                            onPressEnter={this.check}
                        />
                    </div>
                ) : (
                    <div className="editable-cell-text-wrapper">
                        <Icon type="pencil-1" className="editable-cell-icon" onClick={this.edit} />
                        {value || ' '}
                    </div>
                )}
            </div>
        )
    }
}

export default EditableCell
