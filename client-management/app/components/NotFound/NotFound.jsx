import React, { Component } from 'react'

import css from './NotFound.less'

class NotFound extends Component {
    render() {
        return (
            <div className="not-found-container">
                <h1>Page Not Found</h1>
            </div>
        )
    }
}

export default NotFound
