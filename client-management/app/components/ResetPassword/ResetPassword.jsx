import React, { Component } from 'react'
import { formItemLayout, tailFormItemLayout, strongPass } from '../../tools/utils'
import { Button, Form, Input, message, Icon } from 'antd'
import GetData from '../../tools/GetData'
import queryString from 'query-string'

const FormItem = Form.Item

class ResetPassword extends Component {
    handleSubmit = e => {
        e.preventDefault()
        const parsed = queryString.parse(this.props.location.search)
        if (!parsed.id) {
            return message.error('No ID provided, please check email')
        }

        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                values.resetKey = parsed.id

                GetData.email('PUT', values).then(json => {
                    this.handleSubmitResponse(json)
                })
            }
        })
    }

    handleSubmitResponse = json => {
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            message.success('Success - please login')
            this.props.history.push('/management/login')
            this.props.history.replace('/management/login')
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <div className="login-form__container">
                <Form onSubmit={this.handleSubmit} className="login-form__body">
                    <img
                        src="/management/assets/racing-post_dark.png"
                        alt="logo"
                        className="login__logo"
                    />
                    <h3 style={{ marginBottom: '15px' }}>Please enter a new password</h3>

                    <FormItem hasFeedback>
                        {getFieldDecorator('password', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input a password!'
                                },
                                {
                                    validator: strongPass
                                }
                            ]
                        })(
                            <Input
                                type="password"
                                prefix={<Icon type="lock-1" style={{ fontSize: 13 }} />}
                                placeholder="Password"
                            />
                        )}
                    </FormItem>

                    <FormItem hasFeedback>
                        {getFieldDecorator('confirm', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please confirm the password!'
                                },
                                {
                                    validator: this.checkPassword
                                }
                            ]
                        })(
                            <Input
                                type="password"
                                prefix={<Icon type="lock-1" style={{ fontSize: 13 }} />}
                                placeholder="Confirm Password"
                                onBlur={this.handleConfirmBlur}
                            />
                        )}
                    </FormItem>

                    <FormItem>
                        <Button
                            htmlType="submit"
                            type="primary"
                            className="login-form__button"
                            size="large"
                        >
                            Submit
                        </Button>
                    </FormItem>
                </Form>
            </div>
        )
    }
}

export default Form.create()(ResetPassword)
