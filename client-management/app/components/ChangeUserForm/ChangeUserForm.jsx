import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Button, Form, Input, message } from 'antd'
import GetData from '../../tools/GetData'
import { formItemLayout, tailFormItemLayout } from '../../tools/utils'

const FormItem = Form.Item

@inject('globalStore', 'routing')
@observer
class ChangeUserForm extends Component {
    componentDidMount() {
        this.props.onRef(this)
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    handleSubmit = e => {
        e && e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                GetData.account('PUT', values).then(json => {
                    this.handleSubmitResponse(json, values)
                })
            }
        })
    }

    handleSubmitResponse = (json, values) => {
        const userData = this.props.globalStore.user
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            message.success('Success - change has been made')
            this.props.globalStore.storeUser(_.merge(userData, values))
            this.props.form.resetFields()
            this.props.closeModal()
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form

        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem {...formItemLayout} label="First Name" hasFeedback>
                    {getFieldDecorator('firstName', {
                        rules: [
                            {
                                required: true,
                                message: 'Please input a first name!'
                            }
                        ]
                    })(<Input />)}
                </FormItem>

                <FormItem {...formItemLayout} label="Second Name" hasFeedback>
                    {getFieldDecorator('secondName', {
                        rules: [
                            {
                                required: true,
                                message: 'Please input a second name!'
                            }
                        ]
                    })(<Input />)}
                </FormItem>

                <FormItem {...formItemLayout} label="Email" hasFeedback>
                    {getFieldDecorator('email', {
                        rules: [
                            {
                                required: true,
                                message: 'Please input a email address!'
                            }
                        ]
                    })(<Input />)}
                </FormItem>

                <FormItem {...formItemLayout} label="Skype" hasFeedback>
                    {getFieldDecorator('skype', {
                        rules: [
                            {
                                message: 'Please input a skype name!'
                            }
                        ]
                    })(<Input />)}
                </FormItem>
            </Form>
        )
    }
}

export default Form.create()(ChangeUserForm)
