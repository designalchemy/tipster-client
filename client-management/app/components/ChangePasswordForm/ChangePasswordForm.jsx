import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Button, Form, Input, message } from 'antd'
import GetData from '../../tools/GetData'
import { containsNumber, strongPass, formItemLayout, tailFormItemLayout } from '../../tools/utils'

const FormItem = Form.Item

@inject('globalStore', 'routing')
@observer
class ChangePasswordForm extends Component {
    state = {}

    componentDidMount() {
        this.props.onRef(this)
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    handleSubmit = e => {
        e && e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                GetData.account('PUT', values).then(json => {
                    this.handleSubmitResponse(json)
                })
            }
        })
    }

    handleSubmitResponse = json => {
        if (json.error) {
            this.props.form.resetFields()
            message.error(`${json.message}`)
        } else {
            message.success('Success - change has been made')
            this.props.form.resetFields()
            this.props.closeModal()
        }
    }

    checkPassword = (rule, value, callback) => {
        const form = this.props.form
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!')
        } else {
            callback()
        }
    }

    handleConfirmBlur = e => {
        const value = e.target.value
        this.setState({ confirmDirty: this.state.confirmDirty || !!value })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem {...formItemLayout} label="Password" hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [
                            {
                                required: true,
                                message: 'Please input a password!'
                            },
                            {
                                validator: strongPass
                            }
                        ]
                    })(<Input type="password" />)}
                </FormItem>

                <FormItem {...formItemLayout} label="Confirm Password" hasFeedback>
                    {getFieldDecorator('confirm', {
                        rules: [
                            {
                                required: true,
                                message: 'Please confirm the password!'
                            },
                            {
                                validator: this.checkPassword
                            }
                        ]
                    })(<Input type="password" onBlur={this.handleConfirmBlur} />)}
                </FormItem>
            </Form>
        )
    }
}

export default Form.create()(ChangePasswordForm)
