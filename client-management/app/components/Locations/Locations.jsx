import React, { Component } from 'react'
import { Table, Icon, Button, Row, Popconfirm, message, Col, Popover, Select } from 'antd'
import { Link } from 'react-router-dom'
import GetData from 'tools/GetData'
import ModalSwitcher from '../Modals/ModalSwitcher'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import { observable, toJS } from 'mobx'
import { sortAlpha } from '../../tools/utils'
import css from './Locations.less'
import createG2 from 'g2-react'
import { Stat } from 'g2'

const Option = Select.Option

const Line = createG2(chart => {
    function formatBytes(bytes) {
        if (bytes == 0) return '0 Bytes'
        let kilobytes = 1024,
            decimalPoint = 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            index = Math.floor(Math.log(bytes) / Math.log(kilobytes))
        return `${parseFloat((bytes / Math.pow(kilobytes, index)).toFixed(decimalPoint))} ${sizes[
            index
        ]}`
    }

    chart
        .line()
        .position('group*total')
        .color('#36B3C3')
        .size(3)

    chart
        .line()
        .position('group*count')
        .color('#0F86DD')
        .size(3)

    chart.cols({
        group: {
            alias: ' ',
            type: 'timeCat',
            formatter: value => moment(value).format(chart._attrs.data.data[0].time_format)
        },
        total: {
            alias: 'Bandwidth',
            formatter: value => formatBytes(value)
        },
        count: {
            alias: 'Requests'
        }
    })

    chart.legend({
        position: 'bottom',
        dy: 5
    })

    chart.render()
})

@inject('globalStore')
@observer
class Organisation extends Component {
    @observable location = []
    @observable
    plotCfg: {
        margin: [100, 100, 180, 180]
    }
    @observable timeRange = 'day'
    @observable editLocationData = false
    @observable editLocationModal = false
    @observable editAuthModal = false
    @observable showMoreDevices = false
    @observable loading = true
    @observable editAuthData = {}
    @observable devices = []

    @observable
    to = moment()
        .utc()
        .set({ hour: 23, minute: 59, second: 59 })

    @observable
    from = moment()
        .utc()
        .set({ hour: 0, minute: 0, second: 0 })

    @observable interval = 60

    @observable data = []

    componentDidMount() {
        this.getData()
    }

    getData = initialLoad => {
        if (initialLoad) this.loading = true
        this.editLocationData = ''
        this.editAuthData = ''
        this.editLocationModal = false
        this.editAuthModal = false
        GetData.locations('GET', {
            id: this.props.match.params.id
        }).then(json => {
            this.handleResponse(json, true)
        })
        GetData.activityLocation('GET', {
            id: this.props.match.params.id,
            from: this.from,
            to: this.to,
            interval: this.interval
        }).then(json => {
            this.loading = false
            this.data = json
        })
    }

    deleteOrg = () => {
        GetData.orgs('DELETE', { id: this.props.match.params.id }).then(() => {
            this.props.history.push('/management/organisations#deleted')
        })
    }

    getSport = sport => {
        switch (sport) {
            case '1':
                return 'Horse Racing'
                break
            case '2':
                return 'Greyhounds'
                break
            default:
                return ''
        }
    }

    getType = filter => {
        switch (filter) {
            case 'hours':
                this.to = moment()
                    .utc()
                    .set({ hour: 23, minute: 59, second: 59 })

                this.from = moment()
                    .utc()
                    .set({ hour: 0, minute: 0, second: 0 })
                this.interval = 60
                break
            case 'month':
                this.from = moment()
                    .utc()
                    .set({ hour: 23, minute: 59, second: 59 })
                    .subtract(30, 'days')
                this.to = moment()
                    .utc()
                    .set({ hour: 23, minute: 59, second: 59 })
                this.interval = 3600
                break
            case 'year':
                this.from = moment()
                    .utc()
                    .set({
                        year: moment().get('year'),
                        month: 0,
                        date: 1,
                        hour: 0,
                        minute: 0,
                        second: 0
                    })
                this.to = moment()
                    .utc()
                    .set({
                        year: moment().get('year'),
                        month: 11,
                        date: 31,
                        hour: 23,
                        minute: 59,
                        second: 59
                    })
                this.interval = 604800
                break
        }
    }

    changeFilter = filter => {
        this.timeRange = _.split(filter, ':')[1]
        this.getType(_.split(filter, ':')[0])
        this.getData()
    }

    launchModal = data => {
        this.editAuthModal = true
        this.editAuthData = data
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                const res = _.head(json)
                this.location = res
                this.showMoreDevices = res.devices.length > 5
                this.devices = res.devices.slice(0, 5)
            }
            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    setEditLocation = loc => {
        this.editLocationModal = true
        this.editLocationData = loc
    }

    render() {
        const deviceCol = [
            {
                title: 'Soft Name',
                dataIndex: 'softName',
                key: 'softName',
                render: (text, record) => <span>{_.size(text) === 0 ? '-' : text}</span>
            },
            {
                title: 'Last Login',
                dataIndex: 'lastLogin',
                key: 'lastLogin',
                render: (text, record) => <span>{moment(text).format('HH:mm - DD/MM/YYYY')}</span>
            },
            {
                title: 'IP',
                dataIndex: 'ip',
                key: 'ip',
                render: (text, record) => (
                    <Popover
                        placement="topRight"
                        content={`${record.browser}/${record.os}`}
                        title="Full ID"
                        trigger="hover"
                    >
                        <span className="tool-tip">{text}</span>
                    </Popover>
                )
            }
        ]

        const { name, brand_name } = this.location

        return (
            <div className="">
                <ModalSwitcher
                    title="Edit Location"
                    modal="EditLocation"
                    data={this.location}
                    showModal={this.editLocationModal}
                    closeModal={() => {
                        this.editLocationModal = false
                    }}
                    callback={() => this.getData()}
                />

                <ModalSwitcher
                    title="Edit Device"
                    modal="EditAuth"
                    data={this.editAuthData}
                    showModal={this.editAuthModal}
                    closeModal={() => {
                        this.editAuthModal = false
                    }}
                    callback={() => this.getData()}
                />

                <Row type="flex" justify="space-between" align="middle">
                    <h1>
                        {name} - {brand_name}
                    </h1>
                    <Button
                        ghost
                        type="primary"
                        onClick={() => (this.editLocationModal = true)}
                        style={{ marginRight: '5px' }}
                    >
                        Edit {name}
                    </Button>
                </Row>

                <div className="main__body-container" style={{ minHeight: '454px' }}>
                    <span>
                        <Row
                            type="flex"
                            justify="space-between"
                            align="middle"
                            className="title-bar"
                        >
                            <Col>
                                {this.data.length > 1 &&
                                !this.loading && <h2>Data usage for the past {this.timeRange}.</h2>}
                                {this.data.length <= 1 &&
                                !this.loading && <h2>No data for this time range!</h2>}
                            </Col>
                            <Col span={3}>
                                Filter by
                                <Select
                                    className="filter__select-box"
                                    defaultValue="hours:day"
                                    onChange={this.changeFilter}
                                >
                                    <Option value="hours:day">Today</Option>
                                    <Option value="month:30 days">Last 30 days</Option>
                                    <Option value="year:year">Months</Option>
                                </Select>
                            </Col>
                        </Row>
                    </span>
                    {this.data.length > 1 &&
                    !this.loading && (
                        <Line
                            data={toJS(this.data)}
                            forceFit
                            width={0}
                            plotCfg={this.plotCfg}
                            height={400}
                            ref="myChart"
                            timeformat={'HH:mm'}
                        />
                    )}
                    {this.loading && (
                        <div className="line-chart__loading-container">
                            <Icon type="loading-2" spin />
                        </div>
                    )}
                </div>

                <Row gutter={16} style={{ marginTop: '20px' }}>
                    <Col span={12}>
                        <h2>Info:</h2>
                        <div className="table__body-container table__body-small">
                            <Row type="flex" justify="space-between" align="middle">
                                <p>Location</p>
                                <p>{this.location.name}</p>
                            </Row>
                            <Row type="flex" justify="space-between" align="middle">
                                <p>Address line 1</p>
                                <p>{this.location.address_1}</p>
                            </Row>
                            <Row type="flex" justify="space-between" align="middle">
                                <p>Address line 2</p>
                                <p>{this.location.address_2}</p>
                            </Row>
                            <Row type="flex" justify="space-between" align="middle">
                                <p>City</p>
                                <p>{this.location.city}</p>
                            </Row>
                            <Row type="flex" justify="space-between" align="middle">
                                <p>Post code</p>
                                <p>{this.location.post_code}</p>
                            </Row>
                            <Row type="flex" justify="space-between" align="middle">
                                <p>Shop ID</p>
                                <p>{this.location.shop_id}</p>
                            </Row>
                        </div>
                    </Col>

                    <Col span={12}>
                        <h2>Devices:</h2>
                        <div className="table__body-container table__body-small">
                            <Table
                                columns={deviceCol}
                                dataSource={toJS(this.devices)}
                                rowKey={record => record.id}
                                pagination={false}
                                onRowDoubleClick={row => this.launchModal(row)}
                            />
                            {this.showMoreDevices && (
                                <div className="show-more">
                                    <Link to="/management/auth">More...</Link>
                                </div>
                            )}
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Organisation
