import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Button, Row, Popconfirm, Form, Input, Select, message } from 'antd'
import GetData from '../../tools/GetData'
import { formItemLayout, tailFormItemLayout, jsonValidator } from '../../tools/utils'

import css from './Configuration.less'

const FormItem = Form.Item
const Option = Select.Option

@inject('globalStore', 'routing')
@observer
class Configuration extends Component {
    state = {
        getData: {
            config: {}
        },
        soft_name: '',
        brand_name: '',
        brands: []
    }

    componentDidMount() {
        this.getData()
        if (this.props.onRef) {
            this.props.onRef(this)
        }
    }

    componentWillUnmount() {
        if (this.props.onRef) {
            this.props.onRef(undefined)
        }
    }

    getData = () => {
        let id = null
        if (this.props.match) {
            id = this.props.match.params.id
        }

        if (id) {
            GetData.config('GET', { id }).then(json => {
                this.handleResponse(json, true)
            })
        } else {
            GetData.orgs('GET').then(json => {
                this.setState({
                    brands: json.data
                })
            })
        }
    }

    deleteDashboard = id => {
        GetData.config('DELETE', { id }).then(json => {
            this.handleSubmitResponse(json)
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.setState({
                    ...json.config,
                    brands: {
                        ...json.brands
                    },
                    brand_name: json.config.brand_name
                })
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    handleSubmit = e => {
        e && e.preventDefault()
        let id = null
        if (this.props.match) {
            id = this.props.match.params.id
        }
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                let type = 'POST'
                if (id) {
                    values.id = id
                    type = 'PUT'
                }
                GetData.config(type, values).then(json => {
                    this.handleSubmitResponse(json)
                })
            }
        })
    }

    handleSubmitResponse = json => {
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            message.success('Success - change has been made')
            if (!this.props.match) {
                this.props.closeModal()
                this.props.form.resetFields()
                this.props.callback()
            } else {
                this.props.history.push('/management/configurations')
                this.props.history.replace('/management/configurations')
            }
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { soft_name, brand_name, config, config_brand_id } = this.state
        let id = null
        const json = _.isNil(config) ? '{}' : JSON.parse(config)
        if (this.props.match) {
            id = this.props.match.params.id
        }

        const className =
            this.props.match && this.props.match.params.id ? 'main__body-container' : ''
        return (
            <div>
                <Row type="flex" justify="space-between">
                    {id && <h1>Configuration</h1>}

                    {id && (
                        <Popconfirm
                            title="Are you sure delete this Configuration, this cannot be undone?"
                            onConfirm={() => this.deleteDashboard(id)}
                            placement="left"
                            okText="Yes"
                            cancelText="No"
                        >
                            <Button type="danger">Delete Configuration</Button>
                        </Popconfirm>
                    )}
                </Row>

                <div className={className}>
                    <Form onSubmit={this.handleSubmit}>
                        {id && (
                            <FormItem {...formItemLayout} label="ID" hasFeedback>
                                {getFieldDecorator('id', {
                                    initialValue: id,
                                    rules: [
                                        {
                                            required: false
                                        }
                                    ]
                                })(<Input disabled />)}
                            </FormItem>
                        )}

                        <FormItem {...formItemLayout} label="Soft Name" hasFeedback>
                            {getFieldDecorator('soft_name', {
                                initialValue: soft_name,
                                rules: [
                                    {
                                        required: true
                                    }
                                ]
                            })(<Input />)}
                        </FormItem>

                        <FormItem {...formItemLayout} label="Owner" hasFeedback>
                            {getFieldDecorator('config_brand_id', {
                                initialValue: config_brand_id,
                                rules: [
                                    {
                                        required: true
                                    }
                                ]
                            })(
                                <Select>
                                    {_.map(this.state.brands, brand => (
                                        <Option key={`brand${brand.id}`} value={brand.id}>
                                            {brand.brand_name}
                                        </Option>
                                    ))}
                                </Select>
                            )}
                        </FormItem>

                        <FormItem {...formItemLayout} label="JSON" hasFeedback>
                            {getFieldDecorator('config', {
                                initialValue: JSON.stringify(JSON.parse(json), undefined, 4),
                                rules: [
                                    {
                                        required: true
                                    },
                                    {
                                        validator: jsonValidator
                                    }
                                ]
                            })(<Input type="textarea" rows={14} />)}
                        </FormItem>
                        {id && (
                            <FormItem {...tailFormItemLayout}>
                                <Button htmlType="submit" type="primary" size="large">
                                    Submit
                                </Button>
                            </FormItem>
                        )}
                    </Form>
                </div>
            </div>
        )
    }
}

export default Form.create()(Configuration)
