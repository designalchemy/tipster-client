import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import ModalSwitcher from '../Modals/ModalSwitcher'
import { Form, Icon, Input, Button, Checkbox, message } from 'antd'
import { Link } from 'react-router-dom'

import style from './Login.less'

const FormItem = Form.Item

@inject('globalStore')
@observer
class Login extends React.Component {
    @observable loading = false
    @observable forgotPasswordModal = false

    componentDidMount() {
        if (window.location.hash === '#error') {
            localStorage.removeItem('backoffice-jwt')
            setTimeout(() => {
                message.error('Your session has expired, please login again', 5)
            }, 100)
        }
    }

    handleSubmit = e => {
        e.preventDefault()
        this.loading = true
        this.props.form.validateFields((err, values) => {
            if (!err) {
                fetch('/api/login', {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json; charset=utf-8'
                    },
                    body: JSON.stringify(values)
                })
                    .then(response => response.json())
                    .then(json => {
                        this.loading = false

                        if (json.error) {
                            // deal with error, tell user they are wrong etc
                            message.error('Login Failed - Please try again')
                        } else {
                            // let them in, grant a JWT token
                            message.success('Login successful')

                            this.props.globalStore.storeToken(json.token)
                            localStorage.setItem('backoffice-jwt', json.token)

                            this.props.history.push('/management/screens')
                            this.props.history.replace('/management/screens')
                        }
                    })
                    .catch(error => {
                        console.log(error)
                    })
            } else {
                this.loading = false
            }
        })
    }
    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <div className="login-form__container">
                <ModalSwitcher
                    title="Forgot Password"
                    modal="ForgotPassword"
                    showModal={this.forgotPasswordModal}
                    closeModal={() => (this.forgotPasswordModal = false)}
                />

                <Form onSubmit={this.handleSubmit} className="login-form__body">
                    <img
                        src="/management/assets/racing-post_dark.png"
                        alt="logo"
                        className="login__logo"
                    />

                    <FormItem>
                        {getFieldDecorator('email', {
                            rules: [
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!'
                                },
                                {
                                    required: true,
                                    message: 'Please input your E-mail!'
                                }
                            ]
                        })(
                            <Input
                                prefix={<Icon type="user-1" style={{ fontSize: 13 }} />}
                                placeholder="Email"
                            />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('password', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your Password!'
                                }
                            ]
                        })(
                            <Input
                                prefix={<Icon type="lock-1" style={{ fontSize: 13 }} />}
                                type="password"
                                placeholder="Password"
                            />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true
                        })(<Checkbox>Remember me</Checkbox>)}
                        <a
                            className="login-form__forgot"
                            onClick={() => (this.forgotPasswordModal = true)}
                            to=""
                        >
                            Forgot password
                        </a>
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form__button"
                            loading={this.loading}
                        >
                            Log in
                        </Button>
                    </FormItem>
                </Form>
            </div>
        )
    }
}

export default Form.create()(Login)
