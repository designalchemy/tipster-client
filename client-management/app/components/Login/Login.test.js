import React from 'react'
import sinon from 'sinon'
import { mount, shallow } from 'enzyme'
import assert from 'assert'

import Login from './Login'

sinon.spy(Login.prototype, 'componentDidMount')

describe('<Login />', function() {
    beforeEach(function() {
        this.globalStore = {}
    })

    it('calls componentDidMount', () => {
        const wrapper = mount(<Login globalStore={this.globalStore} />)
        assert(Login.prototype.componentDidMount.calledOnce === true)
    })
})
