import React, { Component } from 'react'
import { Table, Icon, Button, Row, Col, Popconfirm, Input, message } from 'antd'
import ModalSwitcher from '../Modals/ModalSwitcher'
import { inject, observer } from 'mobx-react'
import { observable, toJS } from 'mobx'
import GetData from '../../tools/GetData'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { sortAlpha } from '../../tools/utils'

@inject('globalStore')
@observer
class IPWhitelist extends Component {
    @observable
    pagination = {
        current: 1,
        pageSize: 50,
        total: 0
    }

    @observable
    search = {
        query: '',
        type: ''
    }

    @observable
    editWhiteList = {
        modal: false,
        data: {}
    }

    @observable
    sorting = {
        field: '',
        order: ''
    }

    @observable
    softNameFilter = {
        visible: false,
        search: ''
    }

    @observable
    IPRange = {
        visible: false,
        search: ''
    }

    @observable locations = []

    @observable orgs = []

    @observable addWhiteListModal = false

    @observable loading = true

    @observable filters = {}

    @observable tableData = []

    componentDidMount() {
        if (location.hash) {
            this.search.query = location.hash.replace('#', '')
            this.search.type = 'ip_range'
        }
        this.getData()
    }

    getData = sort => {
        this.editWhiteList.modal = false
        this.editWhiteList.data = {}
        this.addWhiteListModal = false
        GetData.IP('GET', {
            pagination: JSON.stringify(this.pagination),
            filters: JSON.stringify(this.filters),
            search: JSON.stringify(this.search),
            sorting: JSON.stringify(this.sorting)
        }).then(json => {
            this.handleResponse(json, true)
        })
    }

    deleteIP = record => {
        GetData.IP('DELETE', {
            id: record.id,
            ip_range: record.ip_range
        }).then(json => {
            this.getData()
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.loading = false
                this.pagination.total = json.total
                this.tableData = json.data
                this.orgs = json.orgs
                this.locations = json.locations
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    launchModal = data => {
        this.editWhiteList.data = data
        this.editWhiteList.modal = true
    }

    onChange = (pagination, filters, sorter) => {
        this.loading = true
        this.sorting.order = sorter.order
        this.sorting.field = sorter.field
        this.pagination = pagination
        this.filters = filters
        this.getData()
    }

    onSearch = (type, key) => {
        this.loading = true
        this[key].visible = false
        this.appliedFilters = _.map(this.appliedFilters, item => (item = false))
        this.appliedFilters[type] = true
        this.search.query = this[key].search
        this[key].search = ''
        this.search.type = type
        this.getData()
    }

    clearSearch = key => {
        this.loading = true
        this[key].visible = false
        this.appliedFilters = _.map(this.appliedFilters, item => (item = false))
        this[key].search = ''
        this.search.query = ''
        this.getData()
    }

    onInputChange = (e, type) => {
        this[type].search = e.target.value
    }

    renderLink = (length, link, text) => {
        if (length) {
            return (
                <span>
                    <Link className="table-link" to={`/management/${link}`}>
                        {text}
                    </Link>
                </span>
            )
        }
        return <span>{text}</span>
    }

    render() {
        const columns = [
            {
                title: 'Soft Name',
                dataIndex: 'softName',
                sorter: true,
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.searchInput = ele)}
                            placeholder="Search soft name"
                            value={this.softNameFilter.search}
                            onChange={event => this.onInputChange(event, 'softNameFilter')}
                            onPressEnter={() => this.onSearch('softName', 'softNameFilter')}
                        />
                        <Button
                            type="primary"
                            onClick={() => this.onSearch('softName', 'softNameFilter')}
                        >
                            Search
                        </Button>
                    </div>
                ),
                filterIcon: (
                    <Icon
                        style={{
                            color: this.search.type === 'soft_name' ? '#0084DC' : '#AAAAAA'
                        }}
                        type="zoom-1"
                    />
                ),
                filterDropdownVisible: this.softNameFilter.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.softNameFilter.visible = visible
                }
            },
            {
                title: 'CIDR',
                dataIndex: 'ip_range',
                sorter: true,
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.searchIPInput = ele)}
                            placeholder="Search IP range"
                            value={this.IPRange.search}
                            onChange={event => this.onInputChange(event, 'IPRange')}
                            onPressEnter={() => this.onSearch('ip_range', 'IPRange')}
                        />
                        <Button type="primary" onClick={() => this.onSearch('ip_range', 'IPRange')}>
                            Search
                        </Button>
                    </div>
                ),
                filterIcon: (
                    <Icon
                        style={{
                            color: this.search.type === 'ip_range' ? '#0084DC' : '#AAAAAA'
                        }}
                        type="zoom-1"
                    />
                ),
                filterDropdownVisible: this.IPRange.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.IPRange.visible = visible
                }
            },
            {
                title: 'Customer',
                dataIndex: 'brand_softName',
                sorter: true,
                filters:
                    _.size(this.orgs) > 0
                        ? _.map(this.orgs, item => ({
                              text: item,
                              value: item
                          }))
                        : false
            },
            {
                title: 'Location',
                dataIndex: 'location_softName',
                sorter: true,
                filters:
                    _.size(this.locations) > 0
                        ? _.map(this.locations, item => ({
                              text: item,
                              value: item
                          }))
                        : false
            },
            {
                title: 'IP Version',
                dataIndex: 'ip_type',
                sorter: true
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a
                            href="#"
                            onClick={e => {
                                e.preventDefault()
                                this.launchModal(record)
                            }}
                        >
                            Edit <Icon type="pencil-1" />
                        </a>

                        <span className="ant-divider" />

                        <Popconfirm
                            title="Are you sure delete this whitelist?"
                            onConfirm={() => this.deleteIP(record)}
                            placement="left"
                            okText="Yes"
                            cancelText="No"
                        >
                            <a href="#">
                                Delete <Icon type="trash-1" />
                            </a>
                        </Popconfirm>
                    </span>
                )
            }
        ]

        return (
            <div>
                <ModalSwitcher
                    title="Add New Whitelist"
                    modal="EditWhitelist"
                    showModal={this.addWhiteListModal}
                    closeModal={() => this.getData()}
                />

                <ModalSwitcher
                    title="Edit Whitelist"
                    modal="EditWhitelist"
                    data={this.editWhiteList}
                    showModal={this.editWhiteList.modal}
                    closeModal={() => this.getData()}
                />

                <Row type="flex" justify="space-between">
                    <h1>IP Authentication</h1>

                    <Button type="primary" onClick={() => (this.addWhiteListModal = true)}>
                        Add New IP
                    </Button>
                </Row>

                <div className="table__body-container">
                    <Table
                        columns={columns}
                        dataSource={toJS(this.tableData)}
                        onChange={this.onChange}
                        loading={this.loading}
                        pagination={this.pagination}
                        rowKey={record => record.id}
                        onRowDoubleClick={row => this.launchModal(row)}
                    />
                </div>
            </div>
        )
    }
}

export default IPWhitelist
