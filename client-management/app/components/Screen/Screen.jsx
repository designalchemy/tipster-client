import React, { Component } from 'react'
import {
    Row,
    Col,
    Form,
    Input,
    InputNumber,
    Select,
    Button,
    Switch,
    Slider,
    Popconfirm,
    message,
    Tabs,
    Popover,
    Tooltip
} from 'antd'
import { observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import GetData from '../../tools/GetData'
import css from './Screen.less'
import { jsonValidator, proccessFilePath } from '../../tools/utils'

const FormItem = Form.Item
const Option = Select.Option
const TabPane = Tabs.TabPane

@inject('globalStore')
@observer
class Screen extends Component {
    state = {
        getData: { config: {}, dashboard: {} },
        sport: '',
        bookmaker_id: '#BESTODDS',
        dashboard_mascot: 'horse_racing',
        global_timing: '9000',
        text_timing: '3000',
        bet_timing: '6000',
        predictor_timing: '9000',
        price_format: 'fraction',
        price_source: 'guide_only',
        show_race_title: true,
        show_prices: true,
        max_promps: '3',
        logo: '',
        main_color: '#000000',
        bottom_bar_text: '',
        bet_prompt_offer_logo: '',
        bet_prompt_offer_text: '',
        frozen_race_card_cells: '5',
        odds_color: '#e12a20'
    }

    componentDidMount() {
        if (this.props.onRef) {
            this.props.onRef(this)
        }

        this.getData()
    }

    componentWillUnmount() {
        if (this.props.onRef) {
            this.props.onRef(undefined)
        }
    }

    getData = () => {
        const id =
            this.props.match && this.props.match.params.id ? this.props.match.params.id : null

        GetData.images('GET', {}, '/logo').then(json => {
            this.imagesList = json
        })

        GetData.images('GET', {}, '/characters').then(json => {
            this.charList = json
        })

        if (id) {
            GetData.dashboard('GET', { id }).then(json => {
                this.handleResponse(json, true)
            })
        } else {
            GetData.orgs('GET').then(json => {
                this.setState({
                    getData: json.data
                })
            })
        }
    }

    @observable imagesList = []
    @observable charList = []

    deleteDashboard = id => {
        GetData.dashboard('DELETE', { id }).then(json => {
            this.handleSubmitResponse(json)
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.setState({
                    getData: json,
                    loading: false,
                    ...json.dashboard.config,
                    brand_name: json.dashboard.brand_name
                })
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    handleSubmit = e => {
        e && e.preventDefault()

        const id =
            this.props.match && this.props.match.params.id ? this.props.match.params.id : null

        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                let type = 'POST'
                if (id) {
                    values.id = id
                    type = 'PUT'
                }
                GetData.dashboard(type, values).then(json => {
                    this.handleSubmitResponse(json)
                })
            }
        })
    }

    handleSubmitResponse = json => {
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            message.success('Success - change has been made')
            if (!this.props.match) {
                this.props.closeModal()
                this.props.callback()
            } else {
                this.props.history.push('/management/screens')
                this.props.history.replace('/management/screens')
            }
        }
    }

    toolTipTitle = (title, info) => (
        <Tooltip title={info}>
            <span className="tool-tip">{title}</span>
        </Tooltip>
    )

    render() {
        const { getFieldDecorator } = this.props.form
        const {
            bookmaker_id,
            sport,
            soft_name,
            dashboard_mascot,
            global_timing,
            text_timing,
            bet_timing,
            predictor_timing,
            price_format,
            price_source,
            max_promps,
            logo,
            main_color,
            bottom_bar_text,
            bet_prompt_offer_logo,
            bet_prompt_offer_text,
            frozen_race_card_cells,
            brand_name,
            show_race_title,
            show_prices,
            clock_is_24_hour,
            race_time_is_24_hour,
            next_race_time_is_24_hour,
            welome_prompt_text,
            odds_color
        } = this.state
        const id =
            this.props.match && this.props.match.params.id ? this.props.match.params.id : null
        const brands = this.state.getData.brands ? this.state.getData.brands : this.state.getData
        const acts = this.state.getData.acts ? this.state.getData.acts : this.state.getData
        const json = this.state.getData.dashboard ? this.state.getData.dashboard.config : '{}'
        const dashboardId = this.state.getData.dashboard
            ? this.state.getData.dashboard.dashboard_brand_id
            : ''

        const smallLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 19 }
            }
        }

        const leftLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 }
            }
        }

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 3 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 21 }
            }
        }

        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0
                },
                sm: {
                    span: 14,
                    offset: 3
                }
            }
        }

        const className = id ? 'main__body-container' : ''

        return (
            <div className="">
                <Row type="flex" justify="space-between">
                    <h1>{soft_name}</h1>

                    {id && (
                        <Popconfirm
                            title="Are you sure delete this Dashboard, this cannot be undone?"
                            onConfirm={() => this.deleteDashboard(id)}
                            placement="left"
                            okText="Yes"
                            cancelText="No"
                        >
                            <Button type="danger">Delete Dashboard</Button>
                        </Popconfirm>
                    )}
                </Row>

                <div className={className}>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Display Settings" key="1">
                            <Form onSubmit={this.handleSubmit}>
                                <Row style={{ paddingLeft: '0' }}>
                                    <Col span={12}>
                                        {id && (
                                            <FormItem {...leftLayout} label="ID" hasFeedback>
                                                {getFieldDecorator('id', {
                                                    initialValue: id,
                                                    rules: [
                                                        {
                                                            required: false
                                                        }
                                                    ]
                                                })(<Input disabled />)}
                                            </FormItem>
                                        )}

                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Soft Name',
                                                'This is a soft name so its easy to recognise'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('soft_name', {
                                                initialValue: soft_name,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(<Input />)}
                                        </FormItem>

                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Sport',
                                                'Choose which sport this is going to be'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('sport', {
                                                initialValue: sport,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    <Option key="horse" value="1">
                                                        horse_racing
                                                    </Option>
                                                    <Option key="greyhound" value="2">
                                                        greyhound_racing
                                                    </Option>
                                                </Select>
                                            )}
                                        </FormItem>

                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Owner',
                                                'The organisation that owns this tipster config'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('dashboard_brand_id', {
                                                initialValue: dashboardId,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    {_.map(brands, brand => (
                                                        <Option
                                                            key={`brand${brand.id}`}
                                                            value={brand.id}
                                                        >
                                                            {brand.brand_name}
                                                        </Option>
                                                    ))}
                                                </Select>
                                            )}
                                        </FormItem>

                                        <hr
                                            style={{
                                                marginBottom: '25px',
                                                marginLeft: '9%',
                                                paddingTop: '3px'
                                            }}
                                        />

                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Book Maker',
                                                'The book maker odds to use'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('bookmaker_id', {
                                                initialValue: bookmaker_id,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    <Option value="#BESTODDS">#BESTODDS</Option>
                                                    <Option value="WH_OXI">William Hill</Option>
                                                    <Option value="VC">BetVictor</Option>
                                                    <Option value="SPORTING INDEX">
                                                        Sporting Bet
                                                    </Option>
                                                    <Option value="PPWR">Paddy Power</Option>
                                                    <Option value="RB">Racebets</Option>
                                                    <Option value="CORAL">Coral</Option>
                                                    <Option value="BOLEYSPORTS">Boylesports</Option>
                                                    <Option value="SURREY">SkyBet</Option>
                                                    <Option value="BET365">Bet365</Option>
                                                    <Option value="SUPERSOCCER">SuperSoccer</Option>
                                                    <Option value="EB">Early Bird</Option>
                                                    <Option value="SIS">SIS</Option>
                                                </Select>
                                            )}
                                        </FormItem>

                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Price Format',
                                                'Decimal or Fraction odds'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('price_format', {
                                                initialValue: price_format,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    <Option value="fraction">Fraction</Option>
                                                    <Option value="decimal">Decimal</Option>
                                                </Select>
                                            )}
                                        </FormItem>

                                        <hr
                                            style={{
                                                marginBottom: '25px',
                                                marginLeft: '9%',
                                                paddingTop: '3px'
                                            }}
                                        />

                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Mascot',
                                                'The tipster mascot to use, e.g football, greyhounds etc'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('dashboard_mascot', {
                                                initialValue: dashboard_mascot,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    {_.map(this.charList, item => (
                                                        <Option
                                                            value={proccessFilePath(item)}
                                                            key={proccessFilePath(item)}
                                                        >
                                                            {proccessFilePath(item)}
                                                        </Option>
                                                    ))}
                                                </Select>
                                            )}
                                        </FormItem>

                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Bottom bar Logo',
                                                'The logo to be used in the bottom branding bar'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('logo', {
                                                initialValue: logo,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    {_.map(this.imagesList, item => (
                                                        <Option
                                                            value={proccessFilePath(item)}
                                                            key={proccessFilePath(item)}
                                                        >
                                                            {proccessFilePath(item)}
                                                        </Option>
                                                    ))}
                                                </Select>
                                            )}
                                        </FormItem>

                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Book Maker Logo',
                                                'Logo that will appear in the book maker offer prompt'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('bet_prompt_offer_logo', {
                                                initialValue: bet_prompt_offer_logo,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    {_.map(this.imagesList, item => (
                                                        <Option
                                                            value={proccessFilePath(item)}
                                                            key={proccessFilePath(item)}
                                                        >
                                                            {proccessFilePath(item)}
                                                        </Option>
                                                    ))}
                                                </Select>
                                            )}
                                        </FormItem>
                                    </Col>

                                    <Col span={12} style={{ paddingLeft: '40px' }}>
                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Timing',
                                                'How long between the main bet prompts'
                                            )}
                                            hasFeedback
                                        >
                                            <Row>
                                                <Col span={6}>
                                                    {getFieldDecorator('global_timing', {
                                                        initialValue: Number(global_timing),
                                                        rules: [
                                                            {
                                                                required: true
                                                            }
                                                        ]
                                                    })(
                                                        <InputNumber
                                                            min={5000}
                                                            max={15000}
                                                            step={1000}
                                                            onChange={value =>
                                                                this.setState({
                                                                    global_timing: value
                                                                })}
                                                        />
                                                    )}
                                                </Col>
                                                <Col span={16}>
                                                    <Slider
                                                        min={5000}
                                                        max={15000}
                                                        step={1000}
                                                        value={Number(global_timing)}
                                                        onChange={value =>
                                                            this.setState({
                                                                global_timing: value
                                                            })}
                                                    />
                                                </Col>
                                            </Row>
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Text Timing',
                                                'How long between the info text bubbles'
                                            )}
                                            hasFeedback
                                        >
                                            <Row>
                                                <Col span={6}>
                                                    {getFieldDecorator('text_timing', {
                                                        initialValue: Number(text_timing),
                                                        rules: [
                                                            {
                                                                required: true
                                                            }
                                                        ]
                                                    })(
                                                        <InputNumber
                                                            min={2000}
                                                            max={10000}
                                                            step={1000}
                                                            onChange={value =>
                                                                this.setState({
                                                                    text_timing: value
                                                                })}
                                                        />
                                                    )}
                                                </Col>
                                                <Col span={16}>
                                                    <Slider
                                                        min={2000}
                                                        max={10000}
                                                        step={1000}
                                                        value={Number(text_timing)}
                                                        onChange={value =>
                                                            this.setState({
                                                                text_timing: value
                                                            })}
                                                    />
                                                </Col>
                                            </Row>
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Book Maker Prompt Timing',
                                                'How long the book maker offer will stay on screen'
                                            )}
                                            hasFeedback
                                        >
                                            <Row>
                                                <Col span={6}>
                                                    {getFieldDecorator('bet_timing', {
                                                        initialValue: Number(bet_timing),
                                                        rules: [
                                                            {
                                                                required: true
                                                            }
                                                        ]
                                                    })(
                                                        <InputNumber
                                                            min={3000}
                                                            max={12000}
                                                            step={1000}
                                                            onChange={value =>
                                                                this.setState({
                                                                    bet_timing: value
                                                                })}
                                                        />
                                                    )}
                                                </Col>
                                                <Col span={16}>
                                                    <Slider
                                                        min={3000}
                                                        max={12000}
                                                        step={1000}
                                                        value={Number(bet_timing)}
                                                        onChange={value =>
                                                            this.setState({
                                                                bet_timing: value
                                                            })}
                                                    />
                                                </Col>
                                            </Row>
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Predictor video timing',
                                                'How long the predictor video will stay on screen'
                                            )}
                                            hasFeedback
                                        >
                                            <Row>
                                                <Col span={6}>
                                                    {getFieldDecorator('predictor_timing', {
                                                        initialValue: Number(predictor_timing),
                                                        rules: [
                                                            {
                                                                required: true
                                                            }
                                                        ]
                                                    })(
                                                        <InputNumber
                                                            min={6000}
                                                            max={12000}
                                                            step={1000}
                                                            onChange={value =>
                                                                this.setState({
                                                                    predictor_timing: value
                                                                })}
                                                        />
                                                    )}
                                                </Col>
                                                <Col span={16}>
                                                    <Slider
                                                        min={6000}
                                                        max={12000}
                                                        step={1000}
                                                        value={Number(predictor_timing)}
                                                        onChange={value =>
                                                            this.setState({
                                                                predictor_timing: value
                                                            })}
                                                    />
                                                </Col>
                                            </Row>
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Max Prompts',
                                                'Maximum number of main bet prompts to show (not including intro, predictor or exit prompts'
                                            )}
                                            hasFeedback
                                        >
                                            <Row>
                                                <Col span={6}>
                                                    {getFieldDecorator('max_promps', {
                                                        initialValue: max_promps,
                                                        rules: [
                                                            {
                                                                required: true
                                                            }
                                                        ]
                                                    })(
                                                        <InputNumber
                                                            min={1}
                                                            max={15}
                                                            step={1}
                                                            onChange={value =>
                                                                this.setState({
                                                                    max_promps: value
                                                                })}
                                                        />
                                                    )}
                                                </Col>

                                                <Col span={16}>
                                                    <Slider
                                                        min={1}
                                                        max={15}
                                                        step={1}
                                                        value={Number(max_promps)}
                                                        onChange={value =>
                                                            this.setState({
                                                                max_promps: value
                                                            })}
                                                    />
                                                </Col>
                                            </Row>
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Frozen Cells',
                                                'Number of frozen cells on race card if it cannot fit on screen'
                                            )}
                                            hasFeedback
                                        >
                                            <Row>
                                                <Col span={6}>
                                                    {getFieldDecorator('frozen_race_card_cells', {
                                                        initialValue: frozen_race_card_cells,
                                                        rules: [
                                                            {
                                                                required: true
                                                            }
                                                        ]
                                                    })(
                                                        <InputNumber
                                                            min={2}
                                                            max={10}
                                                            step={1}
                                                            onChange={value =>
                                                                this.setState({
                                                                    frozen_race_card_cells: value
                                                                })}
                                                        />
                                                    )}
                                                </Col>
                                                <Col span={16}>
                                                    <Slider
                                                        min={2}
                                                        max={10}
                                                        step={1}
                                                        value={Number(frozen_race_card_cells)}
                                                        onChange={value =>
                                                            this.setState({
                                                                frozen_race_card_cells: value
                                                            })}
                                                    />
                                                </Col>
                                            </Row>
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Brand Color',
                                                'Color of the bottom branding bar'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('main_color', {
                                                initialValue: main_color,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(<Input type="color" />)}
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Price Color',
                                                'Color of the odds box'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('odds_color', {
                                                initialValue: odds_color,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(<Input type="color" />)}
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Show Race Title',
                                                'Show tipster show the title of the race?'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('show_race_title', {
                                                initialValue: show_race_title,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Switch
                                                    checked={
                                                        !!(
                                                            show_race_title === 'true' ||
                                                            show_race_title === true
                                                        )
                                                    }
                                                />
                                            )}
                                        </FormItem>

                                        <FormItem
                                            {...smallLayout}
                                            label={this.toolTipTitle(
                                                'Price to show',
                                                'Source Of Race Prices'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('price_source', {
                                                initialValue: price_source,
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    <Option key="off" value="off">
                                                        Off
                                                    </Option>
                                                    <Option key="guide_only" value="guide_only">
                                                        Guide Only
                                                    </Option>
                                                    <Option
                                                        key="diffusion_only"
                                                        value="diffusion_only"
                                                    >
                                                        Diffusion Only
                                                    </Option>
                                                    <Option
                                                        key="guide_diffusion"
                                                        value="guide_diffusion"
                                                    >
                                                        Guide/Diffusion
                                                    </Option>
                                                </Select>
                                            )}
                                        </FormItem>
                                    </Col>
                                </Row>

                                <hr
                                    style={{
                                        marginLeft: '4.5%',
                                        marginBottom: '25px'
                                    }}
                                />

                                <FormItem
                                    {...formItemLayout}
                                    label={this.toolTipTitle(
                                        'Welcome Prompt',
                                        'Text that will appear in the first welcome prompt'
                                    )}
                                    hasFeedback
                                >
                                    {getFieldDecorator('welome_prompt_text', {
                                        initialValue: welome_prompt_text,
                                        rules: [
                                            {
                                                required: true
                                            }
                                        ]
                                    })(<Input />)}
                                </FormItem>

                                <FormItem
                                    {...formItemLayout}
                                    label={this.toolTipTitle(
                                        'Bottom Bar Text',
                                        'Text that will appear in the bottom branding bar'
                                    )}
                                    hasFeedback
                                >
                                    {getFieldDecorator('bottom_bar_text', {
                                        initialValue: bottom_bar_text,
                                        rules: [
                                            {
                                                required: true
                                            }
                                        ]
                                    })(<Input />)}
                                </FormItem>

                                <FormItem
                                    {...formItemLayout}
                                    label={this.toolTipTitle(
                                        'Book Maker Text',
                                        'Text that will appear in the book maker offer prompt'
                                    )}
                                    hasFeedback
                                >
                                    {getFieldDecorator('bet_prompt_offer_text', {
                                        initialValue: bet_prompt_offer_text,
                                        rules: [
                                            {
                                                required: true
                                            }
                                        ]
                                    })(<Input />)}
                                </FormItem>

                                <hr
                                    style={{
                                        marginLeft: '4.5%',
                                        marginBottom: '25px'
                                    }}
                                />

                                <Row>
                                    <Col span={8}>
                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle('Clock', 'Top left clock')}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('clock_is_24_hour', {
                                                initialValue: String(clock_is_24_hour) || 'true',
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    <Option value="true">24 hour</Option>
                                                    <Option value="false">12 hour</Option>
                                                </Select>
                                            )}
                                        </FormItem>
                                    </Col>

                                    <Col span={8}>
                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Race Time',
                                                'Race info time format'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('race_time_is_24_hour', {
                                                initialValue:
                                                    String(race_time_is_24_hour) || 'false',
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    <Option value="true">24 hour</Option>
                                                    <Option value="false">12 hour</Option>
                                                </Select>
                                            )}
                                        </FormItem>
                                    </Col>

                                    <Col span={8}>
                                        <FormItem
                                            {...leftLayout}
                                            label={this.toolTipTitle(
                                                'Next Race Time',
                                                'Top right, next race time format'
                                            )}
                                            hasFeedback
                                        >
                                            {getFieldDecorator('next_race_time_is_24_hour', {
                                                initialValue:
                                                    String(next_race_time_is_24_hour) || 'false',
                                                rules: [
                                                    {
                                                        required: true
                                                    }
                                                ]
                                            })(
                                                <Select>
                                                    <Option value="true">24 hour</Option>
                                                    <Option value="false">12 hour</Option>
                                                </Select>
                                            )}
                                        </FormItem>
                                    </Col>
                                </Row>

                                {id && (
                                    <FormItem {...tailFormItemLayout}>
                                        <Button htmlType="submit" type="primary">
                                            Submit
                                        </Button>
                                    </FormItem>
                                )}
                            </Form>
                        </TabPane>
                        {/*
                        //
                        //
                        //
                        //
                        //
                        */}
                        <TabPane tab="JSON" key="2">
                            <Form onSubmit={this.handleSubmit}>
                                <FormItem hasFeedback>
                                    {getFieldDecorator('json', {
                                        initialValue: JSON.stringify(json, undefined, 8),
                                        rules: [
                                            {
                                                required: true
                                            },
                                            {
                                                validator: jsonValidator
                                            }
                                        ]
                                    })(<Input type="textarea" rows={14} />)}
                                </FormItem>
                                {id && (
                                    <FormItem>
                                        <Button htmlType="submit" type="primary">
                                            Submit
                                        </Button>
                                    </FormItem>
                                )}
                            </Form>
                        </TabPane>
                    </Tabs>
                </div>
            </div>
        )
    }
}

export default Form.create()(Screen)
