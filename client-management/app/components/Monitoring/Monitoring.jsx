import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Button, Row, Tabs } from 'antd'

import css from './Monitoring.less'

const TabPane = Tabs.TabPane

@inject('globalStore', 'routing')
@observer
class Monitoring extends Component {
    render() {
        return (
            <div>
                <Row type="flex" justify="space-between">
                    <h1>Monitoring</h1>

                    <Button type="primary" onClick={() => this.setState({ x: true })}>
                        Add New Monitoring
                    </Button>
                </Row>

                <div className="main__body-container">
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="AWS" key="1">
                            AWS INFO
                        </TabPane>

                        <TabPane tab="Server Stats" key="2">
                            Sever info
                        </TabPane>
                    </Tabs>
                </div>
            </div>
        )
    }
}

export default Monitoring
