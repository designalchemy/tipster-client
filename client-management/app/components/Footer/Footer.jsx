import React, { Component } from 'react'

import css from './Footer.less'

class Footer extends Component {
    render() {
        return (
            <footer className="footer__container">
                <p className="footer__copy">
                    Developed by{' '}
                    <a href="https://korelogic.co.uk/" target="_blank">
                        Korelogic Limited
                    </a>
                </p>
            </footer>
        )
    }
}

export default Footer
