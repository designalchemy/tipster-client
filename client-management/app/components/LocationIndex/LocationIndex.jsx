import React, { Component } from 'react'
import { Table, Icon, Button, Row, Col, Popconfirm, Input, message } from 'antd'
import ModalSwitcher from '../Modals/ModalSwitcher'
import { inject, observer } from 'mobx-react'
import { observable, toJS } from 'mobx'
import GetData from '../../tools/GetData'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { sortAlpha } from '../../tools/utils'

import css from './LocationIndex.less'

@inject('globalStore')
@observer
class Locations extends Component {
    @observable
    pagination = {
        current: 1,
        pageSize: 50,
        total: 0
    }

    @observable
    search = {
        query: '',
        type: ''
    }

    @observable
    editLocation = {
        modal: false,
        data: {}
    }

    @observable cities = {}
    @observable orgs = {}

    @observable
    sorting = {
        field: '',
        order: ''
    }

    @observable addLocationModal = false

    @observable loading = true

    @observable filters = {}

    @observable
    filterName = {
        visible: false,
        search: ''
    }

    @observable
    filterPostCode = {
        visible: false,
        search: ''
    }

    @observable
    filterShopID = {
        visible: false,
        search: ''
    }

    @observable
    appliedFilters = {
        name: false,
        shop_id: false,
        post_code: false
    }

    @observable tableData = []

    componentDidMount() {
        this.getData()
    }

    getData = sort => {
        this.editLocation.modal = false
        this.editLocation.data = {}
        this.addLocationModal = false

        GetData.locations('GET', {
            pagination: JSON.stringify(this.pagination),
            filters: JSON.stringify(this.filters),
            search: JSON.stringify(this.search),
            sorting: JSON.stringify(this.sorting)
        }).then(json => {
            this.handleResponse(json, true)
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.pagination.total = json.total
                this.tableData = json.data
                this.search.query = ''
                this.loading = false
                this.cities = json.cities
                this.orgs = json.orgs
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    deleteLocation = id => {
        this.loading = true

        GetData.locations('DELETE', { id }).then(json => {
            this.handleResponse(json)
        })
    }

    onChange = (pagination, filters, sorter) => {
        this.loading = true
        this.sorting.field = sorter.field
        this.sorting.order = sorter.order
        this.pagination.current = pagination.current
        this.filters = filters
        this.getData()
    }

    launchModal = data => {
        this.editLocation.data = data
        this.editLocation.modal = true
    }

    onInputChange = (e, type) => {
        this[type].search = e.target.value
    }

    onSearch = (type, key) => {
        this.loading = true
        this[key].visible = false
        this.appliedFilters = _.map(this.appliedFilters, item => (item = false))
        this.appliedFilters[type] = true
        this.search.query = this[key].search
        this[key].search = ''
        this.search.type = type
        this.getData()
    }

    clearSearch = key => {
        this.loading = true
        this[key].visible = false
        this.appliedFilters = _.map(this.appliedFilters, item => (item = false))
        this[key].search = ''
        this.search.query = ''
        this.getData()
    }

    renderLink = (length, link, text) => {
        if (length) {
            return (
                <span>
                    <Link className="table-link" to={`/management/${link}`}>
                        {text}
                    </Link>
                </span>
            )
        } else {
            return <span>{text}</span>
        }
    }

    render() {
        const columns = [
            {
                title: 'Location Name',
                dataIndex: 'name',
                sorter: true,
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.lnameInput = ele)}
                            placeholder="Search name"
                            value={this.filterName.search}
                            onChange={event => this.onInputChange(event, 'filterName')}
                            onPressEnter={() => this.onSearch('name', 'filterName')}
                        />
                        <Button type="primary" onClick={() => this.onSearch('name', 'filterName')}>
                            Search
                        </Button>
                        <Button
                            type="primary"
                            ghost
                            style={{ marginLeft: '5px' }}
                            onClick={() => this.clearSearch('filterName')}
                        >
                            Reset
                        </Button>
                    </div>
                ),
                filterIcon: (
                    <Icon
                        type="zoom-1"
                        style={{
                            color: this.appliedFilters.name ? '#108ee9' : '#aaa'
                        }}
                    />
                ),
                filterDropdownVisible: this.filterName.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.filterName.visible = visible
                }
            },
            {
                title: 'Shop ID',
                dataIndex: 'shop_id',
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.searchShopIdInput = ele)}
                            placeholder="Search Shop ID"
                            value={this.filterShopID.search}
                            onChange={event => this.onInputChange(event, 'filterShopID')}
                            onPressEnter={() => this.onSearch('shop_id', 'filterShopID')}
                        />
                        <Button
                            type="primary"
                            onClick={() => this.onSearch('shop_id', 'filterShopID')}
                        >
                            Search
                        </Button>
                        <Button
                            type="primary"
                            ghost
                            style={{ marginLeft: '5px' }}
                            onClick={() => this.clearSearch('filterShopID')}
                        >
                            Reset
                        </Button>
                    </div>
                ),
                filterIcon: (
                    <Icon
                        type="zoom-1"
                        style={{
                            color: this.appliedFilters.shop_id ? '#108ee9' : '#aaa'
                        }}
                    />
                ),
                filterDropdownVisible: this.filterShopID.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.filterShopID.visible = visible
                }
            },
            {
                title: 'Organisation',
                dataIndex: 'brand_name',
                sorter: true,
                filters: _.map(this.orgs, item => ({ text: item, value: item }))
            },
            {
                title: 'Address line 1',
                dataIndex: 'address_1',
                sorter: true
            },
            {
                title: 'Address line 2',
                dataIndex: 'address_2',
                sorter: true
            },
            {
                title: 'City',
                dataIndex: 'city',
                sorter: true,
                filters: _.map(this.cities, item => ({ text: item, value: item }))
            },
            {
                title: 'Post Code',
                dataIndex: 'post_code',
                sorter: true,
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.searchPostCodeInput = ele)}
                            placeholder="Search Post Code"
                            value={this.filterPostCode.search}
                            onChange={event => this.onInputChange(event, 'filterPostCode')}
                            onPressEnter={() => this.onSearch('post_code', 'filterPostCode')}
                        />
                        <Button
                            type="primary"
                            onClick={() => this.onSearch('post_code', 'filterPostCode')}
                        >
                            Search
                        </Button>
                        <Button
                            type="primary"
                            ghost
                            style={{ marginLeft: '5px' }}
                            onClick={() => this.clearSearch('filterPostCode')}
                        >
                            Reset
                        </Button>
                    </div>
                ),
                filterIcon: (
                    <Icon
                        type="zoom-1"
                        style={{
                            color: this.appliedFilters.post_code ? '#108ee9' : '#aaa'
                        }}
                    />
                ),
                filterDropdownVisible: this.filterPostCode.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.filterPostCode.visible = visible
                }
            },
            {
                title: 'RPD Auth IDs',
                dataIndex: 'rpd_auth',
                render: (text, record) =>
                    this.renderLink(
                        text.length,
                        `auth?type=RPD&location=${record.id}`,
                        `${text.length} RPD Auth IDs`
                    )
            },
            {
                title: 'RPT Auth IDs',
                dataIndex: 'rpt_auth',
                render: (text, record) =>
                    this.renderLink(
                        text.length,
                        `auth?type=RPD&location=${record.id}`,
                        `${text.length} RPT Auth IDs`
                    )
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a
                            href="#"
                            onClick={e => {
                                e.preventDefault()
                                this.launchModal(record)
                            }}
                        >
                            Edit <Icon type="pencil-1" />
                        </a>

                        <span className="ant-divider" />

                        <Popconfirm
                            title="Are you sure delete this Locaiton?"
                            onConfirm={() => this.deleteLocation(record.id)}
                            placement="left"
                            okText="Yes"
                            cancelText="No"
                        >
                            <a href="#">
                                Delete <Icon type="trash-1" />
                            </a>
                        </Popconfirm>
                    </span>
                )
            }
        ]

        return (
            <div>
                <ModalSwitcher
                    title="Add New Location"
                    modal="EditLocation"
                    showModal={this.addLocationModal}
                    closeModal={() => this.getData()}
                />

                <ModalSwitcher
                    title="Edit Location"
                    modal="EditLocation"
                    data={this.editLocation.data}
                    showModal={this.editLocation.modal}
                    closeModal={() => this.getData()}
                />

                <Row type="flex" justify="space-between">
                    <h1>Locations</h1>

                    <Button type="primary" onClick={() => (this.addLocationModal = true)}>
                        Add New Locations
                    </Button>
                </Row>

                <div className="table__body-container">
                    <Table
                        columns={columns}
                        dataSource={toJS(this.tableData)}
                        onChange={this.onChange}
                        loading={this.loading}
                        pagination={this.pagination}
                        rowKey={record => record.id}
                        onRowDoubleClick={row =>
                            this.props.history.push(`/management/location/${row.id}`)}
                    />
                </div>
            </div>
        )
    }
}

export default Locations
