import React, { Component } from 'react'
import { Row, Tabs } from 'antd'
import { observable } from 'mobx'
import ChangePasswordForm from '../ChangePasswordForm/ChangePasswordForm'
import ChangeUserForm from '../ChangeUserForm/ChangeUserForm'

import css from './Settings.less'

const TabPane = Tabs.TabPane

class Settings extends Component {
    @observable activeTab = '1'

    componentDidMount() {
        this.props.onRef(this)
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    submitToggle = () => {
        if (this.activeTab === '1') {
            this.childPassword.handleSubmit()
        } else {
            this.childUser.handleSubmit()
        }
    }

    render() {
        return (
            <div>
                <Tabs defaultActiveKey="1" onChange={key => (this.activeTab = key)}>
                    <TabPane tab="Change Password" key="1">
                        <ChangePasswordForm
                            onRef={ref => (this.childPassword = ref)}
                            closeModal={this.props.closeModal}
                        />
                    </TabPane>

                    <TabPane tab="Change User Details" key="2">
                        <ChangeUserForm
                            onRef={ref => (this.childUser = ref)}
                            closeModal={this.props.closeModal}
                        />
                    </TabPane>
                </Tabs>
            </div>
        )
    }
}

export default Settings
