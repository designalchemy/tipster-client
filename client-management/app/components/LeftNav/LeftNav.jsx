import React, { Component } from 'react'
import { Menu, Icon } from 'antd'
import { Link } from 'react-router-dom'

import { inject, observer } from 'mobx-react'

import css from './LeftNav.less'

const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup

@inject('routing')
class LeftNav extends Component {
    state = {
        current: '1',
        openKeys: []
    }

    handleClick = e => {
        // console.log('menu click ', e)
    }

    render() {
        const currentActive = this.props.routing.location.pathname

        return (
            <div style={{ height: '100%' }}>
                <Menu
                    onClick={this.handleClick}
                    defaultSelectedKeys={[currentActive]}
                    selectedKeys={[currentActive]}
                    mode="inline"
                    className="left-nav__container"
                >
                    <Menu.Item key="/screens">
                        <Link to="/management/screens">
                            <Icon type="computer-1" />Screens
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/configuration">
                        <Link to="/management/configuration">
                            <Icon type="notebook-2" />Configurations
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/auth">
                        <Link to="/management/auth">
                            <Icon type="lock-2" />Auth
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/users">
                        <Link to="/management/users">
                            <Icon type="user-1" />Users
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/organisations">
                        <Link to="/management/customers">
                            <Icon type="globe-1" />Organisations
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/monitoring">
                        <Link to="/management/monitoring">
                            <Icon type="activity-monitor-1" />Monitoring
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/settings">
                        <Link to="/management/settings">
                            <Icon type="settings-1" />Settings
                        </Link>
                    </Menu.Item>
                </Menu>
            </div>
        )
    }
}

export default LeftNav
