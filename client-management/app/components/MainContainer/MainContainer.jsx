import React, { Component } from 'react'

import css from './MainContainer.less'

class MainContainer extends Component {
    render() {
        return (
            <div className="main__container">
                {React.cloneElement(this.props.children, {
                    data: this.props.json
                })}
            </div>
        )
    }
}

export default MainContainer
