import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import GetData from '../../tools/GetData'
import {
    Form,
    Input,
    Icon,
    Cascader,
    Select,
    Row,
    Col,
    Checkbox,
    Button,
    AutoComplete,
    TimePicker,
    message
} from 'antd'
import moment from 'moment'
import { containsNumber, strongPass, formItemLayout, tailFormItemLayout } from '../../tools/utils'

import css from './Register.less'

const FormItem = Form.Item
const Option = Select.Option
const AutoCompleteOption = AutoComplete.Option

@inject('globalStore')
@observer
class Register extends React.Component {
    state = {
        confirmDirty: false
    }

    componentDidMount() {
        this.getJson()
        this.props.onRef(this)
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    getJson = () => {
        GetData.orgs('GET').then(json => {
            this.setState({
                organisation: json.data
            })
        })
    }

    handleSubmit = e => {
        e && e.preventDefault()
        const isRegister = _.get(this.props, ['data', 'email'], false)
        const type = isRegister ? 'PUT' : 'POST'

        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                if (isRegister) {
                    values.id = this.props.data.id
                }
                if (isRegister && values.email === this.props.data.email) {
                    values.editEmail = false
                }
                if (isRegister && values.email !== this.props.data.email) {
                    values.editEmail = true
                }

                console.log(values)
                GetData.users(type, values).then(json => {
                    this.handleResponse(json)
                })
            }
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            if (load) {
                this.setState({ getData: json, loading: false })
            }

            if (!load) {
                this.props.form.resetFields()
                this.props.callback()
                message.success('Action succesful')
            }
        }
    }

    handleConfirmBlur = e => {
        const value = e.target.value
        this.setState({ confirmDirty: this.state.confirmDirty || !!value })
    }

    checkPassword = (rule, value, callback) => {
        const form = this.props.form
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!')
        } else {
            callback()
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { autoCompleteResult } = this.state

        const isRegister = _.get(this.props, ['data', 'email'], false)

        return (
            <div className="register-form__container">
                <Form onSubmit={this.handleSubmit} className="register-form__body">
                    <FormItem {...formItemLayout} label={<span>First Name</span>} hasFeedback>
                        {getFieldDecorator('firstName', {
                            initialValue: _.get(this.props, ['data', 'firstName']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input a first name!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Last Name</span>} hasFeedback>
                        {getFieldDecorator('secondName', {
                            initialValue: _.get(this.props, ['data', 'secondName']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input a last name!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label="E-mail" hasFeedback>
                        {getFieldDecorator('email', {
                            initialValue: _.get(this.props, ['data', 'email']),
                            rules: [
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!'
                                },
                                {
                                    required: true,
                                    message: 'Please input a E-mail!'
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Role</span>} hasFeedback>
                        {getFieldDecorator('role', {
                            initialValue: _.get(this.props, ['data', 'role']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please select a role!',
                                    whitespace: true
                                }
                            ]
                        })(
                            <Select>
                                <Option value="admin">Admin</Option>
                                <Option value="user">User</Option>
                            </Select>
                        )}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Organisation</span>} hasFeedback>
                        {getFieldDecorator('organisation', {
                            initialValue: _.get(this.props, ['data', 'organisation']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please select a organisation!',
                                    whitespace: true
                                }
                            ]
                        })(
                            <Select>
                                {_.map(this.state.organisation, item => (
                                    <Option key={item.id} value={item.id}>
                                        {item.brand_name}
                                    </Option>
                                ))}
                            </Select>
                        )}
                    </FormItem>

                    {!isRegister && (
                        <FormItem {...formItemLayout} label="Password" hasFeedback>
                            {getFieldDecorator('password', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please input a password!'
                                    },
                                    {
                                        validator: strongPass
                                    }
                                ]
                            })(<Input type="password" />)}
                        </FormItem>
                    )}

                    {!isRegister && (
                        <FormItem {...formItemLayout} label="Confirm Password" hasFeedback>
                            {getFieldDecorator('confirm', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please confirm the password!'
                                    },
                                    {
                                        validator: this.checkPassword
                                    }
                                ]
                            })(<Input type="password" onBlur={this.handleConfirmBlur} />)}
                        </FormItem>
                    )}
                </Form>
            </div>
        )
    }
}

export default Form.create()(Register)
