import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Button, Row } from 'antd'

import css from './Dashboard.less'

@inject('globalStore', 'routing')
@observer
class Dashboard extends Component {
    render() {
        return (
            <div className="">
                <Row type="flex" justify="space-between">
                    <h1>Dashboard</h1>
                </Row>

                <div className="main__body-container" />
            </div>
        )
    }
}

export default Dashboard
