import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { observable } from 'mobx'
import { Icon, Row } from 'antd'
import { Link } from 'react-router-dom'
import ModalSwitcher from '../Modals/ModalSwitcher'
import Gravatar from 'react-gravatar'

import css from './Header.less'

@inject('globalStore', 'routing')
@observer
class Header extends Component {
    @observable editAccountModal = false

    render() {
        const { firstName } = this.props.globalStore.user
        const { email } = this.props.globalStore.user
        const hasToken = this.props.globalStore.token

        return (
            <header className="header__container">
                <ModalSwitcher
                    title="Edit User Details"
                    modal="EditAccount"
                    showModal={this.editAccountModal}
                    closeModal={() => (this.editAccountModal = false)}
                />
                <Row type="flex" justify="space-between" align="center" className="container">
                    {/*<Link to="/management/"> remove till implmented - remove span and replace with Link */}
                    <span>
                        <img
                            src="/management/assets/racing-post.png"
                            alt="logo"
                            className="header__logo"
                        />
                        <h1>MANAGEMENT</h1>
                    </span>
                    {/*</Link>*/}

                    {hasToken &&
                        <Link
                            className="gravatar-link"
                            to="#"
                            onClick={() => (this.editAccountModal = true)}
                        >
                            <Gravatar
                                size={24}
                                email={email}
                                className="gravatar-image"
                                rating="pg"
                                default="retro"
                                protocol="https://"
                            />
                            {`${firstName}`}
                        </Link>}
                </Row>
            </header>
        )
    }
}

export default Header
