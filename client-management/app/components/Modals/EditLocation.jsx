import React, { Component } from 'react'
import { Modal, Button, Form, Input, Col, message, Select } from 'antd'
import GetData from '../../tools/GetData'
import { inject, observer } from 'mobx-react'

const FormItem = Form.Item
const Option = Select.Option

@inject('globalStore')
@observer
class EditLocation extends Component {
    state = {
        organisation: {}
    }

    componentDidMount() {
        this.getJson()
    }

    getJson = () => {
        GetData.orgs('GET').then(json => {
            this.setState({
                organisation: json.data
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault()
        const isRegister = _.get(this.props, ['data', 'name'], false)
        const type = isRegister ? 'PUT' : 'POST'

        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                if (isRegister) {
                    values.id = this.props.data.id
                }

                GetData.locations(type, values).then(json => {
                    this.handleResponse(json)
                })
            }
        })
        if (this.props.callback) {
            this.props.callback()
        }
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.setState({ getData: json, loading: false })
            }

            if (!load) {
                this.props.form.resetFields()
                this.props.closeModal()
                message.success('Success')
            }
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 }
            }
        }

        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0
                },
                sm: {
                    span: 14,
                    offset: 8
                }
            }
        }

        return (
            <Modal
                title={this.props.title}
                visible={this.props.visible}
                okText="Submit"
                onOk={this.handleSubmit}
                onCancel={this.props.closeModal}
            >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem {...formItemLayout} label={<span>Name</span>} hasFeedback>
                        {getFieldDecorator('name', {
                            initialValue: _.get(this.props, ['data', 'name']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input the name!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Shop ID</span>} hasFeedback>
                        {getFieldDecorator('shop_id', {
                            initialValue: _.get(this.props, ['data', 'shop_id']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input the shop ID!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Organisation</span>} hasFeedback>
                        {getFieldDecorator('owner', {
                            initialValue: _.get(this.props, ['data', 'owner']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please select a organisation!',
                                    whitespace: true
                                }
                            ]
                        })(
                            <Select>
                                {_.map(this.state.organisation, item => (
                                    <Option key={item.id} value={item.id}>
                                        {item.brand_name}
                                    </Option>
                                ))}
                            </Select>
                        )}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Address Line 1</span>} hasFeedback>
                        {getFieldDecorator('address_1', {
                            initialValue: _.get(this.props, ['data', 'address_1']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input the address line 1!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Address Line 2</span>} hasFeedback>
                        {getFieldDecorator('address_2', {
                            initialValue: _.get(this.props, ['data', 'address_2']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input the address line 2!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>City</span>} hasFeedback>
                        {getFieldDecorator('city', {
                            initialValue: _.get(this.props, ['data', 'city']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input the city!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>PostCode</span>} hasFeedback>
                        {getFieldDecorator('post_code', {
                            initialValue: _.get(this.props, ['data', 'post_code']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input the post code!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

export default Form.create()(EditLocation)
