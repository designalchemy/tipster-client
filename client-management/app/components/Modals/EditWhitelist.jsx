import React, { Component } from 'react'
import { Modal, Button, Row, Col, Form, Input, Select, Switch, message } from 'antd'
import rangeCheck from 'range_check'
import cidrRange from 'cidr-range'
import { observable } from 'mobx'
import GetData from '../../tools/GetData'
import { formItemLayout, tailFormItemLayout, ipRanges } from '../../tools/utils'
import { observer } from 'mobx-react'

const FormItem = Form.Item
@observer
class EditWhitelist extends React.Component {
    constructor(props) {
        super(props)
    }

    @observable cidrRanges = []
    @observable orgs = []
    @observable locations = []
    @observable configs = []
    @observable ipType = 4

    @observable editModal = false
    @observable hideConfig = false
    @observable calcRange = false

    componentDidMount() {
        this.getData()
        if (this.props.data) this.editModal = true
    }

    componentDidUpdate() {
        this.checkAutoSelect(this.props.form.getFieldValue('auto_auth'))
    }

    handleSubmit = () => {
        const type = this.editModal ? 'PUT' : 'POST'
        this.cidrRanges = []
        this.props.form.validateFieldsAndScroll((err, values) => {
            const data = values
            if (this.editModal) {
                data.id = this.props.data.data.id
                data.old_ip_range = this.props.data.data.ip_range
            }
            data.configId = _.map(data.configs, config => ({
                auth: true,
                id: config
            }))
            delete data.configs

            data.location_softName = _.get(_.find(this.locations, { id: data.locationId }), 'name')
            data.brand_softName = _.get(_.find(this.orgs, { id: data.brandId }), 'brand_name')
            data.ip_type = this.getIPType(data.ip_range)

            if (!err) {
                GetData.IP(type, { ...data }).then(resp => {
                    this.handleResponse(resp)
                })
            }
        })
    }

    cidrValidator = (rule, value, callback) => {
        this.cidrRanges = []
        if (!rangeCheck.isRange(value)) {
            callback('Please enter a valid CIDR!')
        }
        callback()
    }

    getConfigsArray = configs => _.map(configs, conf => conf.id)

    handleResponse = json => {
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            this.props.closeModal()
            this.props.form.resetFields()
            message.success('Success!')
        }
    }

    getData = () => {
        GetData.locations('GET').then(resp => {
            this.locations = resp.data
        })
        GetData.orgs('GET').then(resp => {
            this.orgs = resp.data
        })
        GetData.dashboard('GET').then(resp => {
            this.configs = resp.data
        })
    }

    getIPType = value => {
        if (value.indexOf(':') !== -1) {
            return 6
        }
        return 4
    }

    filterSelect = (input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0

    calculateRange = () => {
        const values = this.props.form.getFieldsValue()
        const range = cidrRange(values.ip_range)
        this.cidrRanges.push(range[0])
        this.cidrRanges.push(range[range.length - 1])
        this.calcRange = true
    }

    checkAutoSelect = value => {
        if (value) {
            this.hideConfig = true
        } else {
            this.hideConfig = false
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <Modal
                title={this.props.title}
                visible={this.props.visible}
                okText="Submit"
                onOk={() => {
                    this.handleSubmit()
                }}
                onCancel={() => {
                    this.cidrRanges = []
                    this.props.closeModal()
                }}
            >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem {...formItemLayout} label={<span>Soft Name</span>} hasFeedback>
                        {getFieldDecorator('softName', {
                            initialValue: _.get(this.props.data, ['data', 'softName']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please enter a soft name!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>
                    <FormItem {...formItemLayout} label={<span>CIDR Range</span>} hasFeedback>
                        {getFieldDecorator('ip_range', {
                            initialValue: _.get(this.props.data, ['data', 'ip_range']),
                            rules: [
                                {
                                    required: true,
                                    message: 'CIDR is required!',
                                    whitespace: false
                                },
                                {
                                    validator: this.cidrValidator
                                }
                            ]
                        })(<Input />)}
                    </FormItem>
                    <FormItem {...formItemLayout} label={<span>Customer</span>} hasFeedback>
                        {getFieldDecorator('brandId', {
                            initialValue: _.get(this.props.data, ['data', 'brandId']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please select a brand!',
                                    whitespace: false
                                }
                            ]
                        })(
                            <Select>
                                {_.map(this.orgs, value => (
                                    <Select.Option key={value} value={value.id}>
                                        {value.brand_name}
                                    </Select.Option>
                                ))}
                            </Select>
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label={<span>Location</span>} hasFeedback>
                        {getFieldDecorator('locationId', {
                            initialValue: _.get(this.props.data, ['data', 'locationId']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please select a location!',
                                    whitespace: false
                                }
                            ]
                        })(
                            <Select>
                                {_.map(this.locations, value => (
                                    <Select.Option key={value.id} value={value.id}>
                                        {value.name}
                                    </Select.Option>
                                ))}
                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label={<span>Full Configs Access</span>}
                        hasFeedback
                    >
                        {getFieldDecorator('auto_auth', {
                            valuePropName: 'checked',
                            initialValue: _.get(this.props.data, ['data', 'auto_auth']) || false,
                            rules: [
                                {
                                    required: !this.hideConfig
                                }
                            ]
                        })(<Switch onChange={value => this.checkAutoSelect(value)} />)}
                    </FormItem>
                    {!this.hideConfig && (
                        <FormItem {...formItemLayout} label={<span>Configs</span>} hasFeedback>
                            {getFieldDecorator('configs', {
                                initialValue: this.getConfigsArray(
                                    _.get(this.props.data, ['data', 'configId'])
                                ),
                                rules: [
                                    {
                                        required: this.hideConfig,
                                        message: 'Please select allowed configs!'
                                    }
                                ]
                            })(
                                <Select
                                    mode="multiple"
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        this.filterSelect(input, option)}
                                >
                                    {_.map(this.configs, conf => (
                                        <Select.Option
                                            value={conf._dashboard_id}
                                            key={conf.config.soft_name}
                                        >
                                            {conf.config.soft_name}
                                        </Select.Option>
                                    ))}
                                </Select>
                            )}
                        </FormItem>
                    )}
                    <FormItem {...tailFormItemLayout}>
                        <Button onClick={() => this.calculateRange()}>Calculate Ranges</Button>
                    </FormItem>
                    {this.cidrRanges.length === 2 &&
                        this.calcRange && (
                            <FormItem {...tailFormItemLayout}>
                                <p>
                                    {' '}
                                    IPS in range: {this.cidrRanges[0]} to{' '}
                                    {this.cidrRanges[this.cidrRanges.length - 1]}
                                </p>
                            </FormItem>
                        )}
                </Form>
            </Modal>
        )
    }
}

export default Form.create()(EditWhitelist)
