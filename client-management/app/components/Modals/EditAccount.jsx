import React, { Component } from 'react'
import { Modal, Button } from 'antd'
import Settings from '../Settings/Settings'

class EditAccount extends React.Component {
    render() {
        return (
            <Modal
                width={800}
                style={{ top: 50 }}
                title={this.props.title}
                visible={this.props.visible}
                okText="Submit"
                onOk={() => {
                    this.child.submitToggle()
                }}
                onCancel={this.props.closeModal}
            >
                <Settings closeModal={this.props.closeModal} onRef={ref => (this.child = ref)} />
            </Modal>
        )
    }
}

export default EditAccount
