import React, { Component } from 'react'
import * as modals from './'

const ModalSwitcher = props => {
    const SelectModal = modals[props.modal]

    return (
        <SelectModal
            closeModal={props.closeModal}
            data={props.data}
            title={props.title}
            visible={props.showModal}
            callback={props.callback}
        />
    )
}

export default ModalSwitcher
