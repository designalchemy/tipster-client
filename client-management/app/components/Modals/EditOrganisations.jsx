import React, { Component } from 'react'
import { Modal, Button, Form, Input, Col, message } from 'antd'
import GetData from '../../tools/GetData'
import { inject, observer } from 'mobx-react'
import { formItemLayout, tailFormItemLayout } from '../../tools/utils'

const FormItem = Form.Item

@inject('globalStore')
@observer
class EditOrganisations extends Component {
    constructor(props) {
        super(props)
    }

    handleSubmit = e => {
        e.preventDefault()
        const isRegister = _.get(this.props, ['data', 'brand_name'], false)
        const type = isRegister ? 'PUT' : 'POST'

        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                if (isRegister) {
                    values.id = this.props.data.id
                }

                GetData.orgs(type, values).then(json => {
                    this.handleResponse(json)
                })
            }
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            if (load) {
                console.log(json)
                this.setState({ getData: json, loading: false })
            }

            if (!load) {
                this.props.form.resetFields()
                this.props.callback()
                message.success('Success')
            }
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <Modal
                title={this.props.title}
                visible={this.props.visible}
                onOk={this.handleSubmit}
                okText="Submit"
                onCancel={this.props.closeModal}
            >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem {...formItemLayout} label={<span>Name</span>} hasFeedback>
                        {getFieldDecorator('brand_name', {
                            initialValue: _.get(this.props, ['data', 'brand_name']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input the name!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Logo</span>} hasFeedback>
                        {getFieldDecorator('brand_logo', {
                            initialValue: _.get(this.props, ['data', 'brand_logo']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your logo name!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

export default Form.create()(EditOrganisations)
