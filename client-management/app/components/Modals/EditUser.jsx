import React, { Component } from 'react'
import { Modal, Button } from 'antd'
import Register from '../Register/Register'

class EditUser extends React.Component {
    render() {
        return (
            <Modal
                title={this.props.title}
                visible={this.props.visible}
                okText="Submit"
                onOk={() => {
                    this.child.handleSubmit()
                }}
                onCancel={this.props.closeModal}
            >
                <Register
                    callback={this.props.closeModal}
                    data={this.props.data}
                    onRef={ref => (this.child = ref)}
                />
            </Modal>
        )
    }
}

export default EditUser
