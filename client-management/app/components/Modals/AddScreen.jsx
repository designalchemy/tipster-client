import React, { Component } from 'react'
import { Modal, Button } from 'antd'
import Screen from '../Screen/Screen'

class AddScreen extends React.Component {
    render() {
        return (
            <Modal
                width={'80%'}
                style={{ top: 50 }}
                title={this.props.title}
                visible={this.props.visible}
                okText="Submit"
                onOk={() => {
                    this.child.handleSubmit()
                }}
                onCancel={this.props.closeModal}
            >
                <Screen
                    closeModal={this.props.closeModal}
                    onRef={ref => (this.child = ref)}
                    callback={this.props.callback}
                />
            </Modal>
        )
    }
}

export default AddScreen
