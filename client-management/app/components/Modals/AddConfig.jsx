import React, { Component } from 'react'
import { Modal, Button, Form, Input, message, Select } from 'antd'
import GetData from '../../tools/GetData'
import Configuration from '../Configuration/Configuration'
import { inject, observer } from 'mobx-react'
import { formItemLayout } from '../../tools/utils'

@inject('globalStore')
@observer
class AddConfig extends React.Component {
    render() {
        return (
            <Modal
                width={800}
                title={this.props.title}
                visible={this.props.visible}
                okText="Submit"
                onOk={() => this.child.handleSubmit()}
                onCancel={this.props.closeModal}
            >
                <Configuration
                    onRef={ref => {
                        this.child = ref
                    }}
                    closeModal={this.props.closeModal}
                    callback={this.props.callback}
                />
            </Modal>
        )
    }
}

export default AddConfig
