import React, { Component } from 'react'
import { Modal, Button, Form, Input, message } from 'antd'
import Register from '../Register/Register'
import { formItemLayout, tailFormItemLayout } from '../../tools/utils'
import GetData from '../../tools/GetData'

const FormItem = Form.Item

class ForgotPassword extends React.Component {
    handleSubmit = e => {
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                GetData.email('POST', values).then(json => {
                    this.handleResponse(json)
                })
            }
        })
    }

    handleResponse = json => {
        if (json.error) {
            message.error(`${json.message}`)
        } else {
            this.props.form.resetFields()
            this.props.closeModal()
            message.success('Request success - please check your email')
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form

        return (
            <Modal
                title={this.props.title}
                visible={this.props.visible}
                okText="Submit"
                cancelText="Cancel"
                onOk={() => {
                    this.handleSubmit()
                }}
                onCancel={this.props.closeModal}
            >
                <Form onSubmit={this.handleSubmit}>
                    <p style={{ marginBottom: '20px' }}>
                        Please enter your email address and we will email you a reset link
                    </p>

                    <FormItem {...formItemLayout} label={<span>Email</span>} hasFeedback>
                        {getFieldDecorator('email', {
                            rules: [
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!'
                                },
                                {
                                    required: true,
                                    message: 'Please enter a email!',
                                    whitespace: false
                                }
                            ]
                        })(<Input />)}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

export default Form.create()(ForgotPassword)
