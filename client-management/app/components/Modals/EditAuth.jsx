import React, { Component } from 'react'
import { Modal, Button, Form, Input, message, Select, Switch } from 'antd'
import GetData from '../../tools/GetData'
import { inject, observer } from 'mobx-react'
import { formItemLayout } from '../../tools/utils'

const FormItem = Form.Item
const Option = Select.Option

@inject('globalStore')
@observer
class EditAuth extends Component {
    state = {
        location: {}
    }

    componentDidMount() {
        this.getJson()
    }

    getJson = () => {
        GetData.locations().then(json => {
            this.setState({
                location: json.data
            })
        })
        GetData.orgs('GET').then(json => {
            this.setState({
                organisation: json.data
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault()

        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                values.id = this.props.data.id

                GetData.auth('PUT', values).then(json => {
                    this.handleResponse(json)
                })
            }
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.setState({ getData: json, loading: false })
            }

            if (!load) {
                if (this.props.data.authAfter) {
                    const body = {
                        id: this.props.data.id,
                        ref: this.props.data.configRef,
                        auth: !this.props.data.configAuth
                    }
                    this.props.data.authAfter = false
                    GetData.auth('PUT', body).then(json => {
                        this.handleResponse(json)
                    })
                } else {
                    this.props.form.resetFields()
                    this.props.callback()
                    message.success('Success')
                }
            }
        }
    }

    refreshDevice = id => {
        GetData.refesh('PUT', { id }).then(json => {
            message.success('Screen has been refreshed')
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        return (
            <Modal
                title={this.props.title}
                visible={this.props.visible}
                onOk={this.handleSubmit}
                okText="Submit"
                onCancel={this.props.closeModal}
            >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem {...formItemLayout} label={<span>Soft Name</span>} hasFeedback>
                        {getFieldDecorator('softName', {
                            initialValue: _.get(this.props, ['data', 'softName']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please select a soft name!',
                                    whitespace: true
                                }
                            ]
                        })(<Input />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Location</span>} hasFeedback>
                        {getFieldDecorator('location', {
                            initialValue: _.get(this.props, ['data', 'location']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please select a location!'
                                }
                            ]
                        })(
                            <Select>
                                {_.map(this.state.location, item => (
                                    <Option key={`${item.id}${item.name}`} value={item.id}>
                                        {item.name}
                                    </Option>
                                ))}
                            </Select>
                        )}
                    </FormItem>

                    <FormItem {...formItemLayout} label={<span>Organisation</span>} hasFeedback>
                        {getFieldDecorator('brandId', {
                            initialValue: _.get(this.props, ['data', 'brandId']),
                            rules: [
                                {
                                    required: true,
                                    message: 'Please select a organisation!'
                                }
                            ]
                        })(
                            <Select>
                                {_.map(this.state.organisation, item => (
                                    <Option key={`${item.id}${item.name}`} value={item.id}>
                                        {item.brand_name}
                                    </Option>
                                ))}
                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label={<span>Full Configs Access</span>}
                        hasFeedback
                    >
                        {getFieldDecorator('auto_auth', {
                            valuePropName: 'checked',
                            initialValue: _.get(this.props.data, ['auto_auth']) || false,
                            rules: [
                                {
                                    required: false
                                }
                            ]
                        })(<Switch />)}
                    </FormItem>
                </Form>

                <hr className="margin-bottom__small" />

                <p className="margin-bottom__small">
                    This will refresh the device, similar to re-opening the page
                </p>

                <Button
                    type="danger"
                    icon="refresh-1"
                    onClick={() => this.refreshDevice(this.props.data.id)}
                >
                    Refresh the Device
                </Button>
            </Modal>
        )
    }
}

export default Form.create()(EditAuth)
