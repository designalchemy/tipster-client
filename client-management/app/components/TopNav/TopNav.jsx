import React, { Component } from 'react'
import cx from 'classnames'
import { Link } from 'react-router-dom'
import { Menu, Icon, Col, Popconfirm } from 'antd'
import { inject, observer } from 'mobx-react'
import css from './TopNav.less'

const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup

@inject('routing', 'globalStore')
@observer
class TopNav extends Component {
    state = {
        current: 'logo'
    }

    logout = () => {
        this.props.globalStore.deleteToken()
        localStorage.removeItem('backoffice-jwt')
        this.props.routing.push('/management/login')
    }

    render() {
        const currentActive = this.props.routing.location.pathname
        const hasToken = this.props.globalStore.token
        const { role } = this.props.globalStore.user
        return (
            <div className="top-nav__container">
                <div className="container">
                    <Menu
                        defaultSelectedKeys={[currentActive]}
                        selectedKeys={[currentActive]}
                        mode="horizontal"
                    >
                        <SubMenu
                            title={
                                <span>
                                    <Icon type="notebook-2" />Configurations
                                </span>
                            }
                        >
                            <Menu.Item key="/screens">
                                <Link to="/management/screens">
                                    <Icon type="computer-1" />RPD
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="/configuration">
                                <Link to="/management/configurations">
                                    <Icon type="wrench-1" />RPT
                                </Link>
                            </Menu.Item>
                        </SubMenu>

                        <SubMenu
                            title={
                                <span>
                                    <Icon type="computer-1" />Management
                                </span>
                            }
                        >
                            <Menu.Item key="/auth">
                                <Link to="/management/auth">
                                    <Icon type="laptop-1" />Auth Hardware
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/ipwhitelist">
                                <Link to="/management/ipwhitelist">
                                    <Icon type="key-1" />IP Authentication
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="/users">
                                <Link to="/management/users">
                                    <Icon type="user-1" />Users
                                </Link>
                            </Menu.Item>

                            {role === 'admin' && (
                                <Menu.Item key="/organisations">
                                    <Link to="/management/customers">
                                        <Icon type="cart-2" />Customers
                                    </Link>
                                </Menu.Item>
                            )}

                            <Menu.Item key="/locations">
                                <Link to="/management/locations">
                                    <Icon type="location-pin-1" />Locations
                                </Link>
                            </Menu.Item>
                        </SubMenu>

                        <SubMenu
                            title={
                                <span>
                                    <Icon type="globe-1" />Translations
                                </span>
                            }
                        >
                            <Menu.Item key="/management/translations/tokens">
                                <Link to="/management/translations/tokens">
                                    <Icon type="lightbulb-2" />Tokens
                                </Link>
                            </Menu.Item>
                            <Menu.Item key="/management/translations/dialogue">
                                <Link to="/management/translations/dialogue">
                                    <Icon type="chat-3" />Dialogue
                                </Link>
                            </Menu.Item>
                        </SubMenu>

                        {1 === 0 && (
                            <Menu.Item key="/monitoring">
                                <Link to="/management/monitoring">
                                    <Icon type="activity-monitor-1" />Monitoring
                                </Link>
                            </Menu.Item>
                        )}

                        <Menu.Item key="/logout" style={{ float: 'right' }}>
                            <Popconfirm
                                title="Are you sure you want to Logout?"
                                onConfirm={() => this.logout()}
                                okText="Yes"
                                cancelText="No"
                            >
                                <Icon type="unlock-1" />Logout
                            </Popconfirm>
                        </Menu.Item>
                    </Menu>
                </div>
            </div>
        )
    }
}

export default TopNav
