import React, { Component } from 'react'
import { Row, Col, Button, message, Table, Icon, Popconfirm, Input } from 'antd'
import GetData from '../../tools/GetData'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import queryString from 'query-string'
import ModalSwitcher from '../Modals/ModalSwitcher'
import { sortAlpha } from '../../tools/utils'
import { observable, toJS } from 'mobx'

import css from './ScreenIndex.less'

@inject('globalStore')
@observer
class ScreenIndex extends Component {
    @observable tableData = []
    @observable
    pagination = {
        pageSize: 50,
        total: 0,
        current: 1
    }
    @observable filters = {}
    @observable
    sorting = {
        order: 'descend',
        field: 'soft_name'
    }

    @observable
    search = {
        query: '',
        type: ''
    }

    @observable
    nameFilter = {
        visible: false,
        search: ''
    }

    @observable loading = true
    @observable bookmakers = []
    @observable orgs = []

    @observable addNewScreen = false

    componentDidMount() {
        this.getData()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location.search !== this.props.location.search) {
            this.getData()
        }
    }

    getData = () => {
        const parsed = queryString.parse(this.props.location.search)

        GetData.dashboard('GET', {
            pagination: JSON.stringify(this.pagination),
            filters: JSON.stringify(this.filters),
            search: JSON.stringify(this.search),
            sorting: JSON.stringify(this.sorting)
        }).then(json => {
            let jsonCopy = json
            if (parsed.company)
                jsonCopy = _.filter(jsonCopy, item => item.dashboard_brand_id === parsed.company)
            this.handleResponse(jsonCopy, true)
        })
    }

    deleteScreen = id => {
        this.loading = true

        GetData.dashboard('DELETE', { id }).then(json => {
            this.handleResponse(json)
        })
    }

    onChange = (pagination, filters, sorter) => {
        this.loading = true
        this.sorting.order = sorter.order
        this.sorting.field = sorter.field
        this.pagination = pagination
        this.filters = filters
        this.getData()
    }

    onInputChange = (e, type) => {
        this[type].search = e.target.value
    }

    onSearch = (type, key) => {
        this.loading = true
        this[key].visible = false
        this.search.query = this[key].search
        this.search.type = type
        this.getData()
    }

    clearSearch = key => {
        this.loading = true
        this[key].visible = false
        this[key].search = ''
        this.search.query = ''
        this.search.type = ''
        this.getData()
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.bookmakers = json.bookmakers
                this.orgs = json.orgs
                this.pagination.total = json.total
                this.tableData = json.data
                this.loading = false
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    render() {
        const { role } = this.props.globalStore.user

        const columns = [
            {
                title: 'Soft Name',
                dataIndex: 'config.soft_name',
                sorter: true,
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.nameFilterInput = ele)}
                            placeholder="Search name"
                            value={this.nameFilter.search}
                            onChange={event => this.onInputChange(event, 'nameFilter')}
                            onPressEnter={() => this.onSearch('config.soft_name', 'nameFilter')}
                        />
                        <Button
                            type="primary"
                            onClick={() => this.onSearch('config.soft_name', 'nameFilter')}
                        >
                            Search
                        </Button>
                        <Button
                            type="primary"
                            ghost
                            style={{ marginLeft: '5px' }}
                            onClick={() => this.clearSearch('nameFilter')}
                        >
                            Reset
                        </Button>
                    </div>
                ),
                filterIcon: (
                    <Icon
                        type="zoom-1"
                        style={{
                            color: this.search.type === 'config.soft_name' ? '#0084DC' : '#AAAAAA'
                        }}
                    />
                ),
                filterDropdownVisible: this.nameFilter.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.nameFilter.visible = visible
                }
            },
            {
                title: 'ID',
                dataIndex: '_dashboard_id',
                sorter: true
            },
            {
                title: 'Organization',
                dataIndex: 'brand_name',
                sorter: true,
                filters:
                    _.size(this.orgs) > 0
                        ? _.map(this.orgs, item => ({
                              text: item,
                              value: item
                          }))
                        : false
            },
            {
                title: 'Book Maker',
                dataIndex: 'config.bookmaker_id',
                sorter: true,
                filters:
                    _.size(this.bookmakers) > 0
                        ? _.map(this.bookmakers, item => ({
                              text: item,
                              value: item
                          }))
                        : false
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <Link to={`/management/screen/${record._dashboard_id}`}>
                            Edit <Icon type="pencil-1" />
                        </Link>

                        {role === 'admin' && (
                            <span>
                                <span className="ant-divider" />

                                <Popconfirm
                                    title="Are you sure you want to delete this record?"
                                    onConfirm={() => this.deleteScreen(record._dashboard_id)}
                                    placement="left"
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <a href="#">
                                        Delete <Icon type="trash-1" />
                                    </a>
                                </Popconfirm>
                            </span>
                        )}
                    </span>
                )
            }
        ]

        return (
            <div className="">
                <ModalSwitcher
                    title="Add new screen"
                    modal="AddScreen"
                    showModal={this.addNewScreen}
                    closeModal={() => (this.addNewScreen = false)}
                    callback={() => this.getData()}
                />
                <Row type="flex" justify="space-between">
                    <h1>RPD Configs</h1>

                    <Button type="primary" onClick={() => (this.addNewScreen = true)}>
                        Add New Screens
                    </Button>
                </Row>

                <div className="table__body-container">
                    <Table
                        columns={columns}
                        dataSource={toJS(this.tableData)}
                        onChange={this.onChange}
                        loading={this.loading}
                        pagination={this.pagination}
                        rowKey={record => record._dashboard_id}
                        onRowDoubleClick={row =>
                            this.props.history.push(`/management/screen/${row._dashboard_id}`)}
                    />
                </div>
            </div>
        )
    }
}

export default ScreenIndex
