import React, { Component } from 'react'
import { Row, Col, Button, message, Table, Icon, Popconfirm, Input } from 'antd'
import ModalSwitcher from '../Modals/ModalSwitcher'
import GetData from '../../tools/GetData'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import queryString from 'query-string'
import { sortAlpha } from '../../tools/utils'
import { observable, toJS } from 'mobx'

import css from './ConfigurationIndex.less'

@inject('globalStore')
@observer
class ConfigurationIndex extends Component {
    @observable tableData = []
    @observable
    pagination = {
        pageSize: 50,
        total: 0,
        current: 1
    }
    @observable filters = {}
    @observable
    sorting = {
        order: 'descend',
        field: 'soft_name'
    }

    @observable
    search = {
        query: '',
        type: ''
    }

    @observable
    nameFilter = {
        visible: false,
        search: ''
    }

    @observable loading = true
    @observable orgs = []
    @observable addNewConfig = false

    componentDidMount() {
        this.getData()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location.search !== this.props.location.search) {
            this.getData()
        }
    }

    clearSearch = key => {
        this.loading = true
        this[key].visible = false
        this[key].search = ''
        this.search.query = ''
        this.search.type = ''
        this.getData()
    }

    getData = () => {
        const parsed = queryString.parse(this.props.location.search)

        GetData.config('GET', {
            pagination: JSON.stringify(this.pagination),
            filters: JSON.stringify(this.filters),
            search: JSON.stringify(this.search),
            sorting: JSON.stringify(this.sorting)
        }).then(json => {
            let jsonCopy = json
            if (parsed.company)
                jsonCopy = _.filter(jsonCopy, item => item.config_brand_id === parsed.company)
            this.handleResponse(jsonCopy, true)
        })
    }

    onChange = (pagination, filters, sorter) => {
        this.loading = true
        this.sorting.order = sorter.order
        this.sorting.field = sorter.field
        this.pagination = pagination
        this.filters = filters
        this.getData()
    }

    onInputChange = (e, type) => {
        this[type].search = e.target.value
    }

    onSearch = (type, key) => {
        this.loading = true
        this[key].visible = false
        this.search.query = this[key].search
        this.search.type = type
        this.getData()
    }

    deleteConfig = id => {
        this.loading = true

        GetData.config('DELETE', { id }).then(json => {
            this.handleResponse(json)
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
        } else {
            if (load) {
                this.tableData = json.data
                this.pagination.total = json.total
                this.orgs = json.orgs
                this.loading = false
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    render() {
        const { role } = this.props.globalStore.user

        const columns = [
            {
                title: 'Soft Name',
                dataIndex: 'soft_name',
                sorter: true,
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.nameFilterInput = ele)}
                            placeholder="Search name"
                            value={this.nameFilter.search}
                            onChange={event => this.onInputChange(event, 'nameFilter')}
                            onPressEnter={() => this.onSearch('soft_name', 'nameFilter')}
                        />
                        <Button
                            type="primary"
                            onClick={() => this.onSearch('soft_name', 'nameFilter')}
                        >
                            Search
                        </Button>
                        <Button
                            type="primary"
                            ghost
                            style={{ marginLeft: '5px' }}
                            onClick={() => this.clearSearch('nameFilter')}
                        >
                            Reset
                        </Button>
                    </div>
                ),
                filterIcon: (
                    <Icon
                        type="zoom-1"
                        style={{
                            color: this.search.type === 'soft_name' ? '#0084DC' : '#AAAAAA'
                        }}
                    />
                ),
                filterDropdownVisible: this.nameFilter.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.nameFilter.visible = visible
                }
            },
            {
                title: 'ID',
                dataIndex: 'id',
                sorter: true
            },
            {
                title: 'Organization',
                dataIndex: 'brand_name',
                sorter: true,
                filters:
                    _.size(this.orgs) > 0
                        ? _.map(this.orgs, item => ({
                              text: item,
                              value: item
                          }))
                        : false
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <Link to={`/management/configuration/${record.id}`}>
                            Edit <Icon type="pencil-1" />
                        </Link>

                        {role === 'admin' && (
                            <span>
                                <span className="ant-divider" />

                                <Popconfirm
                                    title="Are you sure you want to delete this record?"
                                    onConfirm={() => this.deleteConfig(record.id)}
                                    placement="left"
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <a href="#">
                                        Delete <Icon type="trash-1" />
                                    </a>
                                </Popconfirm>
                            </span>
                        )}
                    </span>
                )
            }
        ]

        return (
            <div className="">
                <ModalSwitcher
                    title="Add new config"
                    modal="AddConfig"
                    showModal={this.addNewConfig}
                    callback={() => this.getData()}
                    closeModal={() => {
                        this.addNewConfig = false
                    }}
                />
                <Row type="flex" justify="space-between">
                    <h1>RPT Configs</h1>

                    <Button
                        type="primary"
                        onClick={() => {
                            this.addNewConfig = true
                        }}
                    >
                        Add New Configuration
                    </Button>
                </Row>

                <div className="table__body-container">
                    <Table
                        columns={columns}
                        dataSource={toJS(this.tableData)}
                        onChange={this.onChange}
                        loading={this.loading}
                        pagination={this.pagination}
                        rowKey={record => record.id}
                        onRowDoubleClick={row =>
                            this.props.history.push(`/management/configuration/${row.id}`)}
                    />
                </div>
            </div>
        )
    }
}

export default ConfigurationIndex
