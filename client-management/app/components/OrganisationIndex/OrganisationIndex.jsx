import React, { Component } from 'react'
import { Table, Icon, Button, Row, Popconfirm, message, Input } from 'antd'
import { Link } from 'react-router-dom'
import ModalSwitcher from '../Modals/ModalSwitcher'
import GetData from '../../tools/GetData'
import { inject, observer } from 'mobx-react'
import { sortAlpha } from '../../tools/utils'
import { observable, toJS } from 'mobx'

import css from './OrganisationIndex.less'

@inject('globalStore')
@observer
class OrganisationIndex extends Component {
    @observable loading = true
    @observable addOrgModal = false
    @observable editOrgModal = false

    @observable tableData = []
    @observable
    pagination = {
        pageSize: 50,
        total: 0,
        current: 1
    }
    @observable filters = {}
    @observable
    sorting = {
        order: 'descend',
        field: 'soft_name'
    }

    @observable
    search = {
        query: '',
        type: ''
    }

    @observable
    nameFilter = {
        visible: false,
        search: ''
    }

    componentDidMount() {
        this.getData()
        if (window.location.hash === '#deleted') {
            message.success('Success - organisation deleted')
        }
    }

    onChange = (pagination, filters, sorter) => {
        this.loading = true
        this.sorting.order = sorter.order
        this.sorting.field = sorter.field
        this.pagination = pagination
        this.filters = filters
        this.getData()
    }

    getData = () => {
        this.loading = true
        this.addOrgModal = false
        this.editOrgModal = false
        GetData.orgs('GET', {
            pagination: JSON.stringify(this.pagination),
            filters: JSON.stringify(this.filters),
            search: JSON.stringify(this.search),
            sorting: JSON.stringify(this.sorting)
        }).then(json => {
            this.handleResponse(json, true)
        })
    }

    deleteOrg = id => {
        this.loading = true

        GetData.orgs('DELETE', { id }).then(json => {
            this.handleResponse(json)
        })
    }

    handleResponse = (json, load) => {
        if (json.error) {
            message.error(`${json.message}`, 5)
            this.loading = false
        } else {
            if (load) {
                this.tableData = json.data
                this.pagination.total = json.total
                this.loading = false
            }

            if (!load) {
                this.getData()
                message.success('Success - change has been made')
            }
        }
    }

    onInputChange = (e, type) => {
        this[type].search = e.target.value
    }

    onSearch = (type, key) => {
        this.loading = true
        this[key].visible = false
        this.search.query = this[key].search
        this.search.type = type
        this.getData()
    }

    launchModal = data => {
        this.editOrgData = data
        this.editOrgModal = true
    }

    closeEditModal = () => {
        this.editOrgData = {}
        this.editOrgModal = false
    }

    renderLink = (length, link, text) => {
        if (length) {
            return (
                <span>
                    <Link className="table-link" to={`/management/${link}`}>
                        {text}
                    </Link>
                </span>
            )
        }
        return <span>{text}</span>
    }

    render() {
        const { role } = this.props.globalStore.user

        const columns = [
            {
                title: 'Customer Name',
                dataIndex: 'brand_name',
                sorter: true,
                filterDropdown: (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => (this.nameFilterInput = ele)}
                            placeholder="Search name"
                            value={this.nameFilter.search}
                            onChange={event => this.onInputChange(event, 'nameFilter')}
                            onPressEnter={() => this.onSearch('brand_name', 'nameFilter')}
                        />
                        <Button
                            type="primary"
                            onClick={() => this.onSearch('brand_name', 'nameFilter')}
                        >
                            Search
                        </Button>
                    </div>
                ),
                filterIcon: (
                    <Icon
                        type="zoom-1"
                        style={{
                            color: this.search.type === 'brand_name' ? '#0084DC' : '#AAAAAA'
                        }}
                    />
                ),
                filterDropdownVisible: this.nameFilter.visible,
                onFilterDropdownVisibleChange: visible => {
                    this.nameFilter.visible = visible
                }
            },

            {
                title: 'Logo',
                dataIndex: 'brand_logo'
            },
            {
                title: 'RPD Config',
                dataIndex: 'rpd_config',
                sorter: true,
                render: (text, record) =>
                    this.renderLink(
                        text.length,
                        `screens?company=${record.id}`,
                        `${text.length} RPD configs`
                    )
            },
            {
                title: 'RPT Config',
                dataIndex: 'rpt_config',
                sorter: true,
                render: (text, record) =>
                    this.renderLink(
                        text.length,
                        `configurations?company=${record.id}`,
                        `${text.length} RPT configs`
                    )
            },
            {
                title: 'RPD Auth IDs',
                dataIndex: 'rpd_auth',
                sorter: true,
                render: (text, record) =>
                    this.renderLink(
                        text.length,
                        `auth?type=RPD&org=${record.id}`,
                        `${text.length} RPD Auth IDs`
                    )
            },
            {
                title: 'RPT Auth IDs',
                dataIndex: 'rpt_auth',
                sorter: true,
                render: (text, record) =>
                    this.renderLink(
                        text.length,
                        `auth?type=RPD&org=${record.id}`,
                        `${text.length} RPT Auth IDs`
                    )
            },
            {
                title: 'Users',
                dataIndex: 'users',
                sorter: true,
                render: (text, record) =>
                    this.renderLink(
                        text.length,
                        `users?company=${record.id}`,
                        `${text.length} Users`
                    )
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a
                            href="#"
                            onClick={e => {
                                e.preventDefault()
                                this.launchModal(record)
                            }}
                        >
                            Edit <Icon type="pencil-1" />
                        </a>

                        <span className="ant-divider" />

                        <Popconfirm
                            title="Are you sure you want to delete this record?"
                            onConfirm={() => this.deleteOrg(record.id)}
                            placement="left"
                            okText="Yes"
                            cancelText="No"
                        >
                            <a href="#">
                                Delete <Icon type="trash-1" />
                            </a>
                        </Popconfirm>
                    </span>
                )
            }
        ]

        return (
            <div>
                {role === 'admin' && (
                    <div>
                        <ModalSwitcher
                            title="Add new Customer"
                            modal="EditOrganisations"
                            showModal={this.addOrgModal}
                            closeModal={() => (this.addOrgModal = false)}
                            callback={() => this.getData()}
                        />

                        <ModalSwitcher
                            title="Edit customer"
                            modal="EditOrganisations"
                            data={this.editOrgData}
                            showModal={this.editOrgModal}
                            closeModal={() => this.closeEditModal()}
                            callback={() => this.getData()}
                        />

                        <Row type="flex" justify="space-between">
                            <h1>Customers</h1>

                            <Button type="primary" onClick={() => (this.addOrgModal = true)}>
                                Add New Customer
                            </Button>
                        </Row>

                        <div className="table__body-container">
                            <Table
                                columns={columns}
                                dataSource={toJS(this.tableData)}
                                onChange={this.onChange}
                                loading={this.loading}
                                pagination={this.pagination}
                                rowKey={record => record.id}
                                onRowDoubleClick={row =>
                                    this.props.history.push(`/management/customer/${row.id}`)}
                            />
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

export default OrganisationIndex
