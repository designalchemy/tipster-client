import React, { Component } from 'react'
import { LocaleProvider, Row, Col } from 'antd'
import enUS from 'antd/lib/locale-provider/en_US'
import jwtDecode from 'jwt-decode'

import _ from 'lodash'

import { inject, observer } from 'mobx-react'
import TopNav from './components/TopNav/TopNav'
import MainContainer from './components/MainContainer/MainContainer'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import koreLogicLogo from 'korelogiclogo'

import css from './App.less'

@inject('globalStore', 'routing')
@observer
class App extends Component {
    state = {}

    componentDidMount() {
        const token = localStorage.getItem('backoffice-jwt')
        koreLogicLogo()
        if (token) {
            this.props.globalStore.storeToken(token)
            this.props.globalStore.storeUser(jwtDecode(token))
        } else {
            this.props.routing.push('/management/login')
        }
    }

    render() {
        const { location, push, goBack } = this.props.routing

        return (
            <LocaleProvider locale={enUS}>
                <div>
                    <Header />
                    <TopNav />

                    <Row type="flex">
                        <Col span="24" className="container" style={{ padding: '20px' }}>
                            {this.props.children}
                        </Col>
                    </Row>

                    <Footer />
                </div>
            </LocaleProvider>
        )
    }
}

export default App
