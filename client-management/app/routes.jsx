import React from 'react'

import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import { Provider } from 'mobx-react'

import Login from './components/Login/Login'
import Dashboard from './components/Dashboard/Dashboard'
import ScreenIndex from './components/ScreenIndex/ScreenIndex'
import Screen from './components/Screen/Screen'
import Users from './components/Users/Users'
import TranslationsIndex from './components/TranslationsIndex/TranslationsIndex'
import OrganisationIndex from './components/OrganisationIndex/OrganisationIndex'
import Organisation from './components/Organisation/Organisation'
import Settings from './components/Settings/Settings'
import NotFound from './components/NotFound/NotFound'
import Auth from './components/Auth/Auth'
import Monitoring from './components/Monitoring/Monitoring'
import ConfigurationIndex from './components/ConfigurationIndex/ConfigurationIndex'
import Configuration from './components/Configuration/Configuration'
import LocationIndex from './components/LocationIndex/LocationIndex'
import Locations from './components/Locations/Locations'
import ResetPassword from './components/ResetPassword/ResetPassword'
import IPWhitelist from './components/IPWhitelist/IPWhitelist'

import globalStore from './stores/globalState'

import App from './App'

const browserHistory = createBrowserHistory()
const routingStore = new RouterStore()

const stores = {
    routing: routingStore,
    globalStore
}

const history = syncHistoryWithStore(browserHistory, routingStore)

const Routes = () => (
    <Provider {...stores}>
        <Router history={history}>
            <Switch>
                <Route exact path="/management/login" component={Login} />
                <Route path="/management/reset-password" component={ResetPassword} />

                <App>
                    <Switch>
                        <Route exact path="/" component={Dashboard} />
                        <Route exact path="/management" component={Dashboard} />
                        <Route path="/management/screens" component={ScreenIndex} />
                        <Route path="/management/screen/:id" component={Screen} />
                        <Route path="/management/configurations" component={ConfigurationIndex} />
                        <Route path="/management/configuration/:id" component={Configuration} />
                        <Route path="/management/users" component={Users} />
                        <Route
                            path="/management/translations/tokens"
                            component={TranslationsIndex}
                        />
                        <Route
                            path="/management/translations/dialogue"
                            component={TranslationsIndex}
                        />
                        <Route path="/management/auth" component={Auth} />
                        <Route path="/management/monitoring" component={Monitoring} />
                        <Route path="/management/customers" component={OrganisationIndex} />
                        <Route path="/management/customer/:id" component={Organisation} />
                        <Route path="/management/settings" component={Settings} />
                        <Route path="/management/locations" component={LocationIndex} />
                        <Route path="/management/location/:id" component={Locations} />
                        <Route path="/management/ipwhitelist/" component={IPWhitelist} />

                        <Route component={NotFound} />
                    </Switch>
                </App>
            </Switch>
        </Router>
    </Provider>
)

export default Routes
