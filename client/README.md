

Run the below 3 things for the different services

How this works:

App.jsx
	this connects to db based on DASHBOARD_ID (from url) grabs all the data
	it then calculates the length of the animations based on no of scenes and their custom times
	after the time limit for the animations is up, it switches the props for the children components


SpeechBubbles.jsx
	this receives the acts to play tho, after one is finished (via custom timer it is sent) its child bubble.jsx calls a call back that increases the state to show the next scene

Bubble.jsx
	this is sent the bubble chat (single or double), preforms a call back to display next bubble when all are done, its parent displays next scene



URL structure:

/tipster/dashboard_id/?meeting=#####&race=######1333&minutes=###&number=###

meeting = comma seperated list of locations eg 'meeting=LONGWORTH,DONCASTER'

race = comma seperated list of locations + times (no colons) eg race='LONGWORTH1355,DONCASTER1220'

minuets = number of minuets to display eg eg 'minuets=120'

number = number of races to display eg 'numeber=10'

=====================

To main app:

yarn install
yarn start

open http://localhost:3000


=====================


run database:

rethinkdb

(needs to be installed globally)
brew update && brew install rethinkdb

=====================


run server for dB:

https://bitbucket.org/korelogic-developers/terminal-backend


clone the repo 'tipster-server'

yarn install

yarn start (this starts the server, seperate tab)

yarn seed:dev (this deletes and creates DB)

yarn fetch (fetches the races, needs VPN access, node 7.6+ required)

node app/diffusion.js (fetches the prices, can be cancelled for dev purposes after its ran)