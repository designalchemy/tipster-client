const webpack = require('webpack')
const DashboardPlugin = require('webpack-dashboard/plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')
const config = require('config')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const BUILD_DIR = path.resolve(__dirname, 'public/')

const webpackConfig = {
    production: {
        devtool: 'eval',
        entry: ['./src/index'],
        output: {
            path: BUILD_DIR,
            filename: '[hash].js',
            chunkFilename: '[chunkhash].js',
            publicPath: '/'
        },
        resolve: {
            extensions: ['.js', '.jsx'],
            alias: {
                components: path.resolve(__dirname, './src/components/'),
                assets: path.resolve(__dirname, './src/assets'),
                styles: path.resolve(__dirname, './src/styles'),
                tools: path.resolve(__dirname, './src/tools')
            }
        },
        module: {
            loaders: [
                {
                    test: /\.jsx?$/,
                    loaders: ['babel-loader'],
                    include: path.join(__dirname, 'src')
                },
                {
                    test: /\.scss$/,
                    use: [
                        { loader: 'style-loader' },
                        { loader: 'css-loader' },
                        {
                            loader: 'postcss-loader',
                            options: {
                                config: {
                                    path: 'postcss.config.js'
                                }
                            }
                        },
                        { loader: 'sass-loader' }
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    loaders: ['file?hash=sha512&digest=hex&name=[hash].[ext]', 'image-webpack']
                },
                {
                    test: /\.json$/,
                    loaders: ['json-loader']
                }
            ]
        },
        plugins: [
            new CopyWebpackPlugin([{ from: './src/assets', to: 'assets' }]),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production')
                }
            }),
            new webpack.optimize.ModuleConcatenationPlugin(),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: './index.hbs'
            }),
            new webpack.IgnorePlugin(/vertx/),
            new webpack.optimize.UglifyJsPlugin()
        ],
        target: 'node'
    },

    development: {
        entry: ['./client/src/index'],
        output: {
            path: BUILD_DIR,
            filename: '[hash].js',
            chunkFilename: '[chunkhash].js',
            publicPath: '/'
        },
        resolve: {
            extensions: ['.js', '.jsx'],
            alias: {
                components: path.resolve(__dirname, './src/components/'),
                assets: './client/src/assets',
                styles: './client/src/styles',
                tools: path.resolve(__dirname, 'src/tools')
            }
        },

        module: {
            loaders: [
                {
                    test: /\.jsx?$/,
                    loaders: ['babel-loader'],
                    include: path.join(__dirname, 'src')
                },
                {
                    test: /\.scss$/,
                    use: [
                        { loader: 'style-loader' },
                        { loader: 'css-loader' },
                        {
                            loader: 'postcss-loader',
                            options: {
                                config: {
                                    path: 'postcss.config.js'
                                }
                            }
                        },
                        { loader: 'sass-loader' }
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    loaders: ['file?hash=sha512&digest=hex&name=[hash].[ext]', 'image-webpack']
                },
                {
                    test: /\.json$/,
                    loaders: ['json-loader']
                }
            ]
        },
        plugins: [
            new CopyWebpackPlugin([{ from: './client/src/assets', to: 'assets' }]),
            new webpack.SourceMapDevToolPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('development')
                }
            }),
            new webpack.optimize.ModuleConcatenationPlugin(),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: 'client/index.hbs'
            }),
            new webpack.IgnorePlugin(/vertx/)
        ],
        target: 'node'
    }
}

module.exports = webpackConfig[config.get('webpack')]
