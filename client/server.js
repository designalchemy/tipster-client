const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server')
const config = require('./webpack.config')

new WebpackDevServer(webpack(config), {
    publicPath: config.output.publicPath,
    hot: true,
    historyApiFallback: true,
    proxy: {
        '/horizon': {
            target: 'ws://127.0.0.1:8181',
            changeOrigin: true,
            ws: true
        },
        '/horizon/*': {
            target: 'http://127.0.0.1:8181',
            changeOrigin: true
        },
        '/api/*': {
            target: 'http://127.0.0.1:8181',
            changeOrigin: true
        }
    }
}).listen(3000, 'localhost', (err, result) => {
    if (err) {
        console.log(err)
    }
    console.log('Listening at localhost:3000')
})
