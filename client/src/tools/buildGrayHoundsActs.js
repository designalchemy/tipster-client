import { takeNFromObject } from 'tools/utils'
import moment from 'moment'
import { returnDelay } from 'tools/utils'

import questions from './ghQuestions.json' // to do : put in DB
import racesStore from '../stores/RacesStore'
import configStore from '../stores/ConfigStore'

const getExtraDetails = dataSource => {
    if (dataSource === 'welcome') {
        return configStore.config.welome_prompt_text
    }
    if (dataSource === 'race_video_intro') {
        return 'Here is what the {{logo}} predictor thinks will happen:' // put into DB
    }
    return null
}

const buildConvoArray = (dataSource, component, double) => {
    // put this in util file

    const questionSet = double ? _.sample(questions[`${dataSource}`]) : ''
    const question = double ? questionSet[0] : ''
    const text = double ? questionSet[1] : ''
    const extraText = getExtraDetails(dataSource)

    const doubleArray = [
        {
            type: 'text',
            direction: 'from',
            timingDelay: returnDelay(component),
            content: {
                text: question
            }
        },
        {
            type: component || 'gh-main-prompt',
            direction: 'to',
            timingDelay: 0,
            content: {
                dataSource,
                text
            }
        }
    ]

    const singleArray = {
        type: component || 'gh-main-prompt',
        direction: 'to',
        timingDelay: returnDelay(component),
        content: {
            dataSource,
            text: extraText || questionSet
        }
    }

    return double ? doubleArray : singleArray
}

const buildActArray = (type, size, component, double, isScrollable, autoScroll) => {
    // put this in util file
    const newActArray = {
        scene_index: size,
        scene_components: [
            {
                component: 'mascot'
            },
            {
                component: 'speech-bubbles',
                component_data: {
                    twoWayChat: double,
                    convo: _.flatMap(type, item => buildConvoArray(item, component, double))
                }
            },
            {
                component: 'gh-race-card',
                component_data: {
                    isScrollable,
                    autoScroll
                }
            }
        ]
    }

    return newActArray
}

const buildGrayHoundsActs = inputActs => {
    // this builds a act script for the bubbles to follow, takes what avaible in /Act/ and whats data is on the API and makes it into a script
    const raceObj = racesStore.currentRaceObj
    console.log(
        'Build new acts for:',
        _.get(raceObj, 'race.diffusion_competition_name'),
        moment(_.get(raceObj, 'race.race_datetime')).format('HH:mm')
    )

    const acts = [] // make a new acts array for the script to run thru

    acts.push(buildActArray(['welcome'], 0, 'text', false, false, false, false)) // creates inial welcome promp

    acts[0].scene_components[1].component_data.convo.push(
        buildConvoArray('book-maker', 'book-maker')
    ) // adds book maker prompt

    acts[0].scene_components[1].component_data.convo.push(
        buildConvoArray('race_video_intro', 'text')
    ) // adds video intro

    acts[0].scene_components[1].component_data.convo.push(
        buildConvoArray('race_video', 'race_video')
    ) // add video

    const statsData = raceObj.statistics

    const reduceData = (runs, wins, percetage) =>
        _.reduce(
            _.get(statsData, 'trap_track_records'),
            (result, item) => ({
                runs: item.runs >= runs ? result.runs + 1 : result.runs,
                wins: item.wins >= wins ? result.wins + 1 : result.wins,
                percentaged:
                    item.percentaged >= percetage ? result.percentaged + 1 : result.percentaged
            }),
            { runs: 0, wins: 0, percentaged: 0 }
        )
    const trapTrackRecord = reduceData(3, 1, 21) // runs, wins, % - these are from req's maybe DB this ?
    const dogPerformance = reduceData(5, 3, 21) // runs, wins, % - these are from req's maybe DB this ?

    // build first lot of stats
    const filterdKeys = _.reduce(
        _.keys(raceObj.bet_prompts),
        (acc, key) => (_.isNil(raceObj.bet_prompts[key]) ? acc : acc.concat(key)),
        []
    )

    // match questions and bet prompts
    const withoutExcessKeys = _.pick(questions, filterdKeys)

    const runners = _.map(racesStore.currentRaceObj.runners, item => item.trap_number) // all traps that are running

    const nonRunnerKey = _.reduce(
        // this is used to add bet prompt score and can be used to filter out non runners in future
        withoutExcessKeys,
        (acc, item, key) => {
            let betPrompts = _.get(racesStore.currentRaceObj.bet_prompts, [`${key}`])

            if (key === 'hot_form' || key === 'top_of_the_class' || key === 'plenty_of_trap') {
                betPrompts = _.maxBy(betPrompts, 'bet_prompt_score')
            }

            if (key === 'leading_the_pack') {
                betPrompts = _.maxBy(betPrompts, 'dog_projected_split')
            }

            acc.push({ [key]: { ...item }, bet_prompt_score: betPrompts.bet_prompt_score })

            return acc
        },
        []
    )

    const sortFilteredPrompts = _.orderBy(nonRunnerKey, 'bet_prompt_score', 'desc') // sort bet prompts by bet score

    const filterByMaxPrompts = takeNFromObject(sortFilteredPrompts, configStore.config.max_promps) // fitler bet prompts by max config val

    const keyList = _.reduce(
        // make list of keys after sorting
        filterByMaxPrompts,
        (acc, item) => {
            _.forEach(item, (subItem, key) => _.isObject(subItem) && acc.push(key)) // reduce the keys to a list, ignoring the bet prompt value keys
            return acc
        },
        []
    )

    // build bet prompts
    if (_.size(keyList)) {
        acts.push(buildActArray(keyList, _.size(acts), '', true))
    }

    // build stats prompt
    if (
        trapTrackRecord.runs >= 4 &&
        trapTrackRecord.wins >= 3 &&
        trapTrackRecord.percentaged >= 1
    ) {
        acts.push(buildActArray(['trap_track_records'], _.size(acts), '', true))
    } else if (
        dogPerformance.runs >= 4 &&
        dogPerformance.wins >= 3 &&
        dogPerformance.percentaged >= 1
    ) {
        acts.push(buildActArray(['dog_performance'], _.size(acts), '', true))
    } else if (raceObj.statistics && raceObj.statistics.recent_trapStats) {
        // build trap stats
        acts.push(buildActArray(['recent_trapStats'], _.size(acts), '', true))
    }

    // post picks
    if (raceObj.post_picks) {
        acts.push(buildActArray(['post-pick'], _.size(acts), 'post-pick', true))
    }

    return acts
}

export default buildGrayHoundsActs
