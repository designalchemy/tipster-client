import React, { Component } from 'react'
import { observer } from 'mobx-react'
import moment from 'moment'
import { observable, reaction } from 'mobx'
import { formattedTime } from 'tools/utils'
import configStore from '../../stores/ConfigStore'

import css from './styles.scss'

class Clock extends Component {
    componentDidMount() {
        reaction(
            () => configStore.config.clock_is_24_hour,
            () => {
                this.updateClock()
            }
        )
        this.runTimer()
    }

    componentWillUnmount() {
        clearTimeout(this.timer)
    }

    @observable time

    runTimer() {
        this.updateClock()

        this.timer = setTimeout(() => {
            this.runTimer()
        }, 1000 * 30) // update every 30 seconds, to keep clock accurate
    }

    updateClock() {
        this.time = formattedTime(configStore.config.clock_is_24_hour)
    }

    render() {
        return <div className="clock-container">{this.time}</div>
    }
}

export default observer(Clock)
