import moment from 'moment'
import _ from 'lodash'
import { getParameterByName } from 'tools/utils'
import racesStore from '../stores/RacesStore'
import configStore from '../stores/ConfigStore'

const filterRaces = races => {
    // remove races allready happened today
    // filter races by URL params
    // join ODDS to races

    const passRaces = races
    let racesToShow = false

    const filteredRaces = _.isNil(getParameterByName('race')) // url race id's in format DONCASTER1450
        ? false
        : getParameterByName('race').split(',')

    const filteredMeetings = _.isNil(getParameterByName('meeting')) // url meeting location eg DONCASTER
        ? false
        : getParameterByName('meeting').split(',')

    const filteredMinutes = _.isNil(getParameterByName('minutes')) // minuets number eg 100
        ? 1440 // a days minuets
        : Number(getParameterByName('minutes'))

    const filteredNumber = _.isNil(getParameterByName('number')) // number of races eg 10
        ? races.length
        : Number(getParameterByName('number'))

    const sortAndFilterRaces = (passRaces, shortCircuit) =>
        _.chain(passRaces)
            .sortBy(race => _.get(race, 'race_card.race_datetime'))
            .filter(race => _.get(race, 'match_url')) // filter if there isnt a match url
            .filter(race => _.get(race, 'runners')) // filter if there are no runners for horses
            .filter(race => {
                if (configStore.config.sport === '1') {
                    if (!_.get(racesStore.nonRunner, [race.match_url])) {
                        return true // stop if falling over if there are no non runner data
                    }
                    return _.reduce(
                        _.get(racesStore.nonRunner, [race.match_url]),
                        (acc, item) => {
                            if (item.running === 'G') {
                                acc = true
                            }
                            return acc
                        },
                        false
                    )
                }

                return true
            })
            .filter(
                race =>
                    !_.includes(
                        // this filters out races that have wrong status codes
                        ['A', 'X'],
                        _.get(race, 'race_card.race_status_code')
                    )
            )
            .filter(race => {
                // this filters races if they are before the current time OR after set number via the url
                const raceTime = _.get(race, 'race_card.race_datetime')
                const isAfterCurrentTime = moment().isAfter(
                    moment(raceTime).add(90, 'seconds') // add extra seconds to let rate finish
                )

                const maxTime = moment().add(filteredMinutes, 'minute')
                const isAfterMaxTime = moment(maxTime).isAfter(raceTime)

                if (!isAfterCurrentTime && isAfterMaxTime) {
                    racesToShow = true
                }

                if (!racesToShow) {
                    return !isAfterCurrentTime
                }

                return !isAfterCurrentTime && isAfterMaxTime
            })
            .filter((race, i) => {
                if (shortCircuit) return true
                // filter race by meeting && races ID

                const raceString = _.get(race, 'match_url').split('/')
                const raceId = raceString[1] + raceString[2].replace(':', '')
                const trimId = raceId.replace(/ /g, '')

                if (!filteredRaces && !filteredMeetings) {
                    return race
                }

                return (
                    _.includes(filteredMeetings, raceString[1]) || _.includes(filteredRaces, trimId)
                )
            })
            .take(filteredNumber) // limits number of races
            .value()

    let activeRaceArr = sortAndFilterRaces(races)

    // if no races match the URL fiter, do it again but ignore filter

    if (activeRaceArr.length === 0) {
        activeRaceArr = sortAndFilterRaces(races, true)
    }

    return activeRaceArr
}

export default filterRaces
