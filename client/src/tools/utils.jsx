import _ from 'lodash'
import racesStore from '../stores/RacesStore'
import configStore from '../stores/ConfigStore'
import moment from 'moment'
import fallbackLang from './fallbackLang.json'

_.templateSettings.interpolate = /{{([\s\S]+?)}}/g

// does a deep search on a object

const deepSearch = (object, searchKey) => {
    let result = null

    if (object instanceof Array) {
        for (let i = 0; i < object.length; i++) {
            result = deepSearch(object[i], searchKey)
            if (result) break
        }
    } else {
        for (const prop in object) {
            if (prop === searchKey) return object[prop]

            if (object[prop] instanceof Object || object[prop] instanceof Array) {
                result = deepSearch(object[prop], searchKey)
                if (result) break
            }
        }
    }
    return result
}

const getUrlId = () => {
    // get dashboard ID out of URL
    const url = new URL(window.location.href).pathname.split('/')
    const id = _.last(_.take(url, 3))
    return id
}

const fracToDecCheck = fraction => {
    if (configStore.config.price_format === 'decimal' && fraction !== '-') {
        const fracSplit = fraction.split('/')
        const result = parseInt(fracSplit[0], 10) / parseInt(fracSplit[1], 10)
        //Limit to 2 decimal places if there's a recurring number.
        return Number(`${Math.round(`${result}e${2}`)}e-${2}`)
    }
    return fraction
}

// remove decinal places from a vlaue
const removeDecimalPlaces = value => (value % 1 === 0 ? Math.round(value) : value)

// turns yards to miles
const calcYardsToMiles = yards => `${_.floor(yards / 1760)}m`

// turns x yards to furlongs and adds half symbol
const calcReminderToFurlongs = yards => {
    const number = Math.round((yards % 1760) / 220 * 2) / 2
    const isNotZero = number < 1 ? '' : _.floor(number)
    if (number === 0) return ''
    return number !== 0.5 && number % 1 === 0 ? `${number}f` : `${isNotZero}½f`
}

// get first object item
const getFirst = obj => obj[_.keys(obj)[0]]

// parses {{ }} tamplate string
const parseTemplate = (string, data) => {
    const compiled = _.template(string)
    return compiled(data)
}

// loads a invisble image to cache it
const preLoadImage = url => {
    const image = new Image()
    image.src = url
}

// gets params from URL ?x=1&y=2
const getParameterByName = (name, url) => {
    let nameCopy = name
    let urlCopy = url
    if (!urlCopy) urlCopy = window.location.href
    nameCopy = nameCopy.replace(/[\[\]]/g, '\\$&')
    const regex = new RegExp(`[?&]${nameCopy}(=([^&#]*)|&|#|$)`)
    const results = regex.exec(urlCopy)
    if (!results) return null
    if (!results[2]) return ''
    return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

const isNonRunner = horseName => {
    // if horse is non runner
    const nonRunnerCodes = ['N', 'W', 'V'] // non runner codes from diffusion, G, J are good

    const runningOdds = _.get(racesStore.nonRunner, [
        racesStore.currentRaceObj.match_url,
        _.toUpper(horseName),
        'running'
    ])

    return _.includes(nonRunnerCodes, runningOdds)
}

const getOdds = (uid, name) => {
    const type = configStore.config.price_source
    let odd = ''

    if (type === 'off') {
        // if odds turned off
        odd = '-'
    } else if (type === 'guide_only') {
        // if only the guide prices
        if (configStore.config.sport === '1') {
            odd = _.get(racesStore.currentRaceObj.runners, [uid, 'forecast_odds_desc']) || '-'
        } else {
            odd =
                _.get(_.find(racesStore.currentRaceObj.runners, ['dog_uid', uid]), 'odds_desc') ||
                '-'
        }
    } else if (type === 'diffusion_only') {
        // if diffusion only
        odd =
            _.get(racesStore.odds, [
                racesStore.currentRaceObj.match_url,
                _.toUpper(name),
                'odds'
            ]) || '-'
    } else {
        // if guide prices and diffusion + diffusion only
        odd = _.get(racesStore.odds, [racesStore.currentRaceObj.match_url, _.toUpper(name), 'odds'])

        if (_.isNil(odd) || odd === 'SP' || !odd) {
            // if diffusion is empty or STARTING PRICE, use guide prices
            odd =
                _.get(racesStore.currentRaceObj.runners, [uid, 'forecast_odds_desc']) ||
                _.get(_.find(racesStore.currentRaceObj.runners, ['dog_uid', uid]), 'odds_desc') ||
                '-'
        }
    }

    return fracToDecCheck(odd)
}

// get odds from odds store

const calcTiming = acts => {
    // this counts the ammout in the que to work how long it will take, pass to Header as prop

    let count = 0 // counter for the total page timer

    const data = acts

    data.forEach(items => {
        // loop each phase
        items.scene_components.forEach(item => {
            // loop each event in the pahse
            if (item.component === 'speech-bubbles') {
                // if its a chat bubble
                if (item.component_data.twoWayChat) {
                    // if its a two way chat
                    item.component_data.convo.forEach(task => {
                        // each item in the two way chat timing
                        if (task.timingDelay) {
                            count += task.timingDelay
                        }
                    })
                } else {
                    // if its a one way chat
                    item.component_data.convo.forEach(task => {
                        // each time in the one way chat
                        count += task.timingDelay
                    })
                }
            }
        })
    })

    return count + 5000 // this is for the final exit animation
}

const findGreyHoundColor = color => {
    const greyhoundsColours = {
        white_black: [19, 28, 38, 16],
        fawn: [13, 15, 22, 25, 26, 21, 27],
        brindle: [1, 30],
        blue: [3, 4, 24, 6],
        black: [5, 8, 10, 9, 12],
        blue_white: [7, 18, 34, 11, 36],
        fawn_white: [14, 20, 32, 33, 35, 37, 2, 17, 23, 29, 31]
    }
    return (
        _.reduce(
            greyhoundsColours,
            (string, item, key) => {
                if (_.includes(item, color)) {
                    string = key
                }
                return string
            },
            ''
        ) || 'black'
    )
}

const takeNFromObject = (obj, n) =>
    _.chain(obj)
        .keys()
        .sort()
        .take(n)
        .reduce((acc, current) => {
            acc[current] = obj[current]
            return acc
        }, {})
        .value()

const isDayTime = time =>
    moment(time).isBetween(moment('06:30 am', 'HH:mm a'), moment('09:00 pm', 'HH:mm a'))

const formattedTime = (is24Hour, time) =>
    is24Hour ? moment(time).format('HH:mm') : moment(time).format('h:mm')

const returnDelay = component => {
    switch (component) {
        case 'text':
            return Number(configStore.config.text_timing)
        case 'book-maker':
            return Number(configStore.config.bet_timing)
        case 'race_video':
            return Number(configStore.config.predictor_timing)
        default:
            return Number(configStore.config.global_timing)
    }
}

const getDogForm = (dogObj, joinChar) =>
    _.map(dogObj, (item, acc) => (item.trial_flag !== 'NT' ? 'T' : item.race_outcome_uid))
        .reverse()
        .join(joinChar || '')

const getTranslation = key =>
    _.get(_.find(configStore.translations, ['token_name', key]), 'text') || _.get(fallbackLang, key)

const isDevTools = () => window.location.href.indexOf('devTools') !== -1

export {
    removeDecimalPlaces,
    calcYardsToMiles,
    calcReminderToFurlongs,
    deepSearch,
    getFirst,
    findGreyHoundColor,
    parseTemplate,
    preLoadImage,
    getParameterByName,
    isNonRunner,
    getUrlId,
    takeNFromObject,
    calcTiming,
    getOdds,
    isDayTime,
    formattedTime,
    returnDelay,
    getDogForm,
    getTranslation,
    isDevTools
}
