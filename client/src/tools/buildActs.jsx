import { isNonRunner, takeNFromObject } from 'tools/utils'
import moment from 'moment'
import { returnDelay } from 'tools/utils'

import questions from './question.json' // to do : put in DB
import singleQuestions from './singleQuestion.json' // to do : put in DB
import configStore from '../stores/ConfigStore'
import racesStore from '../stores/RacesStore'

const getExtraDetails = dataSource => {
    if (dataSource === 'welcome') {
        return configStore.config.welome_prompt_text
    }
}

const buildConvoArray = (dataSource, component, double) => {
    // put this in util file

    const questionSet = double
        ? _.sample(questions[`${dataSource}`])
        : _.sample(singleQuestions[`${dataSource}`])

    const question = double ? questionSet[0] : ''
    const text = double ? questionSet[1] : ''
    const timer = returnDelay(component)
    const extraText = getExtraDetails(dataSource)

    const doubleArray = [
        {
            type: 'text',
            direction: 'from',
            timingDelay: returnDelay(component),
            content: {
                text: question
            }
        },
        {
            type: component || 'main-prompt',
            direction: 'to',
            timingDelay: 0,
            content: {
                dataSource,
                text
            }
        }
    ]

    const singleArray = {
        type: component || 'main-prompt',
        direction: 'to',
        timingDelay: returnDelay(component),
        content: {
            dataSource,
            text: questionSet || extraText
        }
    }

    return double ? doubleArray : singleArray
}

const buildActArray = (type, size, component, double, isScrollable, autoScroll) => {
    // put this in util file
    const newActArray = {
        scene_index: size,
        scene_components: [
            {
                component: 'mascot'
            },
            {
                component: 'speech-bubbles',
                component_data: {
                    twoWayChat: double,
                    convo: _.flatMap(type, item => buildConvoArray(item, component, double))
                }
            },
            {
                component: 'race-card',
                component_data: {
                    isScrollable,
                    autoScroll
                }
            }
        ]
    }

    return newActArray
}

const buildActs = () => {
    // this builds a act script for the bubbles to follow, takes what avaible in /Act/ and whats data is on the API and makes it into a script

    console.log(
        'Build new acts for:',
        _.get(racesStore.currentRaceObj.verdict, 'course_style_name'),
        moment(_.get(racesStore.currentRaceObj.verdict, 'race_datetime')).format('HH:mm')
    )

    const acts = [] // make a new acts array for the script to run thru

    acts.push(buildActArray(['welcome'], 0, 'text', false, false, false, true)) // creates inial welcome promp

    acts[0].scene_components[1].component_data.convo.push(
        buildConvoArray('book-maker', 'book-maker')
    ) // adds book maker prompt

    // first manually add key stat prompt
    if (
        !isNonRunner(
            racesStore.currentRaceObj.verdict.horse_name,
            _.get(racesStore.nonRunner, [racesStore.currentRaceObj.match_url])
        ) &&
        racesStore.currentRaceObj.verdict.rp_verdict &&
        racesStore.currentRaceObj.verdict.key_stats_str &&
        racesStore.currentRaceObj.verdict.horse_name &&
        racesStore.currentRaceObj.verdict.horse_uid !== -1
    ) {
        acts[0].scene_components[1].component_data.convo.push(buildConvoArray('key_stat'))
    }

    // filter keys of main bet prompts
    const filterdKeys = _.reduce(
        _.keys(racesStore.currentRaceObj.bet_prompts),
        (acc, key) => (_.isNil(racesStore.currentRaceObj.bet_prompts[key]) ? acc : acc.concat(key)),
        []
    )

    // match questions and bet prompts
    const withoutExcessKeys = _.pick(questions, filterdKeys)

    // do a non runner check and reduce them out
    const nonRunnerKey = _.reduce(
        withoutExcessKeys,
        (acc, item, key) => {
            let betPrompts = _.get(racesStore.currentRaceObj.bet_prompts, [`${key}`])

            if (key === 'course_jockeys' || key === 'course_trainers') {
                betPrompts = _.head(_.values(betPrompts))
                _.merge(
                    betPrompts,
                    _.maxBy(_.values(betPrompts.jockeys || betPrompts.trainers, 'bet_prompt_score'))
                )
                _.merge(betPrompts, _.head(betPrompts.entries))
            }

            if (
                key === 'hot_jockeys' ||
                key === 'hot_trainers' ||
                key === 'trainers_jockeys' ||
                key === 'horses_for_courses' ||
                key === 'most_napped'
            ) {
                betPrompts = _.maxBy(_.values(betPrompts), 'bet_prompt_score')
                _.merge(betPrompts, _.head(betPrompts.entries))
                betPrompts.horse_name = betPrompts.horse || betPrompts.horse_name
            }

            if (key === 'seven_day_winners') {
                // once their API is stable this can be added to above return
                betPrompts = _.maxBy(_.values(betPrompts), 'bet_prompt_score') || _.head(betPrompts)
            }

            if (key === 'travellers_check') {
                // once their API is stable this can be added to above return
                betPrompts =
                    _.maxBy(_.values(betPrompts), 'bet_prompt_score') ||
                    _.maxBy(betPrompts, 'bet_prompt_score')
            }

            if (!isNonRunner(betPrompts.horse_name)) {
                acc.push({ [key]: { ...item }, bet_prompt_score: betPrompts.bet_prompt_score })
            }
            return acc
        },
        []
    )

    const sortFilteredPrompts = _.orderBy(nonRunnerKey, 'bet_prompt_score', 'desc')

    const filterByMaxPrompts = takeNFromObject(sortFilteredPrompts, configStore.config.max_promps)

    const keyList = _.reduce(
        // make list of keys after sorting
        filterByMaxPrompts,
        (acc, item) => {
            _.forEach(item, (subItem, key) => _.isObject(subItem) && acc.push(key)) // reduce the keys to a list, ignoring the bet prompt value keys
            return acc
        },
        []
    )

    // build bet prompts
    const mappedQuestions = _.flatMap(keyList, item => buildConvoArray(item, null, true))

    if (!racesStore.currentRaceObj.predictor.empty && _.size(filterByMaxPrompts)) {
        // if there is a predictor and bet prompts
        acts.push(buildActArray(['race_video'], _.size(acts), 'race_video', true, true)) // add video
        acts[1].scene_components[1].component_data.convo.push(...mappedQuestions) // add prompts after video
    } else if (_.size(filterByMaxPrompts)) {
        // if there is no predictor but bet prompts
        acts.push(buildActArray(Object.keys(filterByMaxPrompts), _.size(acts), null, true, true)) // add prompts
    } else if (!racesStore.currentRaceObj.predictor.empty) {
        acts.push(buildActArray(['race_video'], _.size(acts), 'race_video', true, true)) // add video
    }

    // match single questions and bet prompts
    const withoutExcessKeysSingle = _.pick(
        singleQuestions,
        _.concat(filterdKeys, ['verdict_info']) // force verdict as not a bet prompt
    )

    // non runner check on single bet prompts
    const nonRunnerKeySingle = _.reduce(
        withoutExcessKeysSingle,
        (acc, item, key) => {
            let betPrompts = _.get(racesStore.currentRaceObj.bet_prompts, [`${key}`])

            // return _.merge(acc, { [key]: { ...item } }) // hack to force all bet prompts thru, dont uncomment unless testing

            if (key === 'verdict_info') {
                betPrompts = racesStore.currentRaceObj.spotlight_verdict_selection
            }

            if (key === 'verdict_info' && !racesStore.currentRaceObj.spotlight_verdict_selection) {
                return acc
            }

            if (key === 'most_tipped') {
                betPrompts = _.maxBy(_.values(betPrompts, 'bet_prompt_score'))
            }

            if (!isNonRunner(_.get(betPrompts, 'horse_name'))) {
                return _.merge(acc, { [key]: { ...item } })
            }
            return acc
        },
        {}
    )

    // build single bet prompts

    if (_.size(nonRunnerKeySingle)) {
        acts.push(buildActArray(Object.keys(nonRunnerKeySingle), _.size(acts), null, false))
    }

    return acts
}

export default buildActs
