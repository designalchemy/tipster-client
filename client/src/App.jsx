import React, { Component } from 'react'
import cx from 'classnames'
import { DataLayer, Auth, ErrorLog } from 'components'
import moment from 'moment'
import Horizon from '@horizon/client'
import _ from 'lodash'
import koreLogicLogo from 'korelogiclogo'
import Cookies from 'js-cookie'
import { observer } from 'mobx-react'
import { observable, reaction } from 'mobx'
import { getUrlId, isDevTools } from 'tools/utils'
import 'babel-polyfill'
import es6Promise from 'es6-promise' // promise polyfill
import 'whatwg-fetch' // fetch polyfill
import url from 'tools/url-poly' // URL polyfill
import configStore from './stores/ConfigStore'
import styles from './App.scss'

const polyfill = es6Promise.polyfill()

class App extends Component {
    constructor(props) {
        super(props)
        this.refreshEarlyMorning()
    }

    componentWillMount() {
        koreLogicLogo()
    }

    refreshEarlyMorning() {
        // hack to reset page at 5 am
        this.refreshTimer = setTimeout(() => {
            if (moment().format('hh:mm') === '05:00') window.location.reload()
            this.refreshEarlyMorning()
        }, 1000 * 35)
    }

    render() {
        return (
            <div>
                {isDevTools() && <ErrorLog />}
                {configStore.isAuthorised && <DataLayer />}
                {!configStore.isAuthorised && (
                    <Auth
                        configId={this.props.match.params.id.split(/[.&_?!@€#£¢$%∞§¶^%*•(ª)º]/)[0]}
                    />
                )}
            </div>
        )
    }
}

export default observer(App)
