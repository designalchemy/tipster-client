import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'

import Routes from './routes'

render(<Routes />, document.getElementById('root'))
