import { observable, action, computed } from 'mobx'

class Races {
    @observable races = {}
    @action
    storeRaces = obj => {
        this.races = obj
    }

    @observable odds = {}
    @action
    storeOdds = obj => {
        this.odds = obj
    }

    @observable nonRunner = {}
    @action
    storeNonRunner = obj => {
        this.nonRunner = obj
    }

    @observable races = {}
    @action
    storeRaces = obj => {
        this.races = obj
    }

    @observable currentRaceIndex = 0
    @action
    storeCurrentRaceIndex = obj => {
        this.currentRaceIndex = obj
    }

    @observable currentRaceObj = {}
    @action
    storeCurrentRaceObj = obj => {
        this.currentRaceObj = obj
    }

    @observable currentRaceUrl = ''
    @action
    storeCurrentRaceUrl = item => {
        this.currentRaceUrl = item
    }

    @observable activeItem = []
    @action
    storeActiveItem = item => {
        this.activeItem = item
    }

    @observable acts = []
    @action
    storeActs = item => {
        this.acts = item
    }

    @observable currentPromptType = ''
    @action
    storeCurrentPromptType = item => {
        this.currentPromptType = item
    }

    @computed
    get racesIndex() {
        return _.map(this.races, race => race.match_url)
    }

    @computed
    get getNextRaceUrl() {
        const raceNo = (this.currentRaceIndex + 1) % this.races.length
        return this.races[raceNo].match_url
    }
    @computed
    get calcIndexPos() {
        const no = _.indexOf(this.racesIndex, this.currentRaceUrl)
        return no === -1 ? 0 : no
    }
}
const racesStore = new Races()

export default racesStore
