import { observable, action } from 'mobx'

class Config {
    @observable config = { main_color: '' }
    @action
    storeConfig = config => {
        this.config = config
    }

    @observable isWhitelist = false
    @action
    storeWhitelist = bool => {
        this.isWhitelist = bool
    }

    @observable userId = ''
    @action
    storeId = id => {
        this.userId = id
    }

    @observable raceTimeLength = 0
    @action
    storeRaceTimeLength = time => {
        this.raceTimeLength = time
    }

    @observable finalActs = []
    @action
    storeFinalActs = acts => {
        this.finalActs = acts
    }

    @action showPrices = () => this.config.bookmaker_id !== ''

    @observable singleRaceHack = 0
    @action
    storeSingleRaceHack = item => {
        this.raceTimeLength = item
    }

    @observable interval = 0
    @action
    storeInterval = time => {
        this.interval = time
    }

    @observable finished = false
    @action
    storeFinished = val => {
        this.finished = val
    }

    @observable translations = {}
    @action
    storeTranslations = arr => {
        this.translations = arr
    }

    @observable isAuthorised = false
    @action
    storeIsAuthorised = val => {
        this.isAuthorised = val
    }
}

const configStore = new Config()

export default configStore
