import React from 'react'
// import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { Provider } from 'mobx-react'

import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'

import App from './App'
import BlankPage from './components/BlankPage/BlankPage'

import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import configStore from './stores/ConfigStore'
import racesStore from './stores/RacesStore'

const browserHistory = createBrowserHistory()
const routingStore = new RouterStore()

const stores = {
    routing: routingStore,
    configStore,
    racesStore
}

const history = syncHistoryWithStore(browserHistory, routingStore)

const Routes = () =>
    <Provider {...stores}>
        <Router history={history}>
            <Switch>
                <Route path="/tipster/:id" component={App} />

                <Route path="/tipster" component={BlankPage} />

                <Route component={BlankPage} />
            </Switch>
        </Router>
    </Provider>

export default Routes
