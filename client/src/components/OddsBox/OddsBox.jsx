import React, { Component } from 'react'
import { observer } from 'mobx-react'
import configStore from '../../stores/ConfigStore'

import styles from './OddsBox.scss'

const OddsBox = props => {
    return (
        <div
            className={`odds-box ${props.className}`}
            style={{ backgroundColor: configStore.config.odds_color || '#e12a20' }}
        >
            {props.text}
        </div>
    )
}

export default observer(OddsBox)
