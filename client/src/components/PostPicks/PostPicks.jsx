import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import cx from 'classnames'

import racesStore from '../../stores/RacesStore'
import configStore from '../../stores/ConfigStore'

import css from './PostPicks.scss'

class PostPicks extends Component {
    componentDidMount() {
        setTimeout(() => {
            this.finished = true
        }, 1200)
        racesStore.storeActiveItem(
            _.map(racesStore.currentRaceObj.post_picks, item => item.post_pick)
        )
    }

    @observable finished = false

    render() {
        const postPickData = racesStore.currentRaceObj.post_picks
        const nth = { 1: '1st', 2: '2nd', 3: '3rd' }
        const runners = racesStore.currentRaceObj.predictor
        const numberOfRacers = _.size(runners) > 6 ? _.size(runners) : 6
        const randomNumberList = _.shuffle(_.times(numberOfRacers, index => index + 1)) // make random number list 1-6 with non recoccuring values
        const backgroundSize = _.size(runners) > 6 ? '1523' : '1141' // number_spite and number_sprite_big height

        return (
            <div className="post-picks__container">
                <h1 className="post-pick__logo">
                    <img src="/assets/logo/post_pick_logo.png" alt="logo" />
                </h1>

                <div className="post-picks__cells">
                    {_.map(postPickData, (item, i) => (
                        <div key={item.dog_name}>
                            <h2>{nth[i + 1]}</h2>
                            <div className="post-picks__slider">
                                <div
                                    className={cx('post-picks__slider-inner', {
                                        'post-picks__big-bg': _.size(runners) > 6
                                    })}
                                    style={{
                                        backgroundPositionY: `${this.finished
                                            ? -((item.post_pick - 1) * 190)
                                            : backgroundSize + randomNumberList[i] * 190}px`
                                    }}
                                />
                            </div>
                            <h2
                                style={{
                                    opacity: `${this.finished ? 1 : 0}`,
                                    fontSize: '0.8em'
                                }}
                            >
                                {item.dog_name}
                            </h2>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default observer(PostPicks)
