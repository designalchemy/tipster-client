import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { isDayTime, getTranslation } from '../../tools/utils'
import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'

const backgroundUrl = () => {
    const sport = configStore.config.sport === '1' ? 'horse-' : 'dogs-'
    const surface = configStore.config.sport === '1' ? 'turf-' : ''
    const time = isDayTime() ? '' : '-night'
    return `${sport}${surface}background${time}`
}

const NoRaces = props => {
    const text =
        _.size(racesStore.races) === 0 && props.racesSize === 0
            ? getTranslation('finished__loading')
            : getTranslation('finished__no_more_racing')

    return (
        <div
            className="home-container"
            style={{
                backgroundImage: `url(/assets/backgrounds/${backgroundUrl()}.jpg)`
            }}
        >
            <div className="no-races__container">
                <img src={'/assets/logo/racing_post_dark.png'} alt="logo" />
                <h1>{text}</h1>
            </div>
        </div>
    )
}

export default observer(NoRaces)
