import React, { Component } from 'react'
import Horizon from '@horizon/client'
import { preLoadImage, getUrlId, calcTiming, isDevTools } from 'tools/utils'
import { Header, Footer, Container, Mascot, DevTools, CompRender, NoRaces } from 'components'
import filterRaces from 'tools/filterRaces'
import buildActs from 'tools/buildActs'
import buildGrayHoundsActs from 'tools/buildGrayHoundsActs'

import _ from 'lodash'
import moment from 'moment'
import { observer } from 'mobx-react'
import { observable, toJS } from 'mobx'
import Cookies from 'js-cookie'

import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'
import css from './DataLayer.scss'

class DataLayer extends Component {
    constructor(props) {
        super(props)
        this.observables = {}
        this.id = {}

        this.dbDisconnect = this.dbDisconnect.bind(this)
    }

    componentWillMount() {
        this.runHz()
    }

    runHz() {
        const userId = configStore.userId
        const urlId = getUrlId()
        console.log('%cAttempting websocket connection', 'background: orange; color: white;')
        this.hz = new Horizon({
            authType: {
                token: [userId, urlId].join('//'), // this only works with rohans horzion, passes user AUTH ID and config ID
                storeLocally: false
            }
        })
        this.hz.onReady(() => this.dbConnect())
        this.hz.onDisconnected(() => this.dbDisconnect())
        this.hz.connect()
    }

    componentWillUnmount() {
        clearTimeout(this.mainTimer)
        clearInterval(this.connectTimer)
        this.time = moment()
        // clear the timers if this dismounts
    }

    @observable acts = {}
    @observable hzConnection = false
    @observable firstRun = true
    @observable singleRaceHack = true
    @observable refresh = ''
    @observable time = 0
    @observable racesLength = 0
    @observable storeRaces = []
    storeUnfilteredRaces = []

    dbConnect() {
        console.log('%cWebsocket connected', 'background: green; color: white;')
        this.hzConnection = true
        clearInterval(this.connectTimer)

        const urlDashboardId = getUrlId()

        if (!urlDashboardId) {
            console.log('%cNo ID supplied, exiting', 'background: red; color: white;')
            return false
        }

        // this is the DB connection + horizon socket
        // collect data then push data to state

        this.hz('Dashboard')
            .find({ _dashboard_id: urlDashboardId })
            .watch()
            .map((data, i) => {
                if (!data) {
                    return false
                }

                configStore.storeConfig(data.config)

                const { dashboard_brand_id, dashboard_race_ids, config } = data
                const joinedArray = []

                dashboard_race_ids.forEach(item => {
                    joinedArray.push({ _race_id: `${item}` })
                })

                // grab all race ID's and join them into arr

                // if (this.firstRun) {
                //     console.log('%cFirst connection raw config dump:', 'color: limegreen')
                //     console.log(data)
                // }

                const getSport = config.sport === '1' ? 'Race' : 'GH_Race'
                const oddsTable = config.sport === '1' ? 'Odds' : 'GH_Odds'
                // this chooese which sport table to look into, this will need a better check

                return this.hz
                    .aggregate({
                        brand: this.hz('Brand').find({
                            id: dashboard_brand_id
                        }),
                        acts: this.hz('Act').find({
                            _act_id: config.sport
                        }),
                        races: this.hz(getSport).above({ _race_id: '1' }), // weird way to get all races for specific sport
                        odds: this.hz(oddsTable).find({
                            bookmaker_id: config.bookmaker_id
                        }),
                        non_runner: this.hz(oddsTable).find({
                            bookmaker_id: '#INFO'
                        }),
                        auth: this.hz('Auth').find({
                            id: configStore.isWhitelist
                                ? configStore.userId
                                : Cookies.get('tipster-auth')
                        }),
                        translations: this.hz('Translations').findAll({ language: config.lang })
                    })
                    .watch()
            })
            .subscribe(data => {
                if (!data) return false
                data.subscribe(finalData => {
                    const oddsData = _.isNil(finalData.odds) ? '' : finalData.odds.odds_data
                    const nonRunnerData = _.isNil(finalData.non_runner)
                        ? ''
                        : finalData.non_runner.odds_data

                    const hasRefeshTimerChanged =
                        !this.firstRun && finalData.auth && this.refresh !== finalData.auth.refresh

                    if (hasRefeshTimerChanged) {
                        // reload the page if the refresh timestamp has changed
                        window.location.reload()
                    }

                    configStore.storeTranslations(finalData.translations)

                    console.log('%cNew data from websocket', 'background: limegreen; color: white;')
                    this.storeData(finalData.races, oddsData, nonRunnerData)
                    // save results to state

                    // console.log(finalData)

                    if (this.firstRun) {
                        this.preLoadImages(finalData.races)
                        this.refresh = finalData.auth.refresh
                        this.firstRun = false
                        this.storeUnfilteredRaces = finalData.races
                        racesStore.storeActs(finalData.acts)
                        racesStore.storeRaces(filterRaces(finalData.races))
                        racesStore.storeCurrentRaceIndex(racesStore.racesIndex[0])
                        // console.log('%cFirst connection raw data dump:', 'color: limegreen')
                        // console.log(finalData)
                        // build race script sorts the ACTS
                        this.buildRaceScript()

                        document.title =
                            configStore.config.sport === '1'
                                ? `${document.title} - Horse Racing`
                                : `${document.title} - Grayhounds`
                    }
                })
            })
    }

    storeData(races, oddsData, nonRunnerData) {
        // check if we need to start race script again

        racesStore.storeOdds(oddsData)
        racesStore.storeNonRunner(nonRunnerData)

        if (!_.isEqual(races, this.storeUnfilteredRaces)) {
            this.storeUnfilteredRaces = races // if the race object has changed, store it again
            racesStore.storeRaces(filterRaces(races))
        }

        if (
            (this.racesLength === races.length && !this.firstRun) || // if not first run
            (this.racesLength !== races.length && this.firstRun) // if it is first run, break
        ) {
            this.racesLength = races.length // if the races length aint change, quit out
            return false
        }

        this.racesLength = races.length

        clearTimeout(this.filterRacesTimeout)

        this.filterRacesTimeout = setTimeout(() => {
            // this is when the races length have changed, go back to race 0 and re-store everything
            const finalRaces = filterRaces(races)

            racesStore.storeRaces(finalRaces) // store new races
            if (finalRaces.length > 0) {
                racesStore.storeCurrentRaceIndex(racesStore.racesIndex[0])
                racesStore.storeCurrentRaceUrl(racesStore.races[0].match_url)
            }
            this.buildRaceScript() // rebuild races script
            this.storeData(races, oddsData, nonRunnerData)
        }, 20 * 1000)
    }

    dbDisconnect() {
        // on DB disconnect, try again with timer
        this.hzConnection = false
        console.log(
            '%cWebsocket disconnected, will try to reconnect',
            'background: red; color: white;'
        )

        clearInterval(this.connectTimer)

        this.connectTimer = setInterval(() => {
            console.log('%cTry reconnect', 'background: orange; color: white;')
            this.runHz()
        }, 10000)
    }

    preLoadImages(races) {
        // preload all silks, then images from a config file
        const silkImageArr = _.flatMap(races, race =>
            _.map(
                race.runners,
                runner => `//images.racingpost.com/svg/${runner.silk_image_path}.svg`
            )
        )

        // use images API to get all images and load
        fetch('/api/images', {
            method: 'GET'
        })
            .then(res => res.json())
            .then(json => {
                _.map(json, url => preLoadImage(url))
            })
    }

    buildRaceScript() {
        if (racesStore.races.length !== 0) {
            const raceIndexPos = racesStore.calcIndexPos

            racesStore.storeCurrentRaceIndex(raceIndexPos) // calcs position of races
            racesStore.storeCurrentRaceObj(racesStore.races[raceIndexPos]) // stores  race object
            racesStore.storeActiveItem([]) // resets the race card active items

            const actData = configStore.config.sport === '1' ? buildActs() : buildGrayHoundsActs()

            configStore.storeRaceTimeLength(calcTiming(actData)) // store time length of acts
            configStore.storeFinalActs(actData) // store final acts

            this.time = moment() // reset time to new time
            this.chartTimer() // start timer
        }
    }

    chartTimer() {
        const timerAmmount = configStore.raceTimeLength / 1000 // no of items * ammount of seconds per slide, fixed for js timer
        const timer = timerAmmount - moment().diff(this.time) / 1000
        clearInterval(this.firstTimer)

        configStore.storeInterval(timer)

        if (timer <= 0 && racesStore.races.length !== 0 && configStore.finished) {
            if (racesStore.races.length === 1) {
                // if only once race, do a loop back to trick it into re-rendering
                this.runSingleRaceHack()
            }

            racesStore.storeCurrentRaceUrl(racesStore.getNextRaceUrl) // if the timer has finished, store next race
            this.buildRaceScript() // then build the new race baesed on race index
        }

        this.firstTimer = setInterval(() => {
            this.chartTimer() // call back on self
        }, 125)
    }

    runSingleRaceHack() {
        this.singleRaceHack = false // hack for single race, turn it off then on again, to trick it into re rendering
        this.singleRaceHack = true
    }

    devNextRace() {
        racesStore.storeCurrentRaceUrl(racesStore.getNextRaceUrl)
        this.buildRaceScript()
    }

    render() {
        // should i display races ?
        const display =
            _.size(racesStore.races) > 0 &&
            _.keys(racesStore.currentRaceObj).length &&
            this.singleRaceHack // keys prevents loading too early

        return (
            <div className="master-container">
                {!this.hzConnection && ( // if db connection issues show warning
                    <div className="hz-warning-message">
                        Network experiencing issues, prices / info may not be accurate.
                    </div>
                )}

                {isDevTools() && <DevTools devNextRace={() => this.devNextRace()} />}

                <Header singleRaceHack={this.singleRaceHack} />

                {display && <CompRender />}

                {!display && <NoRaces racesSize={_.size(this.storeUnfilteredRaces)} />}

                <Footer />
            </div>
        )
    }
}

export default observer(DataLayer)
