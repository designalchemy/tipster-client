import React, { Component } from 'react'
import { observer } from 'mobx-react'
import configStore from '../../stores/ConfigStore'

import css from './Mascot.scss'

const Mascot = () => (
    <img src={`/assets/characters/${configStore.config.dashboard_mascot}`} alt="" />
)

export default observer(Mascot)
