import React, { Component } from 'react'
import Cookies from 'js-cookie'
import { observer } from 'mobx-react'
import configStore from '../../stores/ConfigStore'
import ReactImageFallback from 'react-image-fallback'

import css from './Footer.scss'

const Footer = () => {
    const { config } = configStore
    const color = config.main_color.length === 0 ? '#000000' : config.main_color
    const logoUrl = config.logo ? config.logo : 'racing_post'
    const authId = Cookies.get('tipster-auth')

    return (
        <footer className="staticFooter-container" style={{ backgroundColor: color }}>
            <div className="staticFooter-container_logo">
                <ReactImageFallback
                    src={`/assets/logo/${logoUrl}`}
                    fallbackImage="/assets/logo/racing_post.png"
                    initialImage=""
                    alt="bookmaker image"
                    className="prompt-image"
                />
            </div>

            <p
                dangerouslySetInnerHTML={{
                    __html: config.bottom_bar_text
                }}
            />

            <div className="staticFooter-container_logo">
                <ReactImageFallback
                    src={`/assets/logo/${logoUrl}`}
                    fallbackImage="/assets/logo/racing_post.png"
                    initialImage=""
                    alt="bookmaker image"
                    className="prompt-image"
                />
            </div>

            <div className="staticFooter-id_text">{authId}</div>
        </footer>
    )
}

export default observer(Footer)
