import React, { Component } from 'react'
import moment from 'moment'
import {
    Container,
    Marquee,
    MainInfo,
    Mascot,
    SpeechBubbles,
    Bubble,
    RaceCard,
    Verdict,
    GreyHoundRaceCard
} from 'components'
import { observer } from 'mobx-react'
import { observable, reaction, toJS } from 'mobx'
import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'
import cx from 'classnames'
import { isNonRunner, isDayTime } from '../../tools/utils'

import css from './CompRender.scss'

class CompRender extends Component {
    constructor(props) {
        super(props)

        this.increasePhaseVal = this.increasePhaseVal.bind(this)
    }

    componentWillMount() {
        this.makeAnimation()
        // reset config + start new animation que

        this.reactToChange = reaction(
            () => [configStore.finalActs],
            () => {
                this.makeAnimation()
            }
        )
    }

    componentWillUnmount() {
        this.reactToChange = null
    }

    @observable currentPhase = 0
    @observable showAnimation = false

    loadComponents(item, data) {
        // this is a simple component loader from the act script
        switch (item) {
            case 'mascot':
                return (
                    <div
                        className={cx('mascot-container', {
                            'slide-in-up': this.showAnimation
                        })}
                        key="mascot"
                    >
                        <Mascot />
                    </div>
                )

            case 'speech-bubbles': {
                const areThereRaces = !_.isNil(racesStore.currentRaceObj)
                    ? _.keys(racesStore.currentRaceObj.runners).length
                    : 1
                return (
                    <div
                        className={cx('speech-container', {
                            'fade-in-now-delay': this.showAnimation,
                            'smaller-margin': areThereRaces >= 12,
                            'larger-margin': areThereRaces <= 12
                        })}
                        key="bubbles"
                    >
                        <SpeechBubbles data={data} increasePhaseVal={this.increasePhaseVal} />
                    </div>
                )
            }
            case 'race-card': {
                return (
                    <div
                        className={cx('racecard', {
                            'slide-in-up': this.showAnimation
                        })}
                        key="racecard"
                    >
                        <RaceCard data={data} currentPhase={this.currentPhase} />
                    </div>
                )
            }
            case 'gh-race-card': {
                return (
                    <div
                        className={cx('racecard', {
                            'slide-in-up': this.showAnimation
                        })}
                        key="gh-racecard"
                    >
                        <GreyHoundRaceCard />
                    </div>
                )
            }
            default:
                return <p>Error</p>
        }

        // CHANGE CHILDREN TO SEND CORRECT DATA FROM JSON
    }

    increasePhaseVal() {
        // this is the state change to itarate scenes / phases
        if (this.currentPhase === configStore.finalActs.length - 1) {
            configStore.storeFinished(true)
        }

        if (this.currentPhase < configStore.finalActs.length - 1) {
            // minus 2 as ACT seed data has too many, fix later
            this.currentPhase = this.currentPhase + 1
        }
    }

    makeAnimation() {
        // nasty animnation fix, starts a animation then stops it via CSS class
        this.currentPhase = 0
        configStore.storeFinished(false)
        this.showAnimation = true

        window.setTimeout(() => {
            this.showAnimation = false
        }, 1500)
    }

    backgroundUrl() {
        const currentRaceData = racesStore.currentRaceObj
        const sport = configStore.config.sport === '1' ? 'horse-' : 'dogs-' // chooese sport

        const bgTimeSuffix = isDayTime(currentRaceData.race_card.race_datetime) ? '' : '-night' // if is night time

        const url = _.get(currentRaceData, 'predictor.empty')
            ? 'turf'
            : _.get(currentRaceData, 'predictor.race.surface') // chooese surface

        return `${sport}${url ? `${url}-` : ''}background${bgTimeSuffix}`
    }

    render() {
        const queSystem = configStore.finalActs
        const currentRaceData = racesStore.currentRaceObj

        const showVerdict =
            (currentRaceData.spotlight_verdict_selection &&
                !isNonRunner(
                    currentRaceData.spotlight_verdict_selection.horse_name,
                    _.get(racesStore.nonRunner, [racesStore.currentRaceObj.match_url])
                )) ||
            currentRaceData.post_picks

        return (
            <div
                className="home-container"
                style={{
                    transition: 'background-image 0.2s ease-in-out',
                    backgroundImage: `url(/assets/backgrounds/${this.backgroundUrl()}.jpg)`
                }}
            >
                <div
                    className={cx({
                        'fade-in-now': this.showAnimation
                    })}
                >
                    <MainInfo />
                </div>
                <Container>
                    {queSystem.length &&
                        // if there are acts to display
                        _.map(queSystem, items => {
                            const item = items.scene_components
                            if (this.currentPhase === items.scene_index) {
                                // if the item in the act, is the currently selected act, value comes from increasePhaseVal
                                // only return if its the correct race to display
                                return item.map(comp =>
                                    this.loadComponents(comp.component, comp.component_data)
                                )
                            }
                        })}
                </Container>

                {showVerdict && <Verdict showAnimation={this.showAnimation} />}
            </div>
        )
    }
}

export default observer(CompRender)
