import React, { Component } from 'react'
import { observable, reaction } from 'mobx'
import { observer } from 'mobx-react'
import { isDevTools } from 'tools/utils'

const errors = observable([])

if (isDevTools()) {
    window.console = {
        log(err) {
            errors.push(JSON.stringify(err, Object.getOwnPropertyNames(err)))
        },
        error(err) {
            errors.push(JSON.stringify(err, Object.getOwnPropertyNames(err)))
        },
        exception(err) {
            errors.push(JSON.stringify(err, Object.getOwnPropertyNames(err)))
        }
    }

    window.onerror = function(message, url, linenumber) {
        console.log('JavaScript error: ' + message + ' on line ' + linenumber + ' for ' + url)
        return false
    }

    window.addEventListener('error', function(evt) {
        console.log(
            "Caught[via 'error' event]:  '" +
                evt.message +
                "' from " +
                evt.filename +
                ':' +
                evt.lineno
        )
        console.log(evt) // has srcElement / target / etc
        evt.preventDefault()
    })
}

class ErrorLog extends Component {
    constructor(props) {
        super(props)
        this.errors = errors
    }

    render() {
        return (
            <div className="consoleErrors">
                Console: <br />
                {_.map(this.errors.reverse(), (items, i) => <p key={i}>{items}</p>)}
            </div>
        )
    }
}

export default observer(ErrorLog)
