import React, { Component } from 'react'
import moment from 'moment'
import { observer } from 'mobx-react'
import { toJS } from 'mobx'
import cx from 'classnames'
import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'
import css from './RaceVideo.scss'
import { findGreyHoundColor, isDayTime } from '../../tools/utils'

class RaceVideo extends Component {
    componentDidMount() {
        const runners =
            configStore.config.sport === '1'
                ? racesStore.currentRaceObj.predictor.runners
                : racesStore.currentRaceObj.predictor

        setTimeout(() => {
            racesStore.storeActiveItem([
                _.get(_.maxBy(_.values(runners), 'score'), 'diffusion_name'),
                _.get(_.minBy(runners, 'dog_projected_finish'), 'trap_num')
            ])
        }, 2000)
    }

    isHorseRace() {
        return configStore.config.sport === '1'
    }

    getSportPrefix() {
        return this.isHorseRace()
            ? `horse_${_.get(racesStore.currentRaceObj, 'predictor.race.surface')}`
            : 'dogs'
    }

    getNightPrefix() {
        return isDayTime(racesStore.currentRaceObj.race_card.race_datetime) ? '' : '_night' // night time or day time track
    }

    getColor(id) {
        return _.get(racesStore.currentRaceObj, `runners.${id}.horse_colour_code`)
    }

    mapOfRunners(runners) {
        const sortedRunners = _.orderBy(runners, ['dog_projected_finish'], ['asc'])

        let distance = 0

        const proccessedRunners = _.map(sortedRunners, item => {
            item.finish_point = distance
            distance += 30
            return item
        })

        const reOrderRunners = _.orderBy(proccessedRunners, ['trap_num'], ['asc'])

        return reOrderRunners
    }

    getHorseColor(horse_color_code) {
        switch (horse_color_code) {
            case 'CH':
                return 'chestnut'
            case 'GR':
            case 'WH':
            case 'P':
            case 'SK':
                return 'white'
            default:
                return 'brown'
        }
    }

    getMargin(index) {
        return this.isHorseRace() ? `${125 / 6 * index + -44}px` : `${155 / 6 * index + -85}px`
        // weird numbers to align runners correct on the diagnal line
    }

    render() {
        const currentRace = racesStore.currentRaceObj
        const runners = this.isHorseRace()
            ? currentRace.predictor.runners
            : this.mapOfRunners(currentRace.predictor)
        const numberOfRacers = _.size(runners) > 6 ? _.size(runners) : 6

        let hasBeenAWinner = false

        return (
            <div
                className="race-container"
                style={{
                    backgroundImage: `url(/assets/backgrounds/predictor_${this.getSportPrefix()}${this.getNightPrefix()}.jpg)`
                }}
            >
                {this.isHorseRace() && (
                    <div
                        className="race-video-logo"
                        style={{
                            background: 'url(/assets/logo/racing_post.png)',
                            backgroundSize: 'contain'
                        }}
                    />
                )}

                <div className="race-video-flash" />

                <div
                    className={cx('horse-container', {
                        'is-grayhounds': !this.isHorseRace()
                    })}
                >
                    {_.times(numberOfRacers, index => {
                        const indexAjust = index + 1

                        const getItems = this.isHorseRace()
                            ? _.values(runners)[index]
                            : _.find(runners, {
                                  trap_num: indexAjust
                              })

                        const {
                            dog_name = '',
                            dog_colour_uid,
                            dog_projected_finish,
                            trap_num,
                            diffusion_name,
                            score,
                            finish_point,
                            id,
                            color,
                            saddle_cloth_number
                        } =
                            getItems || {}

                        const horseColor = this.getHorseColor(this.getColor(id))

                        let winner = ''
                        let scoreCopy = score

                        if (
                            _.get(currentRace.nonRunner, [
                                _.toUpper(_.get(runners, [getItems, 'diffusion_name']) || dog_name),
                                'running'
                            ]) === 'N'
                        ) {
                            return false
                        } // if non runner dont show

                        if (scoreCopy === 100 && !hasBeenAWinner) {
                            hasBeenAWinner = true
                            winner = 'winner'
                        } else if (scoreCopy < 70) {
                            scoreCopy = 70 // so even the slowest ones come on the screen
                        }

                        if (finish_point === 0) {
                            winner = 'winner'
                        }

                        const distance = this.isHorseRace() ? (100 - scoreCopy) * 6 : finish_point

                        const url = this.isHorseRace()
                            ? `url(/assets/horses/horse_${color ||
                                  'red'}-silk_${horseColor}-horse.png)`
                            : `url(/assets/dogs/dogs_silks/${findGreyHoundColor(
                                  dog_colour_uid
                              )}/${trap_num}.png)`

                        return (
                            <div // container for each item
                                key={`${indexAjust}`}
                                className="item-container"
                                style={{
                                    animation: this.isHorseRace()
                                        ? 'move 3.5s linear'
                                        : 'move_gh 2.45s linear',
                                    animationFillMode: 'forwards',
                                    marginLeft: this.getMargin(index)
                                }}
                            >
                                <div // acutaly horse / dog image
                                    className={`${winner} ${this.isHorseRace()
                                        ? 'horse-icon'
                                        : 'gh-icon'}`}
                                    style={{
                                        left: `-${distance}px`,
                                        backgroundImage: url
                                    }}
                                />
                                <div // the label behind
                                    className={`item-label ${winner}`}
                                    style={{
                                        animation: this.isHorseRace()
                                            ? 'fadeIn 0.2s 3.6s ease-in'
                                            : 'fadeIn 0.2s 3s ease-in',
                                        animationFillMode: 'forwards',
                                        right: `${distance + 90}px`,
                                        display: `${dog_name || diffusion_name
                                            ? 'inline-block'
                                            : 'none'}`
                                    }}
                                >
                                    {`${saddle_cloth_number || ''} ${diffusion_name || dog_name}`}
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default observer(RaceVideo)
