import React, { Component, PropsValidation } from 'react'
import { OddsBox } from 'components'
import { observer } from 'mobx-react'
import cx from 'classnames'
import { getOdds, getTranslation } from 'tools/utils'
import ReactImageFallback from 'react-image-fallback'

import styles from './Verdict.scss'
import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'

const Verdict = props => {
    let dataSource
    const { post_picks } = racesStore.currentRaceObj

    switch (configStore.config.sport) {
        case '1':
            dataSource = racesStore.currentRaceObj.spotlight_verdict_selection
            break
        case '2':
            dataSource = racesStore.currentRaceObj.post_picks
            break
        default:
            dataSource = {}
            break
    }

    const { horse_name, horse_uid, silk_image_path, start_number } = dataSource
    const odds = getOdds(horse_uid, horse_name)
    const isGrayHound = _.get(dataSource, '[0].dog_name')

    return (
        <div
            className={cx('verdict', {
                verdict__finished: configStore.finished,
                'slide-in-verdict': props.showAnimation
            })}
        >
            {!isGrayHound && (
                <div className={'verdict__slanted'}>
                    {horse_name && (
                        <span>
                            <span className="verdict__postpick-horse">Racing Post</span>
                            {getTranslation('verdict__title')}
                        </span>
                    )}
                </div>
            )}

            {horse_name && (
                <div className="verdict__main">
                    <div className="verdict__rp-logo">
                        <img src="/assets/logo/racing_post_dark.png" alt="logo" />
                    </div>
                    <div className="verdict__participant">
                        <div className="verdict__start-number">{start_number}</div>
                        <div className="verdict__participant-icon">
                            <ReactImageFallback
                                src={`//images.racingpost.com/svg/${silk_image_path}.svg`}
                                fallbackImage="/assets/horses/blank_silk.png"
                                alt="silk"
                            />
                        </div>
                        <div
                            className="verdict__participant-name"
                            dangerouslySetInnerHTML={{ __html: horse_name }}
                        />
                    </div>
                </div>
            )}

            {isGrayHound && (
                <div className="verdict__main verdict__main-gh">
                    <img
                        className="verdict__rp-logo-gh"
                        src={`${configStore.config.sport === '2'
                            ? '/assets/logo/post_pick_logo.png'
                            : '/assets/logo/racing_post_dark.png'}`}
                        alt="logo"
                    />

                    <div className="verdict__rp-post-picks">
                        <img
                            src={`/assets/icons/trap_${_.get(dataSource, '[0].post_pick')}.png`}
                            alt="post pick trap number"
                        />

                        <span>-</span>

                        <img
                            src={`/assets/icons/trap_${_.get(dataSource, '[1].post_pick')}.png`}
                            alt="post pick trap number"
                        />

                        <span>-</span>

                        <img
                            src={`/assets/icons/trap_${_.get(dataSource, '[2].post_pick')}.png`}
                            alt="post pick trap number"
                        />
                    </div>
                </div>
            )}

            {configStore.showPrices &&
                horse_name && <OddsBox text={odds} className="verdict__odds" />}

            <div className="verdict__logo">
                <img src="/assets/bulb.png" alt="bulb" />
            </div>
        </div>
    )
}

export default observer(Verdict)
