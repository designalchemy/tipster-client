import React, { Component, PropTypes } from 'react'
import cx from 'classnames'
import { DataLayer } from 'components'
import bowser from 'bowser'
import moment from 'moment'
import Cookies from 'js-cookie'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import configStore from '../../stores/ConfigStore'
import styles from './Auth.scss'

class Auth extends Component {
    constructor(props) {
        super(props)

        this.state = {
            ip: '',
            id: '',
            requestDate: '',
            data: {}
        }
    }

    componentDidMount() {
        const data = {
            id: configStore.isWhitelist ? configStore.userId : Cookies.get('tipster-auth'),
            browser: this.returnBrowser(),
            os: this.returnOS(),
            configId: this.props.configId
        }

        fetch('/api/auth', {
            method: 'post',
            headers: {
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(json => {
                configStore.storeWhitelist(json.data.ip_whitelist)
                configStore.storeId(json.data.id)

                this.returnData = json.data
                this.message = json.message

                if (json.error) {
                    if (json.data.id === 'error') {
                        return false
                    }

                    if (!json.data.ip_whitelist) {
                        return Cookies.set('tipster-auth', `${json.data.id}`, {
                            expires: 365 * 10
                        })
                    }
                }

                configStore.storeIsAuthorised(true)
            })
            .catch(err => console.log(err))
    }

    @observable message = false
    @observable returnData = {}

    returnOS() {
        let os
        if (bowser.mac) os = `Mac ${bowser.osversion}`
        if (bowser.windows) os = `Windows ${bowser.osversion}`
        if (bowser.ios) os = `IOS ${bowser.osversion}`
        if (bowser.android) os = `Android ${bowser.osversion}`
        if (bowser.linux) os = 'Linux'
        if (!os) os = 'Other OS'

        return os
    }

    returnBrowser() {
        return `${bowser.name} ${bowser.version}`
    }

    render() {
        const invalidConfig = this.returnData.id === 'error'

        return (
            <div className="auth__container">
                <div>
                    <img
                        src="/assets/logo/racing_post.png"
                        alt="logo"
                        style={{ display: 'block' }}
                    />

                    {!invalidConfig && <h1> Auth Code:</h1>}

                    {!invalidConfig && <h2>{this.returnData.id}</h2>}

                    <h2 className="auth__warning-text">{`${invalidConfig
                        ? this.message
                        : 'Waiting For Authentication'}`}</h2>

                    <div className="auth__details">
                        <h3>IP: {this.returnData.ip}</h3>

                        <h3>Browser: {this.returnBrowser()}</h3>

                        <h3>Os: {this.returnOS()}</h3>

                        <h3>
                            Date Requested:{' '}
                            {moment(this.returnData.requestDate).format('HH:mm @ DD-MM-YYYY')}
                        </h3>
                    </div>
                </div>
            </div>
        )
    }
}

export default observer(Auth)
