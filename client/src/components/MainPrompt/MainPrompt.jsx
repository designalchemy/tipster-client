import React, { Component } from 'react'
import ReactImageFallback from 'react-image-fallback'
import moment from 'moment'
import { OddsBox } from 'components'
import { parseTemplate, isNonRunner, getOdds } from 'tools/utils'
import { observer } from 'mobx-react'
import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'

import countryCode from 'tools/country.json'
import css from './MainPrompt.scss'

const buildGraph = props => {
    const items = {
        trainer_form_output: 'Trainer Form',
        going_output: 'Going',
        distance_output: 'Distance',
        course_output: 'Course',
        draw_output: 'Draw',
        ability_output: 'Ability',
        recent_form_output: 'Recent Form'
    }
    return _.keys(items).map(item => {
        if (props[item] === 0) {
            return false
        }

        return (
            <div key={items[item]} className="prompt-graph__row">
                <p>{items[item]}</p>

                <div className="prompt-graph__checks">
                    {_.times(props[item], i => (
                        <div key={`${i}${props[item]}`} className="prompt-graph__tick" />
                    ))}
                </div>
            </div>
        )
    })
}

const MainPrompt = props => {
    // main prompt
    const { data, dataSource, text } = props
    const { config } = configStore

    const finalData = _.merge(data, {
        logo: '<img class="inline-logo" src="/assets/logo/racing_post_dark.png" />'
    })

    const {
        percentage,
        jockey_name,
        trainer_name,
        jockey_uid,
        trainer_uid,
        dog_style_name,
        dog_style_name_2,
        trap_number,
        trap_number_2,
        percentaged,
        num,
        track_name,
        horse_name,
        horse_uid,
        horse_uid_2,
        dog_uid,
        dog_uid_2,
        horse_name_2,
        silk_image_path,
        silk_image_path_2,
        rp_postmark,
        declared_runners,
        start_number,
        start_number_2,
        trainer_form_output,
        going_output,
        distance_output,
        course_output,
        draw_output,
        ability_output,
        recent_form_output,
        naps_count,
        ahead_of_handicap,
        race_clear_points,
        bet_prompt_score
    } = finalData

    const goldPrompt = {
        3: '20',
        5: '40',
        7: '60',
        10: '80',
        11: '100'
    }

    const goldTipStrength =
        _.reduce(
            goldPrompt,
            (acc, item, key) => {
                if (race_clear_points >= key) {
                    acc = item
                }
                return acc
            },
            ''
        ) || 0

    // this is to make images urls for both jockeys and horses

    let imageUrl = `icons/${dataSource}.png`

    if (!_.isNil(jockey_uid)) {
        imageUrl = `jockeys/${jockey_uid}.jpg`
    } else if (!_.isNil(trainer_uid)) {
        imageUrl = `trainers/${trainer_uid}.jpg`
    }

    const odds = getOdds(horse_uid || dog_uid, horse_name || dog_style_name)
    const odds_2 = getOdds(horse_uid_2 || dog_uid_2, horse_name_2 || dog_style_name_2)

    return (
        <div className="main-prompt">
            <div className="prompt-image-container">
                <ReactImageFallback
                    src={`/assets/${imageUrl}`}
                    fallbackImage={`/assets/icons/${dataSource}.png`}
                    alt="prompt icon"
                    className="prompt-image"
                />
            </div>

            <div className="main-prompt__top-info">
                <h1
                    dangerouslySetInnerHTML={{
                        __html: dataSource.replace(/_/g, ' ')
                    }}
                />

                {percentage && (
                    <div className="prompt-strike">
                        <h1>{percentage || percentaged}%</h1>
                        <span>Strike Rate</span>
                    </div>
                )}
            </div>

            <p
                dangerouslySetInnerHTML={{
                    __html: parseTemplate(text, data, dataSource)
                }}
            />

            {(goldTipStrength || dataSource === 'any_way_will_do') && (
                <div className="main-promp__proggress-container">
                    <p>Tip Strength:</p>
                    <div className="main-prompt__proggress-bar">
                        <span style={{ width: `${goldTipStrength || bet_prompt_score * 10}%` }} />
                    </div>
                </div>
            )}

            {dataSource === 'post_data_selection' && (
                <div className="prompt-graph">{buildGraph(data)}</div>
            )}

            {rp_postmark && <div className="rp-prompt-icon">{rp_postmark}</div>}

            {ahead_of_handicap && (
                <div className="rp-prompt-icon-lb">
                    {ahead_of_handicap}
                    <span>lb</span>
                </div>
            )}

            {(horse_name || dog_style_name) && (
                <div className="main-prompt__footer">
                    {start_number && <div className="main-prompt__race-no">{start_number}</div>}
                    {silk_image_path && (
                        <div className="main-prompt__footer-silk">
                            <ReactImageFallback
                                src={`//images.racingpost.com/svg/${silk_image_path}.svg`}
                                fallbackImage="/assets/horses/blank_silk.png"
                                alt="silk"
                            />
                        </div>
                    )}
                    {(num || trap_number) && (
                        <img
                            src={`/assets/icons/trap_${num || trap_number}.png`}
                            alt="trap number"
                            className="main-prompt__trap-no"
                        />
                    )}
                    <div className="main-prompt__footer-horse">
                        <h1>{horse_name || dog_style_name}</h1>
                    </div>
                    {config.show_prices && (
                        <div className="main-prompt__odds">
                            <OddsBox text={odds} />
                        </div>
                    )}
                </div>
            )}

            {(horse_name_2 || dog_style_name_2) && (
                <div className="main-prompt__footer">
                    {start_number_2 && <div className="main-prompt__race-no">{start_number_2}</div>}

                    {silk_image_path_2 && (
                        <div className="main-prompt__footer-silk">
                            <img
                                src={`//images.racingpost.com/svg/${silk_image_path_2}.svg`}
                                alt="silk"
                            />
                        </div>
                    )}

                    {trap_number_2 && (
                        <img
                            src={`/assets/icons/trap_${trap_number_2}.png`}
                            alt="trap number"
                            className="main-prompt__trap-no"
                        />
                    )}

                    <div className="main-prompt__footer-horse">
                        <h1>{horse_name_2 || dog_style_name_2}</h1>
                    </div>

                    {config.show_prices && (
                        <div className="main-prompt__odds">
                            <OddsBox text={odds_2} />
                        </div>
                    )}
                </div>
            )}
        </div>
    )
}

MainPrompt.defaultProps = { proccessedData: {} }

export default observer(MainPrompt)
