import React, { Component } from 'react'
import { observer } from 'mobx-react'

import css from './DevTools.scss'
import racesStore from '../../stores/RacesStore'

const DevTools = props => {
    return (
        <div>
            <div className="dev-tools">
                <div>
                    Move: <a onClick={props.devNextRace}>Next</a>
                </div>
                <div>{`RaceNo: ${racesStore.currentRaceIndex || 0} / ${racesStore.racesIndex
                    .length}`}</div>
            </div>
        </div>
    )
}

export default observer(DevTools)
