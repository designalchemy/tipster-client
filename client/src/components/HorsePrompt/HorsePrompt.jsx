import React, { Component } from 'react'
import moment from 'moment'
import { observer } from 'mobx-react'
import { MainPrompt } from 'components'
import { observable } from 'mobx'

import countryCode from 'tools/country.json'

import racesStore from '../../stores/RacesStore'

class HorsePrompt extends Component {
    componentDidMount() {
        // main prompt
        this.dataSource = this.props.content.dataSource
        this.data = _.get(racesStore.currentRaceObj.bet_prompts, [`${this.dataSource}`])
        this.text = this.props.content.text

        if (this.dataSource === 'course_jockeys') {
            this.data = _.head(_.values(this.data))
            _.merge(this.data, _.maxBy(_.values(this.data.jockeys, 'bet_prompt_score')))
            _.merge(this.data, _.head(this.data.entries))
        }

        if (this.dataSource === 'course_trainers') {
            this.data = _.head(_.values(this.data))
            _.merge(this.data, _.maxBy(_.values(this.data.trainers, 'bet_prompt_score')))
            _.merge(this.data, _.head(this.data.entries))

            if (_.keys(this.data.entries).length > 1) {
                this.text = `<strong><em>{{trainer_name}}</em></strong> has had <strong>{{wins}}</strong> winners here and has <strong>${_.keys(
                    this.data.entries
                ).length}</strong> horses in this one`
            }
        }

        if (
            this.dataSource === 'hot_jockeys' ||
            this.dataSource === 'hot_trainers' ||
            this.dataSource === 'trainers_jockeys' ||
            this.dataSource === 'horses_for_courses' ||
            this.dataSource === 'ahead_of_handicapper' ||
            this.dataSource === 'most_napped'
        ) {
            this.data = _.maxBy(_.values(this.data), 'bet_prompt_score')
            _.merge(this.data, _.head(this.data.entries))
            this.data.horse_name = this.data.horse || this.data.horse_name
        }

        if (this.dataSource === 'travellers_check') {
            this.data =
                _.maxBy(_.values(this.data), 'bet_prompt_score') ||
                _.maxBy(this.data, 'bet_prompt_score')
            const distOut = this.data.dist_out

            if (_.isInteger(Number(distOut))) {
                this.data.real_location = `${distOut} miles`
            } else {
                const location = countryCode[distOut].country_desc
                this.data.real_location = `from ${location}`
            }
        }

        if (this.dataSource === 'seven_day_winners') {
            this.data = _.maxBy(_.values(this.data), 'bet_prompt_score') || _.head(this.data)

            const days = [
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',
                'Sunday'
            ]

            const daysLength = 6
            const currentIndex = days.indexOf(moment().format('dddd'))
            const prevIndex = days.indexOf(this.data.date_won)

            if (currentIndex > prevIndex) {
                this.data.days_ago = currentIndex - prevIndex
            } else {
                this.data.days_ago = daysLength - prevIndex + currentIndex + 1 // fix 0 based array
            }
        }

        if (this.dataSource === 'verdict_info') {
            this.data = racesStore.currentRaceObj.spotlight_verdict_selection
        }

        if (this.dataSource === 'key_stat') {
            this.data = racesStore.currentRaceObj.verdict
        }

        if (this.dataSource === 'most_tipped') {
            if (_.keys(this.data).length > 1) {
                const firsthorse = this.data[_.keys(this.data)[0]]
                const secondHorse = this.data[_.keys(this.data)[1]]

                if (firsthorse.selection_count / secondHorse.selection_count < 1.75) {
                    this.text =
                        'The newspaper tipsters are split on this one with <strong>{{newspaper_name}}</strong> going for <strong><em>{{horse_name}}</em></strong> and <strong>{{newspaper_name_2}}</strong> for <strong><em>{{horse_name_2}}</em></strong>'

                    this.data = firsthorse
                    this.data.newspaper_name = firsthorse.tips[0].newspaper_name
                    this.data.newspaper_name_2 = secondHorse.newspaper_name
                    this.data.horse_name_2 = secondHorse.horse_name
                    this.data.start_number_2 = secondHorse.start_number
                    this.data.silk_image_path_2 = secondHorse.silk_image_path
                    this.data.horse_uid_2 = secondHorse.horse_uid
                } else {
                    this.data = _.maxBy(_.values(this.data, 'bet_prompt_score'))
                }
            } else {
                this.data = _.maxBy(_.values(this.data, 'bet_prompt_score'))
            }
        }

        if (this.data && racesStore.currentRaceObj.race_card) {
            this.data.course_style_name = racesStore.currentRaceObj.race_card.course_style_name
        }

        racesStore.storeActiveItem([
            _.get(this.data, 'horse_name'),
            _.get(this.data, 'horse_name_2')
        ])
    }

    componentWillUnmount() {
        racesStore.storeActiveItem([])
    }

    @observable data = {}
    @observable proccessedData = {}
    @observable dataSource = ''
    @observable text = ''

    render() {
        return <MainPrompt data={this.data} dataSource={this.dataSource} text={this.text} />
    }
}

export default observer(HorsePrompt)
