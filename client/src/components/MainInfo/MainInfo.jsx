import React, { Component } from 'react'
import moment from 'moment'
import cx from 'classnames'
import { observer } from 'mobx-react'

import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'
import { calcYardsToMiles, calcReminderToFurlongs, formattedTime } from 'tools/utils'
import css from './MainInfo.scss'

const MainInfo = () => {
    const {
        course_style_name,
        race_datetime,
        race_instance_title,
        going_type_desc,
        distance_yard,
        race_class,
        division_preference,
        race_group_desc,
        track_name,
        distance_metre,
        race_title,
        race_grade_code,
        race_status_code,
        race_type_code
    } = racesStore.currentRaceObj.race_card

    const calcDistance =
        distance_yard < 1760
            ? `${_.floor(distance_yard / 220)}f`
            : `${calcYardsToMiles(distance_yard)} ${calcReminderToFurlongs(distance_yard)}` // calc the distance from yards to miles and furlongs

    const classInfo =
        Number(race_class) === 1
            ? `(${race_group_desc}) (Class ${race_class})`
            : `(Class ${race_class})` // calc the race class info

    const ifClass = _.isNil(race_class) ? '' : classInfo
    const showTitle = configStore.config.show_race_title
    const horseTitle = configStore.config.sport === '1' && showTitle
    const dogTitle = configStore.config.sport === '2' && showTitle

    return (
        <div
            className={cx('main-info-container', {
                'main-info__finished': configStore.finished
            })}
        >
            <h1 className="main-info__time">
                {formattedTime(configStore.config.race_time_is_24_hour, race_datetime)}
            </h1>

            <h2 className="main-info__location">
                {_.startCase(_.lowerCase(course_style_name || track_name))}
            </h2>

            {horseTitle && (
                <h3 className="main-info__info">
                    {race_instance_title}
                    <span>
                        <span className="main-info__class">{` ${ifClass || ''}`}</span>
                        <span className="main-info__class">{` ${calcDistance || ''}`}</span>
                        <span className="main-info__going">{` Going: ${going_type_desc ||
                            ''}`}</span>
                    </span>
                </h3>
            )}

            {dogTitle && (
                <h3 className="main-info__info main-info__gh">
                    <div>{`${race_title
                        ? `${race_title} -`
                        : ''} ${race_grade_code} - ${distance_metre}M ${race_type_code === 'F'
                        ? 'Flat'
                        : 'Hurdles'} `}</div>
                </h3>
            )}
        </div>
    )
}

export default observer(MainInfo)
