import React, { Component, PropTypes } from 'react'
import { NextMatch } from 'components'
import Clock from '../../tools/Clock'
import { observer } from 'mobx-react'

import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'
import css from './Header.scss'

const Header = singleRaceHack => {
    const { config } = configStore

    return (
        <header className="header-container">
            <Clock />

            <div className="header-logo">
                <img src={`/assets/logo/racing_post.png`} alt="" />
            </div>

            {racesStore.races.length > 0 &&
                config.raceTimeLength !== '' &&
                singleRaceHack &&
                <NextMatch />}
        </header>
    )
}

export default observer(Header)
