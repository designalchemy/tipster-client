import React, { Component } from 'react'
import { Bubble } from 'components'
import TransitionGroup from 'react-addons-transition-group'
import { isNonRunner } from 'tools/utils'
import { observer } from 'mobx-react'
import { observable, toJS, reaction } from 'mobx'

import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'
import css from './SpeechBubbles.scss'

class SpeechBubbles extends Component {
    componentDidMount() {
        if (this.props.data.twoWayChat) {
            // if its a 2 way chat, send data to be proccessed into arrays
            this.returnTwoWayChat()
        }
        this.setUpTimingConfig()
    }

    componentWillUpdate(nextProps) {
        if (this.props.data !== nextProps.data) {
            this.que = 0
        }

        this.reactToChange = reaction(
            () => [this.twoChatArray],
            () => {
                this.setUpTimingConfig()
            }
        )
    }

    componentWillUnmount() {
        clearTimeout(this.quePositionTimeout)
        this.reactToChange = null
    }

    @observable que = 0
    @observable twoChatArray = []

    setUpTimingConfig() {
        // set up custom timing config

        if (this.props.data.convo[this.que] && !this.props.data.twoWayChat) {
            // if its the single bubble que
            const timeDelay = this.props.data.convo[this.que].timingDelay
            this.setTimer(timeDelay)
        }

        if (
            _.size(this.twoChatArray) &&
            this.twoChatArray[this.que] &&
            this.props.data.twoWayChat
        ) {
            // if its the double bubble que (see componentdidupdate)
            const timeDelay = this.twoChatArray[this.que][0].timingDelay
            this.setTimer(timeDelay)
        }
    }

    setTimer(timingDelay) {
        this.quePositionTimeout = setTimeout(() => {
            // change the state to change the on screen
            this.increaseQuePosition()

            if (
                this.que === this.props.data.convo.length ||
                this.que === this.twoChatArray.length
            ) {
                // check finished for 2 types of ques
                setTimeout(() => {
                    // wait for all animations to finish, set this to custom timer later
                    this.props.increasePhaseVal() // i have finished all animations in this que, so tell my parent
                }, 500) // timing is for animation to finishing leaving
            } else {
                this.setUpTimingConfig() // if this is not the final one, run it again
            }
        }, timingDelay) // timing for each cell to stay on screen, pass via config
    }

    increaseQuePosition() {
        // add to the state which renders new items
        this.que = this.que + 1
    }

    returnTwoWayChat() {
        // turn the list of objects, into arrays of pairs, see componentwillupdate

        const items = this.props.data.convo
        const arrays = []
        const size = 2

        for (let i = 0; i < items.length; i += size) {
            arrays.push(items.slice(i, i + size))
        }

        this.twoChatArray = arrays
    }

    render() {
        const items = this.props.data.convo
        const twoChatItems = this.twoChatArray

        // TO DO: fix how this double bubble feature works, no need for repeated code
        return (
            <div>
                {!this.props.data.twoWayChat && // this is for the single bubble chat
                    items.map((item, i) => (
                        <div key={i}>
                            <TransitionGroup>
                                {this.que >= i && <Bubble key={i} {...item} delay={0} />}
                            </TransitionGroup>
                        </div>
                    ))}

                {this.props.data.twoWayChat && // this is for the duel bubble chat
                    twoChatItems.map((item, i) => (
                        <div key={i}>
                            <TransitionGroup>
                                {this.que >= i && <Bubble key={i} {...item[0]} delay={0} />}
                            </TransitionGroup>

                            <TransitionGroup>
                                {this.que >= i && (
                                    <Bubble
                                        key={`${i}_second`}
                                        {...item[1]}
                                        delay={1500}
                                        updateType
                                    />
                                )}
                            </TransitionGroup>
                        </div>
                    ))}
            </div>
        ) // class speech container close
    }
}

export default observer(SpeechBubbles)
