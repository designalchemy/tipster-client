import React, { Component } from 'react'
import { observer } from 'mobx-react'

import css from './BlankPage.scss'

const BlankPage = () => {
    return (
        <div className="blank-page__container">
            <img src="/assets/logo/racing_post.png" alt="" />
        </div>
    )
}

export default observer(BlankPage)
