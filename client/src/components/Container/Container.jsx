import React, { Component } from 'react'
import cx from 'classnames'
import { observer } from 'mobx-react'
import configStore from '../../stores/ConfigStore'

import css from './Container.scss'

// to do, this is pointless, get rid of it

class Container extends Component {
    render() {
        return (
            <div
                className={cx('main-container', {
                    'main-container__finished': configStore.finished
                })}
            >
                {this.props.children}
            </div>
        )
    }
}

export default observer(Container)
