import React, { Component } from 'react'
import { observer } from 'mobx-react'

import racesStore from '../../stores/RacesStore'
import configStore from '../../stores/ConfigStore'

import css from './RaceStats.scss'

const RaceStats = props => {
    return <div>Race Stats</div>
}

export default observer(RaceStats)
