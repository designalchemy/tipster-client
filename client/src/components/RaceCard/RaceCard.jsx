import React, { Component, PropTypes } from 'react'
import cx from 'classnames'
import { OddsBox } from 'components'
import { observer } from 'mobx-react'
import { observable, reaction } from 'mobx'
import scrollIntoViewIfNeeded from 'scroll-into-view-if-needed'
import ReactImageFallback from 'react-image-fallback'

import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'
import { isNonRunner, getOdds, getTranslation } from '../../tools/utils'

import styles from './RaceCard.scss'

class RaceCard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            scrollLog: 0
        }

        this.triggerScroll = this.triggerScroll.bind(this)
    }

    componentDidMount() {
        if (
            this.rowTrigger &&
            this.props.data.autoScroll &&
            _.size(racesStore.currentRaceObj.runners) > 14
        ) {
            this.triggerScroll()
        }
        reaction(
            () => racesStore.activeItem,
            () => {
                this.highLightDelay()
            }
        )
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.active.number !== this.props.active.number) {
            this.runAnimation()
        }

        if (this.props.active.number === 0) {
            clearTimeout()
        }
    }

    componentWillUnmount() {
        clearTimeout(this.runAnimationTimer)
        clearTimeout(this.triggerTimer)
        clearTimeout(this.subTrigger)
    }

    @observable activeClass = ''
    @observable subItems = []
    @observable highLight = []

    highLightDelay() {
        // trigger for the race card currently active anmation
        this.runAnimationTimer = setTimeout(() => {
            this.highLight = racesStore.activeItem
            this.runAnimation()
        }, 2000)
    }

    triggerScroll() {
        // this triggers the animation if > 14 races to scroll the card up and down
        const target = document.getElementsByClassName('racecard__scroll-section')[0]
        const noOfChildren = _.size(target.childNodes)

        this.triggerTimer = setTimeout(() => {
            scrollIntoViewIfNeeded(target.lastChild, false, {
                duration: noOfChildren * 400,
                easing: 'linear'
            })
        }, 2000)
    }

    runAnimation() {
        // trigger for the race card currently active anmation
        const firstOne = _.replace(_.head(_.compact(this.highLight)), / /g, '')
        if (document.getElementById(firstOne)) {
            scrollIntoViewIfNeeded(document.getElementById(firstOne), true, {
                duration: 500,
                easing: 'linear'
            })
            this.activeClass = this.props.active.number
        }
    }

    returnRows(data, isSubSection) {
        return data.map((runner, index) => {
            const {
                start_number,
                forecast_odds_desc,
                horse_name,
                jockey_name,
                trainer_stylename,
                silk_image_path,
                figures_calculated,
                non_runner,
                horse_uid
            } = runner

            // if no odds, turn to 0/0, shoud be - instead

            const isRunning = isNonRunner(
                horse_name,
                _.get(racesStore.nonRunner, [racesStore.currentRaceObj.match_url])
            )

            // if there is a runner
            const odds = getOdds(horse_uid, horse_name)
            const runnerPos = isRunning ? 'NR' : start_number
            const runnerOdds = isRunning ? '-' : odds
            const sectionClassName = cx('racecard__section', {
                'racecard__section--not-running': isRunning,
                'racecard__section--is-active': _.includes(this.highLight, horse_name)
            })
            const formArr = _.map(figures_calculated, (item, acc) => item.form_figure)
                .reverse()
                .join('')

            if (isSubSection) {
                this.subItems.push(horse_uid)
            }

            return (
                <section
                    id={_.replace(horse_name, / /g, '')}
                    key={horse_uid}
                    className={sectionClassName}
                >
                    <div className="racecard-cell__num racecard-item__num">{runnerPos}</div>
                    <div className="racecard-cell__silk racecard-item__silk">
                        <ReactImageFallback
                            src={`//images.racingpost.com/svg/${silk_image_path}.svg`}
                            fallbackImage="/assets/horses/blank_silk.png"
                            alt="silk"
                        />
                    </div>
                    <div className="racecard-cell__form racecard-item__form">{formArr}</div>
                    <div className="racecard-cell__horse racecard-item__horse-name">
                        {horse_name}
                    </div>
                    <div className="racecard-cell__jockey-and-trainer racecard-item__jockey-and-trainer">
                        <div className="racecard-item__jockey">
                            <span className="racecard-item__label">J:</span>
                            {!isRunning && jockey_name}
                        </div>
                        <div className="racecard-item__trainer">
                            <span className="racecard-item__label">T:</span>
                            {trainer_stylename}
                        </div>
                    </div>
                    {configStore.showPrices && (
                        <OddsBox
                            text={runnerOdds}
                            className="racecard-cell__odds racecard-item__odds"
                        />
                    )}
                </section>
            )
        })
    }

    sortAllRunners(allRunners) {
        const [runners, nonRunners] = this.splitRunnersAndNonRunners(allRunners)
        const sortedRunners = this.sortRunners(runners)
        const sortedNonRunners = this.sortNonRunners(nonRunners)
        return sortedRunners.concat(sortedNonRunners)
    }

    splitRunnersAndNonRunners(runners) {
        const nonRunnerInfo = this.getNonRunnerInfo()
        return _.partition(runners, runner => !isNonRunner(runner.horse_name, nonRunnerInfo))
    }

    getNonRunnerInfo() {
        const matchUrl = racesStore.currentRaceObj.match_url
        return _.get(racesStore.nonRunner, [matchUrl])
    }

    sortRunners(runners) {
        return _.chain(runners)
            .sortBy(runner => runner.start_number) // sort by saddle number first
            .sortBy(runner => this.scoreWeight(runner)) // sort only horses that are running by score
            .value()
    }

    sortNonRunners(nonRunners) {
        return _.sortBy(
            nonRunners,
            runner => this.scoreWeight(runner) // sort non-runners by score
        )
    }

    scoreWeight({ horse_uid, horse_name }) {
        const odds = getOdds(horse_uid, horse_name)
        return this.getDecimalValue(odds)
    }

    getDecimalValue(value) {
        if (this.isDecimal()) {
            return value
        }
        const odds = this.textValuesToFraction(value)
        return this.convertFractionToDecimal(odds)
    }

    textValuesToFraction(value) {
        switch (value) {
            // Even odds
            case 'Evs':
                return '1/1'
            // Starting price
            case 'Sp':
            case '-':
                return '1000/1'
            default:
                return value || '1000/1'
        }
    }

    convertFractionToDecimal(fraction) {
        return fraction
            .split('/')
            .map(x => Number(x))
            .reduce((x, y) => x / y)
    }

    isDecimal() {
        return configStore.config.price_format !== 'fraction'
    }

    numberOfFrozenCells() {
        return configStore.config.frozen_race_card_cells
    }

    render() {
        const currentRaceData = racesStore.currentRaceObj
        const runners = currentRaceData.runners
        const canScroll = this.props.data.isScrollable

        return (
            <div
                className={cx({
                    'racecard__extra-padding': _.size(racesStore.currentRaceObj.runners) <= 14
                })}
            >
                <header className="racecard__header" name="RaceCardHeader">
                    <div className="racecard-cell__num">{getTranslation('race_card__no')}</div>
                    <div className="racecard-cell__silk">{getTranslation('race_card__silk')}</div>
                    <div className="racecard-cell__form">{getTranslation('race_card__form')}</div>
                    <div className="racecard-cell__horse">{getTranslation('race_card__horse')}</div>
                    <div className="racecard-cell__jockey-and-trainer">
                        <div>{getTranslation('race_card__jockey')}</div>
                        <div>{getTranslation('race_card__trainer')}</div>
                    </div>
                    {configStore.showPrices && (
                        <div className="racecard-cell__odds">
                            {getTranslation('race_card__odds')}
                        </div>
                    )}
                </header>
                <div
                    id="scroll-box"
                    className={cx('racecard__main', {
                        'racecard__main--scrollable': canScroll,
                        'racecard__main--not-scrollable': this.props.currentPhase === 2, // this is a hack to now show display
                        'racecard__main--is-active': _.size(this.highLight)
                    })}
                >
                    {this.renderRaceCardRow(runners)}
                </div>
            </div>
        )
    }

    renderRaceCardRow(allRunners) {
        if (_.isNil(allRunners)) return false
        const sortedAllRunners = this.sortAllRunners(allRunners)
        const frozenRows = this.renderFrozenRows(sortedAllRunners)
        const restOfRows = this.renderUnfrozenRows(sortedAllRunners)
        return frozenRows.concat(restOfRows)
    }

    renderFrozenRows(runners) {
        const frozenRunners = _.take(runners, this.numberOfFrozenCells())
        return this.returnRows(frozenRunners, false)
    }

    renderUnfrozenRows(runners) {
        const unfrozenRunners = _.drop(runners, this.numberOfFrozenCells())
        const key = `${_.keys(racesStore.currentRaceObj.runners).length}_key`
        return (
            <div
                className={this.createUnfrozenClasses()}
                ref={e => {
                    this.rowTrigger = e
                }}
                key={key}
            >
                {this.returnRows(unfrozenRunners, true)}
            </div>
        )
    }

    createUnfrozenClasses() {
        return cx({
            'racecard__scroll-section': true,
            [`racecard__scroll-shadow-${this.numberOfFrozenCells()}`]: this.hasShadowClass()
        })
    }

    hasShadowClass() {
        // 14 is the number that fit on screen, calc this somethow from config v2
        return (
            _.size(racesStore.currentRaceObj.runners) > 14 &&
            !this.props.data.isScrollable &&
            _.size(this.highLight) === 0
        )
    }
}

RaceCard.defaultProps = {
    runners: [],
    active: 0
}

export default observer(RaceCard)
