import React, { Component } from 'react'
import {
    OddsBox,
    RaceVideo,
    BookMaker,
    MainPrompt,
    TextBubble,
    RaceStats,
    PostPicks,
    HorsePrompt,
    GreyHoundPrompt
} from 'components'
import cx from 'classnames'
import ReactImageFallback from 'react-image-fallback'
import { getFirst, parseTemplate } from 'tools/utils'
import { observer } from 'mobx-react'
import { observable, toJS } from 'mobx'

import racesStore from '../../stores/RacesStore.jsx'
import configStore from '../../stores/ConfigStore.jsx'

import css from './Bubble.scss'

_.templateSettings.interpolate = /{{([\s\S]+?)}}/g

class Bubble extends Component {
    constructor(props) {
        super(props)

        this.fixOverflow = this.fixOverflow.bind(this)
        this.getHeight = this.getHeight.bind(this)
    }

    componentWillMount() {
        // this is to let the other element leave with a animation before render (double timer to allow min + extra if needed)
        this.mountTimer = setTimeout(() => {
            // this is for custom loader delay on entering from props (EITHER 0 or x)
            this.renderDelay = true
            this.fixOverflow()
        }, this.props.delay + 500)
    }

    componentDidUpdate(prevProps) {
        if (
            (racesStore.currentPromptType !== _.get(this.props, 'content.dataSource') &&
                this.props.updateType) ||
            this.props.type === 'race_video'
        ) {
            setTimeout(() => {
                // time out so aniimations dont jump so much
                racesStore.storeCurrentPromptType(_.get(this.props, 'content.dataSource'))
            }, 1200)
        }
    }

    componentWillUnmount() {
        // clear all timeouts
        clearTimeout(this.mountTimer)
        clearTimeout(this.fixOverFlowTimer)
        clearTimeout(this.willLeaveTimer)
        racesStore.storeCurrentPromptType('')
    }

    @observable elementHeight = false
    @observable renderDelay = false
    @observable animationIn = ''
    @observable animationOut = 'false'
    @observable animiationFinished = false

    componentWillAppear(callback) {
        // this is the first one
        // console.log('First - animation in chain')
        this.animationIn = 'ani-in'
        callback()
    }

    componentWillEnter(callback) {
        // this is after the 1st, 2nd, 3rd etc
        // console.log('Not First - animation in chain')
        this.animationIn = 'ani-in'
        callback()
    }

    componentWillLeave(callback) {
        // this hides the element THIS NEEDS FIXING, DOSNT WORK
        // console.log('Exit Animation')
        this.animationOut = 'ani-out'

        this.willLeaveTimer = setTimeout(() => {
            // this gets rid of the element & waits for it to do its thing so dosnt suddenly dissapear
            callback()
        }, 500)
    }

    fixOverflow() {
        // hack to allow animation of max height but also the speech bubble icon to show
        this.fixOverFlowTimer = setTimeout(() => {
            this.animiationFinished = true
        }, 500)
    }

    renderMarkup() {
        const content = this.props.content

        switch (this.props.type) {
            case 'main-prompt':
            case 'verdict-text':
            case 'key-stat':
                return <HorsePrompt content={this.props.content} />

            case 'book-maker':
                return <BookMaker />

            case 'race_video':
                return <RaceVideo />

            case 'prompt-text':
                return <TextBubble text={content.text} dataSource={content.dataSource} />

            case 'post-pick':
                return <PostPicks />

            case 'gh-main-prompt':
                return <GreyHoundPrompt content={this.props.content} />

            default:
                return <TextBubble text={content.text} dataSource={content.dataSource} />
        }
    }

    getSurface() {
        return _.isNil(_.get(racesStore, 'currentRaceObj.predictor.race'))
            ? 'turf'
            : racesStore.currentRaceObj.predictor.race.surface
    }

    getHeight(element) {
        // get the height of the element so we can animation from 0 - 100% height
        if (element && !this.elementHeight) {
            // need to check that we haven't already set the height or we'll create an infinite render loop
            setTimeout(() => {
                const height = element.offsetHeight
                const margin = window.getComputedStyle(element).getPropertyValue('margin-bottom')

                this.elementHeight = element.offsetHeight + parseInt(margin, 10)
            }, 50) // timeout hack to prevent mis calucating height
        }
    }

    getArrowType() {
        if (this.props.type === 'race_video') {
            const suffix = configStore.config.sport === '1' ? this.getSurface() : 'gh'
            return `race-video__arrow-${suffix}`
        }
    }

    getDelay() {
        return this.props.type === 'key-stat' || this.props.type === 'verdict-text'
            ? 3000
            : this.props.delay // key stat hack to fix animation
    }

    getDirection() {
        return this.props.direction === 'from' ? 'from-container' : 'to-container'
    }

    getOverflow() {
        return this.animiationFinished || this.getDelay() === 0 ? 'visible' : 'hidden'
    }

    render() {
        const direction = this.getDirection()
        const trackType = this.getArrowType()
        const overflow = this.getOverflow()

        const classNames = cx(
            'bubble-container',
            direction,
            this.animationIn,
            this.animationOut,
            trackType
        )

        return (
            <div
                className="height-animation-container height-overflow-fix"
                style={{ height: this.elementHeight, overflow }}
            >
                {this.renderDelay && (
                    <div className={classNames} ref={this.getHeight}>
                        {this.renderMarkup()}
                    </div>
                )}
            </div>
        )
    }
}

export default observer(Bubble)
