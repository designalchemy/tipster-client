import React, { Component } from 'react'
import PieChart from 'react-simple-pie-chart'
import moment from 'moment'
import { observer } from 'mobx-react'
import { observable, reaction } from 'mobx'
import { formattedTime, getTranslation } from 'tools/utils'

import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'
import css from './NextMatch.scss'

class NextMatch extends Component {
    componentDidMount() {
        reaction(
            () => configStore.interval,
            () => {
                const lengthOfRace = configStore.raceTimeLength / 1000
                const hidden = configStore.interval / lengthOfRace * 100

                this.visibleBar = Math.abs(hidden - 100) > 0 ? Math.abs(hidden - 100) : 0
                this.hiddenBar = hidden < 100 ? hidden : 100
            }
        )
        reaction(
            () => [configStore.raceTimeLength, racesStore.currentRaceUrl],
            () => {
                this.visibleBar = 0
                this.hiddenBar = 100
            }
        )
    }

    @observable visibleBar = 0
    @observable hiddenBar = 100

    render() {
        const nextRaceIndex =
            racesStore.currentRaceIndex + 1 === _.size(racesStore.races)
                ? 0
                : racesStore.currentRaceIndex + 1

        const raceData = _.get(racesStore.races, [nextRaceIndex, 'race_card']) || {}

        const getTitle = _.startCase(
            _.lowerCase(raceData.course_style_name || _.get(raceData, 'track_name'))
        )

        const getTime = formattedTime(
            configStore.config.next_race_time_is_24_hour,
            raceData.race_datetime
        )

        return (
            <div className="next-match-container">
                <div className="chart-container">
                    <div className="chart-container-mask">
                        <div className="char-container-timer">
                            {configStore.interval > 0 && _.floor(configStore.interval)}
                        </div>
                    </div>

                    <PieChart
                        slices={[
                            {
                                color: '#000000',
                                value: this.hiddenBar > 0 ? this.hiddenBar : 0
                            },
                            {
                                color: '#ffffff',
                                value: this.visibleBar < 100 ? this.visibleBar : 100
                            }
                        ]}
                    />
                </div>

                <div className="next-match-container_text-container">
                    <p>{getTranslation('header__showing_next')}</p>

                    <h1>{`${getTitle} ${getTime}`}</h1>
                </div>
            </div>
        )
    }
}

export default observer(NextMatch)
