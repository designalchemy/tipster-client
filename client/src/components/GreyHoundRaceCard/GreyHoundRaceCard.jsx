import React, { Component, PropTypes } from 'react'
import cx from 'classnames'
import { OddsBox } from 'components'
import { inject, observer } from 'mobx-react'
import { toJS, observable, reaction } from 'mobx'
import { getOdds, getDogForm, getTranslation } from 'tools/utils'

import configStore from '../../stores/ConfigStore'
import racesStore from '../../stores/RacesStore'

import styles from './GreyHoundRaceCard.scss'

class GreyHoundRaceCard extends Component {
    componentDidMount() {
        reaction(
            () => racesStore.activeItem,
            () => {
                this.highLight = []
                this.highLightDelay()
            }
        )
        reaction(
            () => racesStore.currentPromptType,
            () => {
                this.highLight = []
                this.darkCardBackground = false

                const noneAciveRaceCard = [
                    'recent_trapStats',
                    'dog_performance',
                    'trap_track_records',
                    'top_of_the_class',
                    'plenty_of_trap'
                ]

                if (_.includes(noneAciveRaceCard, racesStore.currentPromptType)) {
                    setTimeout(() => {
                        // time out to allow arrow animiation to finish first
                        this.darkCardBackground = true
                    }, 2200)
                } else {
                    this.darkCardBackground = true
                }
            }
        )
    }

    componentWillUnmount() {
        clearTimeout(this.runAnimationTimer)
    }

    @observable highLight = []
    @observable darkCardBackground = false

    highLightDelay() {
        // trigger for the race card currently active anmation
        this.runAnimationTimer = setTimeout(() => {
            this.highLight = racesStore.activeItem
        }, 1750)
    }

    returnRows() {
        const runners = racesStore.currentRaceObj.runners
        const numberOfRacers = _.size(runners) > 6 ? _.size(runners) : 6

        return _.times(numberOfRacers, index => {
            const indexAjust = index + 1

            const findRunner = _.find(runners, { trap_number: indexAjust })

            const {
                dog_style_name = '',
                dog_uid = '',
                form = '',
                trainer_style_name = '',
                topspeed_master = '',
                reserve = '',
                wide_yn = ''
            } =
                findRunner || {}

            const sectionClassName = cx('gh-racecard__section', {
                'gh-racecard__section--not-running': !dog_style_name,
                'gh-racecard__section--is-active': _.includes(this.highLight, indexAjust)
            })

            const odds = getOdds(dog_uid, dog_style_name)

            const dogForm = getDogForm(form)

            const allowedSeed = ['W', 'M']

            return (
                <section id={dog_uid} key={dog_uid} className={sectionClassName}>
                    <div className="gh-racecard-cell__num gh-racecard-item__num">
                        <img
                            src={`/assets/icons/trap_${indexAjust}.png`}
                            alt="trap number"
                            className="gh-racecard__trap-no"
                        />
                    </div>
                    <div className="gh-racecard-cell__form gh-racecard-item__form">{dogForm}</div>
                    <div
                        className={`gh-racecard-cell__horse gh-racecard-item__horse-name ${dog_style_name
                            ? ''
                            : 'is-non-runner'}`}
                    >
                        <div>
                            {`${dog_style_name || 'Non Runner'} ${_.includes(allowedSeed, wide_yn)
                                ? `(${wide_yn})`
                                : ''}`}
                            {reserve &&
                                dog_style_name && <span className="is-reserve">&nbsp;(R)</span>}
                        </div>
                    </div>
                    <div className="gh-racecard-cell__trainer gh-racecard-item__trainer">
                        {trainer_style_name}
                    </div>
                    <div className="gh-racecard-cell__speed gh-racecard-item__speed">
                        {topspeed_master === 0 ? '-' : topspeed_master}
                    </div>
                    {configStore.showPrices && (
                        <OddsBox
                            text={odds}
                            className="gh-racecard-cell__odds gh-racecard-item__odds"
                        />
                    )}
                </section>
            )
        })
    }

    getCorrectStat() {
        switch (racesStore.currentPromptType) {
            case 'top_of_the_class':
                return racesStore.currentRaceObj.statistics.record_at_this_grade
            case 'plenty_of_trap':
                return racesStore.currentRaceObj.statistics.trap_record
            default:
                return _.get(racesStore.currentRaceObj.statistics, [racesStore.currentPromptType])
        }
    }

    returnCareerRows() {
        const runners = racesStore.currentRaceObj.runners
        const stats = this.getCorrectStat()
        const mostWins = _.maxBy(stats, 'runs') || _.maxBy(stats, 'total') // get highest no of races for the dog
        const numberOfRacers = _.size(runners) > 6 ? _.size(runners) : 6

        return _.times(numberOfRacers, index => {
            const indexAjust = index + 1

            const getStats =
                _.find(stats, { num: indexAjust }) || _.find(stats, { trap_number: indexAjust })

            const getRunner = _.find(runners, { trap_number: indexAjust })

            const { percentaged = '', runs = '', wins = '', total = '' } = getStats || {}
            const { dog_style_name = '', dog_uid = '' } = getRunner || {}

            const sectionClassName = cx('gh-racecard__section', {
                'gh-racecard__section--is-active': _.includes(this.highLight, indexAjust)
            })

            return (
                <section id={dog_uid} key={dog_uid} className={sectionClassName}>
                    <div className="gh-racecard-cell-arrow__odds">
                        <span>{dog_style_name}</span>
                        {configStore.showPrices && (
                            <OddsBox
                                text={getOdds(dog_uid, dog_style_name)}
                                className="gh-racecard-item-arrow__odds"
                            />
                        )}
                    </div>
                    <div className="gh-racecard-cell-arrow__trap">
                        <img src={`/assets/icons/trap_${indexAjust}.png`} alt="trap icon" />
                    </div>
                    <div className="gh-racecard-cell-arrow__container">
                        <div
                            className="gh-racecard-cell__arrow gh-racecard-cell__arrow-light-blue"
                            style={{
                                left: `-${100 -
                                    (runs || total) / (mostWins.runs || mostWins.total) * 100}%`
                            }}
                        />
                        <div
                            className="gh-racecard-cell__arrow gh-racecard-cell__arrow-blue"
                            style={{
                                left: `-${100 - wins / (mostWins.runs || mostWins.total) * 100}%`
                            }}
                        />
                    </div>
                    <div className="gh-racecard-cell-arrow__record">
                        <div className="gh-racecard-cell__bottom-text">{`${wins} Wins`}</div>
                        <div className="gh-racecard-cell__top-text">{`${runs || total} Races`}</div>
                    </div>
                </section>
            )
        })
    }

    returnTrapStatsRows() {
        const runners = racesStore.currentRaceObj.runners
        const numberOfRacers = _.size(runners) > 6 ? _.size(runners) : 6

        return _.times(numberOfRacers, index => {
            const indexAjust = index + 1

            const findRunner = _.find(racesStore.currentRaceObj.statistics.recent_trapStats, {
                num: indexAjust
            })

            const { total = '', wins = '', perfomance = '' } = findRunner || {}

            const sectionClassName = cx('gh-racecard__section', 'gh-racecard__trap-stats', {
                'gh-racecard__section--is-active': _.includes(this.highLight, indexAjust)
            })

            return (
                <section id={indexAjust} key={indexAjust} className={sectionClassName}>
                    <div className="gh-racecard-cell-arrow__trap">
                        <img src={`/assets/icons/trap_${indexAjust}.png`} alt="trap icon" />
                    </div>
                    <div className="gh-racecard-cell-arrow__container">
                        <div className="gh-racecard-cell__arrow gh-racecard-cell__arrow-light-blue" />
                        <div
                            className="gh-racecard-cell__arrow gh-racecard-cell__arrow-blue"
                            style={{ left: `${wins / total * 100 - 100}%` }}
                        />
                    </div>
                    <div className="gh-racecard-cell-arrow__record">
                        <div className="gh-racecard-cell__percentage-text">{`${Math.floor(
                            wins / total * 100
                        )}%`}</div>
                    </div>
                </section>
            )
        })
    }

    // leave this for now, might need later
    // getTitle() {
    //     const raceTitle = racesStore.currentRaceObj.race_card.track_name
    //     switch (racesStore.currentPromptType) {
    //         case 'trap_track_records':
    //             return `Individual dog’s career record at ${raceTitle} in the same trap and class it is running from today`
    //         case 'top_of_the_class':
    //             return `Individual dog’s career record at ${raceTitle} from in the class it is running in today`
    //         case 'plenty_of_trap':
    //             return `Individual dog’s career record at ${raceTitle} from the trap it is running from today`
    //         default:
    //             return `Individual dog’s career record at ${raceTitle}`
    //     }
    // }

    getTitle() {
        const raceTitle = racesStore.currentRaceObj.race_card.track_name
        switch (racesStore.currentPromptType) {
            case 'trap_track_records':
                return getTranslation('race_card__trap_track_record')
            case 'top_of_the_class':
                return getTranslation('race_card__class_record')
            case 'plenty_of_trap':
                return getTranslation('race_card__trap_record')
            default:
                return getTranslation('race_card__career_record')
        }
    }

    renderRaceCard() {
        switch (racesStore.currentPromptType) {
            case 'recent_trapStats':
                return (
                    <div key="stats">
                        <header className="gh-racecard__header gh-racecard__header-other">
                            {getTranslation('race_card__top_traps')}
                            {racesStore.currentRaceObj.race_card.track_name}
                        </header>
                        {this.returnTrapStatsRows()}
                    </div>
                )

            case 'dog_performance':
            case 'trap_track_records':
            case 'top_of_the_class':
            case 'plenty_of_trap':
                return (
                    <div key="record">
                        <header className="gh-racecard__header gh-racecard__header-other">
                            {this.getTitle()}
                        </header>
                        {this.returnCareerRows()}
                    </div>
                )

            default:
                return (
                    <div key="main">
                        <header className="gh-racecard__header">
                            <div className="gh-racecard-cell__num">
                                {getTranslation('race_card__dog_no')}
                            </div>
                            <div className="gh-racecard-cell__form">
                                {getTranslation('race_card__dog_form')}
                            </div>
                            <div className="gh-racecard-cell__horse">
                                {getTranslation('race_card__dog_dog')}
                            </div>
                            <div className="gh-racecard-cell__trainer">
                                {getTranslation('race_card__dog_trainer')}
                            </div>
                            <div className="gh-racecard-cell__speed">
                                {getTranslation('race_card__dog_top_speed')}
                            </div>
                            {configStore.showPrices && (
                                <div className="gh-racecard-cell__odds">
                                    {getTranslation('race_card__dog_odds')}
                                </div>
                            )}
                        </header>
                        {this.returnRows()}
                    </div>
                )
        }
    }

    render() {
        return (
            <div>
                <div
                    className={cx('gh-racecard__main', {
                        'gh-racecard__main--is-active':
                            this.darkCardBackground && _.size(racesStore.activeItem) > 0
                    })}
                >
                    {this.renderRaceCard()}
                </div>
            </div>
        )
    }
}

export default observer(GreyHoundRaceCard)
