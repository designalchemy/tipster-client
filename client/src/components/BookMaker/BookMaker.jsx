import React from 'react'
import { observer } from 'mobx-react'
import configStore from '../../stores/ConfigStore'
import ReactImageFallback from 'react-image-fallback'

import css from './BookMaker.scss'

const BookMaker = () => {
    const { config } = configStore

    return (
        <div className="book-maker">
            <div className="book-maker_logo">
                <ReactImageFallback
                    src={`/assets/logo/${config.bet_prompt_offer_logo}`}
                    fallbackImage="/assets/logo/racing_post.png"
                    initialImage=""
                    alt="bookmaker offer image"
                    className="prompt-image"
                />
            </div>

            <div className="book-maker_body">
                <p
                    dangerouslySetInnerHTML={{
                        __html: config.bet_prompt_offer_text
                    }}
                />
            </div>
        </div>
    )
}

export default observer(BookMaker)
