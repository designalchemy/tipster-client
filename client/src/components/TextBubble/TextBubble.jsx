import React, { Component } from 'react'
import moment from 'moment'
import { observer } from 'mobx-react'
import racesStore from '../../stores/RacesStore'
import configStore from '../../stores/ConfigStore'

import { getFirst, parseTemplate } from 'tools/utils'
import css from './TextBubble.scss'

const getTimeTillOff = () => {
    const raceTime = moment(racesStore.currentRaceObj.race_card.race_datetime)
    const isAfterMaxTime = moment()
        .add(30, 'minute')
        .isBefore(raceTime)

    if (isAfterMaxTime) {
        return '.' // this is to finish the string setence off
    }
    return ` and the race is off <strong>${raceTime.fromNow()}</strong>.`
}

const getLogo = () => '<img class="inline-logo" src="/assets/logo/racing_post_dark.png" />'

const getPostPickLogo = () =>
    '<img class="inline-logo post-picks" src="/assets/logo/post_pick_logo.png" />'

const formatDate = () => moment(racesStore.currentRaceObj.race_card.race_datetime).format('h:mm')

const countNoOfRunners = () =>
    _.countBy(
        _.get(racesStore.nonRunner, [racesStore.currentRaceObj.match_url]),
        val => val.running
    ).G

const TextBubble = props => {
    const data = { ...racesStore.currentRaceObj.race_card }

    const extraTemplateStrings = {
        till_race_time: getTimeTillOff(),
        race_datetime: formatDate(),
        no_of_runners: countNoOfRunners(),
        logo: getLogo(),
        post_pick_logo: getPostPickLogo()
    }

    const dataWithTemplateStrings = _.merge(data, extraTemplateStrings)

    return (
        <div
            className="text-bubble"
            dangerouslySetInnerHTML={{
                __html: parseTemplate(props.text, dataWithTemplateStrings)
            }}
        />
    )
}

export default observer(TextBubble)
