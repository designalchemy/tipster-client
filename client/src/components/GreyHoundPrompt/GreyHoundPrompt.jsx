import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { MainPrompt } from 'components'
import { toJS, observable } from 'mobx'
import { getDogForm } from 'tools/utils'

import racesStore from '../../stores/RacesStore'

import countryCode from 'tools/country.json'

class GreyHoundPrompt extends Component {
    componentDidMount() {
        const runners = _.map(racesStore.currentRaceObj.runners, item => item.trap_number) // all traps that are running

        this.dataSource = this.props.content.dataSource
        this.data = _.get(racesStore.currentRaceObj.bet_prompts, [`${this.dataSource}`])
        this.text = this.props.content.text

        const runnersByTrap = _.reduce(
            racesStore.currentRaceObj.runners,
            (result, dog) =>
                _.set(result, [dog.trap_number], {
                    dog_style_name: dog.dog_style_name
                }),
            {}
        )

        const runnersByUid = _.reduce(
            racesStore.currentRaceObj.runners,
            (result, dog) =>
                _.set(result, [dog.dog_uid], {
                    dog_style_name: dog.dog_style_name
                }),
            {}
        )

        if (
            this.dataSource === 'dog_performance' ||
            this.dataSource === 'recent_trapStats' ||
            this.dataSource === 'trap_track_records'
        ) {
            const search = this.dataSource === 'trap_track_records' ? 'perfomance' : 'perfomance'
            const data = _.orderBy(
                racesStore.currentRaceObj.statistics[this.dataSource],
                search,
                'desc'
            ) // sort data by perforamnce

            this.data = _.find(data, item => _.includes(runners, item.num || item.trap_number)) // find the first runner with the higest value

            _.merge(this.data, runnersByTrap[this.data.num || this.data.trap_number])

            const findDogProps = _.find(racesStore.currentRaceObj.runners, {
                dog_style_name: this.data.dog_style_name
            }) // get the dog object

            _.merge(this.data, findDogProps) //merge dog name from odds

            this.data.race_class = racesStore.currentRaceObj.race_card.race_grade_code
        }

        if (this.dataSource === 'golden_nugget') {
            this.data.dog_style_name = this.data.dog_name
        }

        if (this.dataSource === 'top_of_the_class' || this.dataSource === 'plenty_of_trap') {
            // proccess the accumluator
            const proccessAcc = (acc, item, title) => {
                if (item[title] > acc[title]) {
                    acc[title] = item[title]
                    acc[`${title}_items`] = [item]
                } else if (item[title] === acc[title]) {
                    acc[`${title}_items`].push(item)
                }
                return acc
            }

            // proccess bet prompt to a useable array
            const proccessData = _.reduce(
                this.data,
                (acc, item) => {
                    acc = proccessAcc(acc, item, 'bet_prompt_score')
                    acc = proccessAcc(acc, item, 'race_grade_percentage')
                    acc = proccessAcc(acc, item, 'race_grade_wins')
                    acc = proccessAcc(acc, item, 'trap_wins')
                    return acc
                },
                {
                    bet_prompt_score: 0,
                    bet_prompt_score_items: [],
                    race_grade_percentage: 0,
                    race_grade_percentage_items: [],
                    race_grade_wins: 0,
                    race_grade_wins_items: [],
                    trap_wins: 0,
                    trap_wins_items: []
                }
            )

            // logic to chooese which stat to display
            if (_.size(proccessData.bet_prompt_score_items) === 1) {
                this.data = proccessData.bet_prompt_score_items[0]
                this.data.dog_style_name = this.data.dog_name
            } else if (_.size(proccessData.race_grade_percentage_items) === 1) {
                this.data = proccessData.race_grade_percentage_items[0]
                this.data.dog_style_name = this.data.dog_name
            } else if (
                _.size(proccessData.race_grade_wins_items) === 1 ||
                _.size(proccessData.trap_wins_items) === 1
            ) {
                this.data = proccessData.race_grade_wins_items[0] || proccessData.trap_wins_items[0]
                this.data.dog_style_name = this.data.dog_name
            } else {
                this.text =
                    "{{dog_style_name}} and {{dog_style_name_2}} both have strong records when running in today's class."

                if (this.dataSource === 'plenty_of_trap') {
                    this.text =
                        "The full win records are below but it is worth noting {{dog_style_name}} and {{dog_style_name_2}} are particularly at home with today's draw"
                }

                this.data = proccessData.bet_prompt_score_items[0]

                this.data.dog_style_name = proccessData.bet_prompt_score_items[0].dog_name
                this.data.dog_style_name_2 = proccessData.bet_prompt_score_items[1].dog_name

                this.data.trap_number = proccessData.bet_prompt_score_items[0].trap_number
                this.data.trap_number_2 = proccessData.bet_prompt_score_items[1].trap_number

                this.data.dog_uid = proccessData.bet_prompt_score_items[0].dog_uid
                this.data.dog_uid_2 = proccessData.bet_prompt_score_items[1].dog_uid
            }
        }

        if (this.dataSource === 'leading_the_pack') {
            this.data = _.maxBy(this.data, 'dog_projected_split')
            this.data.dog_style_name = this.data.dog_name
            this.data.projected_split_difference = (this.data.projected_split_difference * 16
            ).toFixed(1)
        }

        if (this.dataSource === 'hot_form') {
            this.data = _.maxBy(this.data, 'bet_prompt_score')
            this.data.dog_style_name = this.data.dog_name

            const dogObj = _.find(racesStore.currentRaceObj.runners, {
                dog_uid: this.data.dog_uid
            }) // get the dog object

            this.data.final_dog_form = getDogForm(dogObj.form, '-')
        }

        if (this.dataSource === 'any_way_will_do') {
            this.data.dog_style_name = this.data.post_pick_1.dog_name
            this.data.dog_style_name_2 = this.data.post_pick_2.dog_name
            this.data.trap_number = this.data.post_pick_1.trap_number
            this.data.trap_number_2 = this.data.post_pick_2.trap_number
            this.data.dog_uid = this.data.post_pick_1.dog_uid
            this.data.dog_uid_2 = this.data.post_pick_2.dog_uid
        }

        this.data.track_name = racesStore.currentRaceObj.race_card.track_name

        racesStore.storeActiveItem([this.data.num, this.data.trap_number, this.data.trap_number_2])
    }

    componentWillUnmount() {
        racesStore.storeActiveItem([])
    }

    @observable data = {}
    @observable dataSource = ''
    @observable text = ''

    render() {
        return <MainPrompt data={this.data} dataSource={this.dataSource} text={this.text} />
    }
}

export default observer(GreyHoundPrompt)
